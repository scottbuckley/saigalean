import .saiga_lemmas
import .saiga_tactics
open saiga

variable cfg: saigaconfig

/-
 ##    ##  #######   ######  ######## ######## ########
 ###   ## ##     ## ##    ##    ##    ##       ##     ##
 ####  ## ##     ## ##          ##    ##       ##     ##
 ## ## ## ##     ##  ######     ##    ######   ########
 ##  #### ##     ##       ##    ##    ##       ##
 ##   ### ##     ## ##    ##    ##    ##       ##
 ##    ##  #######   ######     ##    ######## ##
-/

theorem value.nostep {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
    (c1 ⊨ {{v}} ⟶ c2 ⊨ e) -> false
:= begin
  intros c1 c2 e T v hs,
  cases hs,
end





/-
  ######  ######## ######## ########     ########  ######## ##     ##
 ##    ##    ##    ##       ##     ##    ##     ##    ##    ###   ###
 ##          ##    ##       ##     ##    ##     ##    ##    #### ####
  ######     ##    ######   ########     ##     ##    ##    ## ### ##
       ##    ##    ##       ##           ##     ##    ##    ##     ##
 ##    ##    ##    ##       ##           ##     ##    ##    ##     ##
  ######     ##    ######## ##           ########     ##    ##     ##
-/

theorem step.determinism {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {e1 e2 e3:exp cfg},
    (c1 ⊨ e1 ⟶ c2 ⊨ e2) ->
    (c1 ⊨ e1 ⟶ c3 ⊨ e3) ->
    e3 = e2 ∧ c3 = c2
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing c3 e3,
  { -- condStep
    cases hs2,
    any_goals { value_step_contra, },
    have hh := hs1_ih hs2_cs,
    doublecases `hh,
    split; reflexivity,
  }, { -- condTrue
    cases hs2,
    any_goals { value_step_contra, },
    split; reflexivity,
  }, { -- condFalse
    cases hs2,
    any_goals { value_step_contra, },
    split; reflexivity,
  }, { -- appFun
    cases hs2,
    any_goals { value_step_contra, },
    have hh := hs1_ih hs2_fs,
    doublecases `hh,
    split; reflexivity,
  }, { -- appParam
    generalize hg: ({{hs1_f}} = ev), rewrite hg at hs2,
    cases hs2, {
      rewrite <- hg at hs2_fs,
      value_step_contra,
    }, {
      have hh := hs1_ih hs2_ps,
      doublecases `hh,
      split; reflexivity,
    }, {
      value_step_contra,
    },
  }, { -- appApp
    generalize hg: (@exp.eVal cfg (hs1_T1 -> hs1_T2) hs1_f = ev),
    rewrite hg at hs2,
    cases hs2, {
      rewrite <- hg at hs2_fs,
      value_step_contra,
    }, {
      value_step_contra,
    }, {
      have hh := fun_type_eq (exp.val.type_eq hg), cases hh,
      cases hg, split; reflexivity,
    }    
  }, { -- attrNode
    cases hs2, {
      have hh := hs1_ih hs2_ns,
      doublecases `hh, split; reflexivity,
    },
    any_goals { value_step_contra },
  }, { -- attrParam
    cases hs2,
    any_goals { value_step_contra }, {
      have hh := hs1_ih hs2_ps,
      doublecases `hh,
      split; reflexivity,
    },
  }, { -- attrFetchValue
    cases hs2, 
    any_goals {
      value_step_contra
    }, {
      split; reflexivity,
    }, {
      cases value_not_notvalue hs1_v hs2_nv,
    }, {
      rewrite hs2_ismk at hs1_v,
      cases hs1_v,
    },
  }, { -- attrFetchCached
    cases hs2,
    any_goals {
      value_step_contra
    }, {
      cases value_not_notvalue hs2_v hs1_nv,
    }, {
      split; reflexivity,
    }, {
      rewrite hs2_ismk at hs1_nv,
      cases hs1_nv,
    },
  }, { -- attrFetchHO
    cases hs2,
        any_goals {
      value_step_contra
    }, {
      rewrite hs1_ismk at hs2_v,
      cases hs2_v,
    }, {
      rewrite hs1_ismk at hs2_nv,
      cases hs2_nv,
    }, {
      rewrite <- hs1_isfresh at hs2_isfresh,
      cases hs2_isfresh,
      rewrite hs1_ismk at hs2_ismk,
      cases hs2_ismk,
      split; reflexivity,
    },
  }, { -- cacheStep
    cases hs2, {
      have hh := hs1_ih hs2_es,
      doublecases `hh,
      split; reflexivity,
    },
    value_step_contra,
  }, { -- cacheWrite
    cases hs2,
    value_step_contra,
    split; reflexivity,
  },
end


/-
 ######## ##    ## ########  ########    ########  ######## ##     ##
    ##     ##  ##  ##     ## ##          ##     ##    ##    ###   ###
    ##      ####   ##     ## ##          ##     ##    ##    #### ####
    ##       ##    ########  ######      ##     ##    ##    ## ### ##
    ##       ##    ##        ##          ##     ##    ##    ##     ##
    ##       ##    ##        ##          ##     ##    ##    ##     ##
    ##       ##    ##        ########    ########     ##    ##     ##
-/

theorem type.determinism {cfg:saigaconfig}:
  ∀ {e:exp cfg} {T1 T2:Type}
    (t1: expType e T1)
    (t2: expType e T2),
    T2 = T1
:= begin
  introv, intros ht1 ht2,
  induction e generalizing T1 T2,
  { -- val
    cases ht1, cases ht2,
    reflexivity,
  }, { -- cond
    cases ht1, cases ht2,
    apply e_x_1 ht1_tt ht2_tt,
  }, { -- app
    cases ht1, cases ht2,
    have hh1 := e_x_1 ht1_pt ht2_pt,
    ccases `hh1,
    have hh2 := e_x ht1_ft ht2_ft,
    apply fun_type_eq hh2,
  }, { -- attr
    cases ht1, cases ht2,
    reflexivity,
  }, { -- cache
    cases ht1, cases ht2,
    reflexivity,
  }, { -- higher order
    cases ht1, cases ht2,
    reflexivity,
  }
end


/-
 ######## ##    ## ########  ########    ########  ########  ########  ######
    ##     ##  ##  ##     ## ##          ##     ## ##     ## ##       ##    ##
    ##      ####   ##     ## ##          ##     ## ##     ## ##       ##
    ##       ##    ########  ######      ########  ########  ######    ######
    ##       ##    ##        ##          ##        ##   ##   ##             ##
    ##       ##    ##        ##          ##        ##    ##  ##       ##    ##
    ##       ##    ##        ########    ##        ##     ## ########  ######
-/

def type_safe {cfg:saigaconfig} (c:ctxN cfg) :=
  ∀ (n:node) (a:cfg.A) (p:cfg.ρ a),
    expType (c.c n a p) (cfg.τ a)

theorem step.type_preservation {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {e1 e2:exp cfg} {T:Type}
    (hts: type_safe c1)
    (ht: expType e1 T)
    (hs: c1 ⊨ e1 ⟶ c2 ⊨ e2),
    expType e2 T
:= begin
  introv, intros hts ht hs,
  induction hs generalizing T,
  { -- condStep
    cases ht,
    have hh1 := hs_ih hts ht_ct,
    constructor; assumption,
  }, { -- condTrue
    cases ht, assumption,
  }, { -- condFalse
    cases ht, assumption,
  }, { -- appFunStep
    cases ht,
    have hh1 := hs_ih hts ht_ft,
    constructor; assumption,
  }, { -- appParStep
    cases ht,
    have hh1 := hs_ih hts ht_pt,
    constructor; assumption,
  }, { -- appApp
    cases ht, 
    cases ht_pt,
    have hh1 := exptype.val.type_eq ht_ft,
    have hh2 := fun_type_eq hh1,
    cases hh2,
    constructor,
  }, { -- attrNode
    cases ht,
    have hh1 := hs_ih hts ht_nt,
    constructor; assumption,
  }, { -- attrParam
    cases ht,
    have hh1 := hs_ih hts ht_pt,
    constructor; assumption,
  }, { -- attrFetchValue
    cases ht,
    apply hts,
  }, { -- attrFetchCached
    cases ht,
    constructor,
    apply hts,
  }, { -- attrFetchHO
    cases ht, rewrite hs_tp_a,
    constructor,
  }, { -- cacheStep
    cases ht,
    have hh1 := hs_ih hts ht_et,
    constructor; assumption,
  }, { -- cacheWrite
    cases ht, assumption,
  }
end



/-
 ########  ########   #######   ######   ########  ########  ######   ######
 ##     ## ##     ## ##     ## ##    ##  ##     ## ##       ##    ## ##    ##
 ##     ## ##     ## ##     ## ##        ##     ## ##       ##       ##
 ########  ########  ##     ## ##   #### ########  ######    ######   ######
 ##        ##   ##   ##     ## ##    ##  ##   ##   ##             ##       ##
 ##        ##    ##  ##     ## ##    ##  ##    ##  ##       ##    ## ##    ##
 ##        ##     ##  #######   ######   ##     ## ########  ######   ######
-/

theorem step.progress {cfg:saigaconfig}:
  ∀ {c1:ctxN cfg} {e1:exp cfg} {T:Type}
    (hts: type_safe c1)
    (ht: expType e1 T),
    (∃ c2 e2, c1 ⊨ e1 ⟶ c2 ⊨ e2) ∨ value e1 ∨ cont_mk e1
:= begin
  introv, intros hts ht,
  induction e1 generalizing T,
  { -- val
    right, left, constructor,
  },
   { -- cond
    cases ht,
    have hh1 := e1_x ht_ct, clear e1_x e1_x_1 e1_x_2,
    cases hh1, {
      left,
      cases hh1 with c2' hh1,
      cases hh1 with e2' hh1,
      existsi _, existsi _,
      apply step.condStep hh1,
    }, cases hh1, {
      left,
      cases hh1 with _ b hh1,
      cases ht_ct,
      existsi c1,
      cases b, {
        existsi _, apply step.condFalse,
      }, {
        existsi _, apply step.condTrue,
      },
    }, {
      right, right,
      apply cont_mk.condC hh1,
    },
  }, { -- app
    cases ht,
    have hh1 := e1_x ht_ft, clear e1_x,
    have hh2 := e1_x_1 ht_pt, clear e1_x_1,
    cases hh1, {
      left,
      cases hh1 with c2' hh1, cases hh1 with e2' hh1,
      existsi _, existsi _,
      apply step.appFun hh1,
    }, cases hh1, {
      cases hh1 with _ f hh1,
      cases ht_ft,
      cases hh2, {
        left,
        cases hh2 with c2' hh2, cases hh2 with e2' hh2,
        existsi _, existsi _,
        apply step.appParam hh2,
      }, cases hh2, {
        left,
        cases hh2 with _ p hh2, cases ht_pt,
        existsi _, existsi _,
        apply step.appApp,
      }, {
        right, right,
        apply cont_mk.appP hh2,
      },
    }, {
      right, right,
      apply cont_mk.appF hh1,
    },
  }, { -- attr
    cases ht,
    have hh1 := e1_x ht_nt, clear e1_x,
    have hh2 := e1_x_1 ht_pt, clear e1_x_1,
    cases hh1, {
      left,
      cases hh1 with c2' hh1, cases hh1 with e2' hh1,
      existsi _, existsi _,
      apply step.attrNode hh1,
    }, cases hh1, {
      cases hh1 with _ n hh1,
      cases ht_nt,
      cases hh2, {
        left,
        cases hh2 with c2' hh2, cases hh2 with e2' hh2,
        existsi _, existsi _,
        apply step.attrParam hh2,
      }, cases hh2, {
        left,
        cases hh2 with _ p hh2,
        cases ht_pt,
        have hh3 := value_or_notvalue_or_ho (c1.c n e1_a p),
        cases hh3, {
          existsi _, existsi _,
          apply step.attrFetchValue hh3,
        }, cases hh3, {
          existsi _, existsi _,
          apply step.attrFetchCached hh3
        }, {
          cases hh3 with hl hh3,
          existsi _, existsi _,
          apply step.attrFetchHO,
          tactic.rotate 1,
          apply hh3, reflexivity,
          have hh4 := hts n e1_a p,
          rewrite hh3 at hh4,
          generalize hg: (cfg.τ e1_a) = T3,
          rewrite hg at hh4, cases hh4,
          reflexivity,
        },
      }, {
        right, right,
        apply cont_mk.attrP hh2,
      },
    }, {
      right, right,
      apply cont_mk.attrN hh1,
    },
  }, { -- cache
    cases ht,
    have hh1 := e1_x ht_et, clear e1_x,
    cases hh1, {
      left,
      cases hh1 with c2' hh1, cases hh1 with e2' hh1,
      existsi _, existsi _,
      apply step.cacheStep hh1,
    }, cases hh1, {
      left,
      cases hh1 with _ v hh1,
      cases ht_et,
      existsi _, existsi _,
      apply step.cacheWrite,
    }, {
      right, right,
      apply cont_mk.cache hh1,
    }
  }, { -- mk
    right, right,
    apply cont_mk.mk,
  }
end



/-
 ########  ####  ######    ######  ######## ######## ########
 ##     ##  ##  ##    ##  ##    ##    ##    ##       ##     ##
 ##     ##  ##  ##        ##          ##    ##       ##     ##
 ########   ##  ##   ####  ######     ##    ######   ########
 ##     ##  ##  ##    ##        ##    ##    ##       ##
 ##     ##  ##  ##    ##  ##    ##    ##    ##       ##
 ########  ####  ######    ######     ##    ######## ##
-/

lemma multistep.rec.condC {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {eC eT eF:exp cfg} {T1 T2:Type} {v1:T1} {v2:T2}
    (cs: c1 ⊨ eC ⟹ c2 ⊨ v1)
    (ms: c2 ⊨ IFF {{v1}} THEN eT ELSE eF ⟹ c3 ⊨ v2),
    (c1 ⊨ IFF eC THEN eT ELSE eF ⟹ c3 ⊨ v2)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing T2 c3, {
    assumption,
  }, { 
    have hh := hs1_ih hs2,
    apply multistep.step,
    apply step.condStep hs1_a,
    apply hs1_ih hs2,
  },
end

lemma multistep.rec.appF {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {eF eP:exp cfg} {T1 T2:Type} {v1:T1} {v2:T2}
    (fs: c1 ⊨ eF ⟹ c2 ⊨ v1)
    (ms: c2 ⊨ ({{v1}} OF eP) ⟹ c3 ⊨ v2),
    (c1 ⊨ (eF OF eP) ⟹ c3 ⊨ v2)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing T2 c3, {
    assumption,
  }, {
    apply multistep.step,
    apply step.appFun hs1_a,
    apply hs1_ih hs2,
  },
end

lemma multistep.rec.appP {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {eP:exp cfg} {T1 T2 T3:Type} {v1:T1} {v2:T2} {v3:T3}
    (ps: c1 ⊨ eP ⟹ c2 ⊨ v2)
    (ms: c2 ⊨ ({{v1}} OF {{v2}}) ⟹ c2 ⊨ v3),
    (c1 ⊨ ({{v1}} OF eP) ⟹ c2 ⊨ v3)
:= begin
  introv, intros hs1 hs2,
  induction hs1, {
    assumption,
  }, {
    apply multistep.step,
    apply step.appParam hs1_a,
    apply hs1_ih hs2,
  }
end

lemma multistep.rec.attrN {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {eN eP:exp cfg} {T1 T2:Type}
            {v1:T1} {v2:T2} {a:cfg.A}
    (cs: c1 ⊨ eN ⟹ c2 ⊨ v1)
    (ms: c2 ⊨ ({{v1}} DOT a WITH eP) ⟹ c3 ⊨ v2),
    (c1 ⊨ (eN DOT a WITH eP) ⟹ c3 ⊨ v2)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing T2 c3, {
    assumption,
  }, {
    apply multistep.step,
    apply step.attrNode hs1_a,
    apply hs1_ih hs2,
  },
end

lemma multistep.rec.attrP {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {eP:exp cfg} {T1 T2 T3:Type}
            {v1:T1} {v2:T2} {v3:T3} {a:cfg.A}
    (ps: c1 ⊨ eP ⟹ c2 ⊨ v2)
    (ms: c2 ⊨ ({{v1}} DOT a WITH {{v2}}) ⟹ c3 ⊨ v3),
    (c1 ⊨ ({{v1}} DOT a WITH eP) ⟹ c3 ⊨ v3)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing T3 c3, {
    assumption,
  }, {
    apply multistep.step,
    apply step.attrParam hs1_a,
    apply hs1_ih hs2,
  },
end

lemma multistep.rec.cache {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {eC:exp cfg} {T1 T2:Type}
            {v1:T1} {v2:T2} {n:node} {a:cfg.A} {p:cfg.ρ a}
    (cs: c1 ⊨ eC ⟹ c2 ⊨ v1)
    (ms: c2 ⊨ (n/a/p ;= {{v1}}) ⟹ c3 ⊨ v2),
    (c1 ⊨ (n/a/p ;= eC) ⟹ c3 ⊨ v2)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing T2 c3, {
    assumption,
  }, {
    apply multistep.step,
    apply step.cacheStep hs1_a,
    apply hs1_ih hs2,
  },
end



theorem bigstep.multistep {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T}
    (hs: c1 ⊨ e »» c2 ⊨ v),
    (c1 ⊨ e ⟹ c2 ⊨ v)
:= begin
  introv, intros hs,
  induction hs,
  { -- refl
    apply multistep.refl,
  }, { -- condT
    apply multistep.rec.condC hs_ih_cs,
    apply multistep.step step.condTrue,
    assumption,
  }, { -- condF
    apply multistep.rec.condC hs_ih_cs,
    apply multistep.step step.condFalse,
    assumption,
  }, { -- app
    apply multistep.rec.appF hs_ih_fs,
    apply multistep.rec.appP hs_ih_ps,
    apply multistep.step step.appApp multistep.refl,
  }, { -- attrValue
    apply multistep.rec.attrN hs_ih_ns,
    apply multistep.rec.attrP hs_ih_ps,
    apply multistep.step,
    apply step.attrFetchValue hs_hv,
    assumption,
  }, { -- attrCache
    apply multistep.rec.attrN hs_ih_ns,
    apply multistep.rec.attrP hs_ih_ps,
    apply multistep.step,
    apply step.attrFetchCached hs_nv,
    apply multistep.rec.cache hs_ih_cs,
    apply multistep.step step.cacheWrite multistep.refl,
  }, { -- attrHO
    apply multistep.rec.attrN hs_ih_ns,
    apply multistep.rec.attrP hs_ih_ps,
    apply multistep.step,
    apply step.attrFetchHO hs_tp_a hs_ismk hs_isfresh,
    apply multistep.refl,
  }, { -- cache
    apply multistep.rec.cache hs_ih,
    apply multistep.step,
    apply step.cacheWrite,
    apply multistep.refl,
  }
end





lemma step.bigstep {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {e1 e2:exp cfg} {T:Type} {v:T}
    (hs:  c1 ⊨ e1 ⟶ c2 ⊨ e2)
    (hs2: c2 ⊨ e2 »» c3 ⊨ v),
    (c1 ⊨ e1 »» c3 ⊨ v)
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing c3 T,
  { -- condStep
    cases hs2, {
      apply bigstep.condT _ hs2_ts,
      apply hs1_ih hs2_cs,
    }, {
      apply bigstep.condF _ hs2_fs,
      apply hs1_ih hs2_cs,
    },
  }, { -- condTrue
    apply bigstep.condT bigstep.refl hs2,
  }, { -- condFalse
    apply bigstep.condF bigstep.refl hs2,
  }, { -- appFun
    cases hs2,
    apply bigstep.app _ hs2_ps,
    apply hs1_ih hs2_fs,
  }, { -- appParam
    cases hs2,
    cases bigstep.val.same.v hs2_fs,
    cases bigstep.val.same.ctx hs2_fs,
    apply bigstep.app bigstep.refl,
    apply hs1_ih hs2_ps,
  }, { -- appApp
    cases hs2,
    apply bigstep.app bigstep.refl bigstep.refl,
  }, { -- attrNode
    cases hs2, {
      apply bigstep.attrValue hs2_hv _ hs2_ps hs2_cs,
      apply hs1_ih hs2_ns,
    }, {
      apply bigstep.attrCached hs2_nv _ hs2_ps hs2_cs,
      apply hs1_ih hs2_ns,
    }, {
      apply bigstep.attrHO _ hs2_ismk hs2_isfresh _ hs2_ps,
      apply hs1_ih hs2_ns,
    },
  }, { -- attrParam
    cases hs2, {
      cases bigstep.val.same.v hs2_ns,
      cases bigstep.val.same.ctx hs2_ns, clear _x _x_1, clear hs2_ns,
      apply bigstep.attrValue hs2_hv bigstep.refl _ hs2_cs,
      apply hs1_ih hs2_ps,
    }, {
      cases bigstep.val.same.v hs2_ns,
      cases bigstep.val.same.ctx hs2_ns, clear _x _x_1, clear hs2_ns,
      apply bigstep.attrCached hs2_nv bigstep.refl _ hs2_cs,
      apply hs1_ih hs2_ps,
    }, {
      cases bigstep.val.same.v hs2_ns,
      cases bigstep.val.same.ctx hs2_ns, clear _x _x_1, clear hs2_ns,
      apply bigstep.attrHO _ hs2_ismk hs2_isfresh bigstep.refl _,
      apply hs1_ih hs2_ps,
    },
  }, { -- attrFetchValue
    apply bigstep.attrValue hs1_v bigstep.refl bigstep.refl hs2,
  }, { -- attrFetchCached
    cases hs2,
    apply bigstep.attrCached hs1_nv bigstep.refl bigstep.refl hs2_c,
  }, { -- attrFetchHO
    cases hs2,
    apply bigstep.attrHO hs1_tp_a hs1_ismk hs1_isfresh bigstep.refl bigstep.refl,
  }, { -- cacheStep
    cases hs2,
    apply bigstep.cache,
    apply hs1_ih hs2_c,
  }, { -- cacheWrite
    cases hs2,
    apply bigstep.cache bigstep.refl,
  },
end


theorem multistep.bigstep {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T}
    (hs: c1 ⊨ e ⟹ c2 ⊨ v),
    (c1 ⊨ e »» c2 ⊨ v)
:= begin
  introv, intros hs,
  induction hs, {
    apply bigstep.refl,
  }, {
    apply step.bigstep hs_a hs_ih,
  }
end

/-
 ########   ######  ######## ######## ########     ########  ########   #######  ########   ######
 ##     ## ##    ##    ##    ##       ##     ##    ##     ## ##     ## ##     ## ##     ## ##    ##
 ##     ## ##          ##    ##       ##     ##    ##     ## ##     ## ##     ## ##     ## ##
 ########   ######     ##    ######   ########     ########  ########  ##     ## ########   ######
 ##     ##       ##    ##    ##       ##           ##        ##   ##   ##     ## ##              ##
 ##     ## ##    ##    ##    ##       ##           ##        ##    ##  ##     ## ##        ##    ##
 ########   ######     ##    ######## ##           ##        ##     ##  #######  ##         ######
-/


theorem multistep.determinism {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {e:exp cfg} {T2 T3:Type} {v2:T2} {v3:T3},
    (c1 ⊨ e ⟹ c2 ⊨ v2) ->
    (c1 ⊨ e ⟹ c3 ⊨ v3) ->
    v3 == v2 ∧ c3 = c2
:= begin
  introv, intros hs1 hs2,
  induction hs1 generalizing c3 T3; cases hs2, {
    split; reflexivity,
  }, {
    cases hs2_a,
  }, {
    cases hs1_a,
  }, {
    have hh1 := step.determinism hs1_a hs2_a,
    doublecases `hh1,
    have hh2 := hs1_ih hs2_a_1,
    exact hh2,
  }
end

theorem bigstep.determinism {cfg:saigaconfig}:
  ∀ {c1 c2 c3:ctxN cfg} {e:exp cfg} {T2 T3:Type} {v2:T2} {v3:T3},
    (c1 ⊨ e »» c2 ⊨ v2) ->
    (c1 ⊨ e »» c3 ⊨ v3) ->
    v3 == v2 ∧ c3 = c2
:= begin
  introv, intros hs1 hs2,
  have hh1 := bigstep.multistep hs1,
  have hh2 := bigstep.multistep hs2,
  have hh3 := multistep.determinism hh1 hh2,
  exact hh3,
end
