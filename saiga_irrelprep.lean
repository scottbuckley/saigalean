
import .saiga_lemmas
import .saiga_tactics
import .saiga_metatheoretic
open saiga
open tactic

variable cfg: saigaconfig





/-
    ##       ########  ######      ###    ##
    ##       ##       ##    ##    ## ##   ##
    ##       ##       ##         ##   ##  ##
    ##       ######   ##   #### ##     ## ##
    ##       ##       ##    ##  ######### ##
    ##       ##       ##    ##  ##     ## ##
    ######## ########  ######   ##     ## ########
-/

-- inductive legal {cfg:_} (ctx:ctxN cfg): exp cfg -> Prop
-- | notcache {e:exp cfg}:
--     notcache e -> legal e
-- | cache {e:exp cfg} {n:node} {a:cfg.A} {p:cfg.ρ a}:
--     legal (n/a/p;= ctx.c n a p)

-- axiom legal_context_axiom {cfg:saigaconfig} : ∀ {n:node} {a:cfg.A}
--           {p:cfg.ρ a} {ctx:ctxN cfg},
--   legal ctx (ctx.c n a p)

-- lemma lg.condC : ∀ {cfg:_} {c1 c2:ctxN cfg} {eC eT eF:exp cfg},
--   legal c1 (IFF eC THEN eT ELSE eF) ->
--   legal c2 eC
-- := begin {intros, cases a, cases a_a, constructor, assumption,} end

-- lemma lg.condT : ∀ {cfg:_} {c1 c2:ctxN cfg} {eC eT eF:exp cfg},
--   legal c1 (IFF eC THEN eT ELSE eF) ->
--   legal c2 eT
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end

-- lemma lg.condF : ∀ {cfg:_} {c1 c2:ctxN cfg} {eC eT eF:exp cfg},
--   legal c1 (IFF eC THEN eT ELSE eF) ->
--   legal c2 eF
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end

-- lemma lg.appF : ∀ {cfg:_} {c1 c2:ctxN cfg} {eF eP:exp cfg},
--   legal c1 (eF OF eP) ->
--   legal c2 eF
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end

-- lemma lg.appP : ∀ {cfg:_} {c1 c2:ctxN cfg} {eF eP:exp cfg},
--   legal c1 (eF OF eP) ->
--   legal c2 eP
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end

-- lemma lg.attrN : ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg} {a:cfg.A} {eN eP:exp cfg},
--   legal c1 (eN DOT a WITH eP) ->
--   legal c2 eN
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end

-- lemma lg.attrP : ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg} {a:cfg.A} {eN eP:exp cfg},
--   legal c1 (eN DOT a WITH eP) ->
--   legal c2 eP
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end



inductive legal {cfg:_}: ctxN cfg -> exp cfg -> Prop
-- | val_notnode {T:Type} {v:T} {c}:
--     T ≠ node ->
--     legal c {{v}}
-- | val_node {n:node} {c:ctxN cfg}:
--     contained n c.N ->
--     legal c {{n}}
| val {T:Type} {v:T} {c1:ctxN cfg}:
    legal c1 {{v}}
| condT {eC eT eF} {c1 c2:ctxN cfg}:
    legal c1 eC ->
    (c1 ⊨ eC »» c2 ⊨ tt) ->
    legal c2 eT ->
    legal c1 (IFF eC THEN eT ELSE eF)
| condF {eC eT eF} {c1 c2:ctxN cfg}:
    legal c1 eC ->
    (c1 ⊨ eC »» c2 ⊨ ff) ->
    legal c2 eF ->
    legal c1 (IFF eC THEN eT ELSE eF)
| app {eF eP} {c1 c2:ctxN cfg} {T:Type} {f:T}:
    legal c1 eF ->
    (c1 ⊨ eF »» c2 ⊨ f) ->
    legal c2 eP ->
    legal c1 (eF OF eP)
| attr {eN eP} {a:cfg.A} {c1 c2:ctxN cfg} {n:node}:
    legal c1 eN ->
    (c1 ⊨ eN »» c2 ⊨ n) ->
    contained n c2.N ->
    legal c2 eP ->
    legal c1 (eN DOT a WITH eP)
| cache {e:exp cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {c:ctxN cfg}:
    contained n c.N ->
    legal c (n/a/p;= c.c n a p)

axiom legal_context_axiom {cfg:saigaconfig} : ∀ {n:node} {a:cfg.A}
          {p:cfg.ρ a} {ctx:ctxN cfg},
  legal ctx (ctx.c n a p)

lemma lg.condC : ∀ {cfg:_} {c1:ctxN cfg} {eC eT eF:exp cfg},
  legal c1 (IFF eC THEN eT ELSE eF) ->
  legal c1 eC
:= begin
  introv, intros hl, cases hl; assumption,
end
-- := begin {intros, cases a, cases a_a, constructor, assumption,} end

lemma lg.condT : ∀ {cfg:_} {c1 c2:ctxN cfg} {eC eT eF:exp cfg},
  legal c1 (IFF eC THEN eT ELSE eF) ->
  (c1 ⊨ eC »» c2 ⊨ tt) ->
  legal c2 eT
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl hs,
  cases hl; have hh := bigstep.determinism hs hl_a_1,
  doublecases `hh, assumption,
  cases hh with hh1 hh2, cases hh1,
end

lemma lg.condF : ∀ {cfg:_} {c1 c2:ctxN cfg} {eC eT eF:exp cfg},
  legal c1 (IFF eC THEN eT ELSE eF) ->
  (c1 ⊨ eC »» c2 ⊨ ff) ->
  legal c2 eF
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl hs,
  cases hl; have hh := bigstep.determinism hs hl_a_1,
  cases hh with hh1 hh2, cases hh1,
  doublecases `hh, assumption,
end

lemma lg.appF : ∀ {cfg:_} {c1:ctxN cfg} {eF eP:exp cfg},
  legal c1 (eF OF eP) ->
  legal c1 eF
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl,
  cases hl, assumption,
end

lemma lg.appP : ∀ {cfg:_} {c1 c2:ctxN cfg} {eF eP:exp cfg} {T:Type} {v:T},
  legal c1 (eF OF eP) ->
  (c1 ⊨ eF »» c2 ⊨ v) ->
  legal c2 eP
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl hs,
  cases hl, have hh := bigstep.determinism hs hl_a_1,
  doublecases `hh, assumption,
end

lemma lg.attrN : ∀ {cfg:saigaconfig} {c1:ctxN cfg} {a:cfg.A} {eN eP:exp cfg},
  legal c1 (eN DOT a WITH eP) ->
  legal c1 eN
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl,
  cases hl, assumption,
end

lemma lg.attrP : ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg} {a:cfg.A} {eN eP:exp cfg} {T:Type} {v:T},
  legal c1 (eN DOT a WITH eP) ->
  (c1 ⊨ eN »» c2 ⊨ v) ->
  legal c2 eP
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl hs,
  cases hl, have hh := bigstep.determinism hs hl_a_2,
  doublecases `hh, assumption,
end

lemma lg.attrCont : ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg} {a:cfg.A} {eN eP:exp cfg} {n:node},
  legal c1 (eN DOT a WITH eP) ->
  (c1 ⊨ eN »» c2 ⊨ n) ->
  contained n c2.N
-- := begin {introv, intros hl, cases hl, cases hl_a, constructor, assumption,} end
:= begin
  introv, intros hl hs,
  cases hl, have hh := bigstep.determinism hs hl_a_2,
  doublecases `hh, assumption,
end





-- admitted for now
lemma bigstep.value.remains: ∀ {cfg:_} {e:exp cfg} {c1 c2:ctxN cfg} {n:node}
          {a:cfg.A} {p:cfg.ρ a} {T:Type} {v:T},
  legal c1 e ->
  (contained n c1.N) ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  value (c1.c n a p) ->
  c2.c n a p = c1.c n a p
:= begin
  introv, intros hl hc hs hv,
  induction hs generalizing n a p,
  { -- refl
    reflexivity,
  }, { -- condT
    have hh1 := hs_ih_cs (lg.condC hl) hc hv, rewrite <- hh1 at hv,
    have hh2 := hs_ih_ts (lg.condT hl hs_cs) (bigstep.contained hc hs_cs) hv,
    rewrite hh2, rewrite hh1,
  }, { -- condF
    have hh1 := hs_ih_cs (lg.condC hl) hc hv, rewrite <- hh1 at hv,
    have hh2 := hs_ih_fs (lg.condF hl hs_cs) (bigstep.contained hc hs_cs) hv,
    rewrite hh2, rewrite hh1,
  }, { -- app
    have hh1 := hs_ih_fs (lg.appF hl) hc hv,rewrite <- hh1 at hv,
    have hh2 := hs_ih_ps (lg.appP hl hs_fs) (bigstep.contained hc hs_fs) hv,
    rewrite hh2, rewrite hh1,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns (lg.attrN hl) hc hv, rewrite <- hh1 at hv,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) (bigstep.contained hc hs_ns) hv, rewrite <- hh2 at hv,
    have hh3 := hs_ih_cs _ _ hv, rotate 1, {
      apply legal_context_axiom,
    }, {
      apply bigstep.contained _ hs_ps,
      apply bigstep.contained hc hs_ns,
    },
    rewrite hh3, rewrite hh2, rewrite hh1,
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns (lg.attrN hl) hc hv, rewrite <- hh1 at hv,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) (bigstep.contained hc hs_ns) hv, rewrite <- hh2 at hv,
    have hh3 := hs_ih_cs _ _ hv, rotate 1, {
      apply legal_context_axiom,
    }, {
      apply bigstep.contained _ hs_ps,
      apply bigstep.contained hc hs_ns,
    },
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    destruct (napmatch _ n' n a' a p' p); intro hnp, {
      -- different
      rewrite napmatch.ff_update_unused_CN hnp,
      rewrite hh3, rewrite hh2, rewrite hh1,
    }, {
      -- same
      napmatch_rewrites `hnp,
      generalize he : (hs_ctx3.c n a p) = e,
      rewrite he at hv hs_nv,
      cases hv, cases hs_nv,
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns (lg.attrN hl) hc hv, rewrite <- hh1 at hv,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) (bigstep.contained hc hs_ns) hv, rewrite <- hh2 at hv,
    have hnap : (napmatch cfg n' n a' a p' p = ff), {
      destruct (napmatch cfg n' n a' a p' p); intros hnap,
      assumption, napmatch_rewrites `hnap, rewrite hs_ismk at hv,
      cases hv,
    },
    rewrite <- hh1, rewrite <- hh2,
    rewrite napmatch.ff_update_unused_CN hnap,
    rewrite addN.c, rewrite writeAL.unused,
    -- intro hcc, ccases `hcc,
    have hh3 := bigstep.contained hc hs_ns,
    have hh4 := bigstep.contained hh3 hs_ps,
    have hh5 := fresh_for.contained hh4,
    rewrite <- hs_isfresh at hh5, exact hh5,
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih _ hc hv,
    destruct (napmatch _ n' n a' a p' p); intro hnp, {
      -- different
      rewrite napmatch.ff_update_unused_CN hnp,
      exact hh1,
    }, {
      -- same
      napmatch_rewrites `hnp,
      rewrite extendCN.match,
      have he : (hs_eC = hs_ctx1.c n a p), {
        cases hl, reflexivity,
      },
      rewrite he at hs_c,
      generalize he : (hs_ctx1.c n a p) = e,
      rewrite he at hv hs_c,
      cases hv,
      have hh := bigstep.val.same.v hs_c,
      cases hh, reflexivity,
    }, {
      cases hl, apply legal_context_axiom,
    }
  },
end









/-
       ###    ########  ########  ##    ##  ######
      ## ##   ##     ## ##     ## ###   ## ##    ##
     ##   ##  ##     ## ##     ## ####  ## ##
    ##     ## ##     ## ##     ## ## ## ##  ######
    ######### ##     ## ##     ## ##  ####       ##
    ##     ## ##     ## ##     ## ##   ### ##    ##
    ##     ## ########  ########  ##    ##  ######
-/
lemma addNs.addN.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg} {n:node} {l:list node},
  addN n (addNs c l) = addNs (addN n c) l
:= begin
  introv, induction l, {
    unfold addNs,
  }, {
    unfold addNs,
    rewrite addN.order.irrel, rewrite l_ih,
  },
end

lemma addNs.rewrite {cfg:saigaconfig}: ∀ {c1 c2:ctxN cfg} {l:list node},
  c1.N = c2.N ->
  (addNs c1 l).N = (addNs c2 l).N
:= begin
  introv, intros he,
  induction l, {
    unfold addNs, apply he,
  }, {
    unfold addNs,
    unfold addN,
    simp, rewrite l_ih,
  }
end

lemma addNs.concat {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg}
        {l1 l2:list node},
  c2.N = (addNs c1 l1).N ->
  (addNs c2 l2).N = (addNs c1 (l2 ++ l1)).N
:= begin
  introv, intros h,
  induction l2 generalizing l1 c1 c2,
  {
    unfold addNs,
    rewrite append_nil_left,
    rewrite h,
  }, {
    rewrite list.cons_append,
    unfold addNs, unfold addN, simp,

    have hh := l2_ih h,
    rewrite hh,
  },
end

lemma addNs.concat_simple {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {l1 l2:list node},
  addNs (addNs c l2) l1 = (addNs c (l1 ++ l2))
:= begin
  intros,
  induction l1 generalizing l2 c, {
    unfold addNs, simp,
  }, {
    unfold addNs,
    rewrite list.cons_append,
    unfold addNs, rewrite l1_ih,
  }
end

lemma addNs.concat2 {cfg:saigaconfig} : ∀ {c1 c2 c3:ctxN cfg}
        {l1 l2:list node},
  c2.N = (addNs c1 l1).N ->
  c3.N = (addNs c2 l2).N ->
  c3.N = (addNs c1 (l2 ++ l1)).N
:= begin
  introv, intros h1 h2,
  have h3 := addNs.concat h1,
  rewrite <- h2 at h3, apply h3,
end


lemma addN.addNs.contra {cfg:saigaconfig} : ∀ {c:ctxN cfg} {l:list node} {n:node},
  c.N = (addN n (addNs c l)).N ->
  false
:= begin
  introv, intros h,
  induction l, {
    unfold addNs at h,
    have hh1 := addN.contra h,
    apply hh1,
  }, {
    unfold addNs at h,
    have hh := l_ih h, apply hh,
  }
end

lemma addNs.nil {cfg:saigaconfig} : ∀ {c:ctxN cfg} {l:list node},
  c.N = (addNs c l).N ->
  l = list.nil
:= begin
  introv, intros he,
  cases l, {
    reflexivity,
  }, {
    unfold addNs at he,
    have hh := addN.addNs.contra he, cases hh,
  },
end


lemma bigstep.addNs {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg}
        {e:exp cfg} {T:Type} {v:T}
  (hs: c1 ⊨ e »» c2 ⊨ v),
  exists l:(list node),
    c2.N = (addNs c1 l).N
:= begin
  introv, intros hs, induction hs,
  { -- refl
    existsi list.nil,
    unfold addNs,
  }, { -- condT,
    cases hs_ih_cs with l1 hh1,
    cases hs_ih_ts with l2 hh2,
    have hh4 := addNs.concat2 hh1 hh2,
    existsi _, apply hh4,
  }, { -- condF
    cases hs_ih_cs with l1 hh1,
    cases hs_ih_fs with l2 hh2,
    have hh4 := addNs.concat2 hh1 hh2,
    existsi _, apply hh4,
  }, { -- app
    cases hs_ih_fs with l1 hh1,
    cases hs_ih_ps with l2 hh2,
    have hh4 := addNs.concat2 hh1 hh2,
    existsi _, apply hh4,
  }, { -- attrValue
    cases hs_ih_ns with l1 hh1,
    cases hs_ih_ps with l2 hh2,
    cases hs_ih_cs with l3 hh3,
    have hh4 := addNs.concat2 hh1 hh2,
    have hh5 := addNs.concat2 hh2 hh3,
    existsi _,
    apply addNs.concat2,
    assumption, assumption,
  }, { -- attrCache
    rewrite extendCN.N,
    cases hs_ih_ns with l1 hh1,
    cases hs_ih_ps with l2 hh2,
    cases hs_ih_cs with l3 hh3,
    have hh4 := addNs.concat2 hh1 hh2,
    have hh5 := addNs.concat2 hh2 hh3,
    existsi _,
    apply addNs.concat2,
    assumption, assumption,
  }, { -- attrHO
    rewrite extendCN.N,
    rewrite addN.writeAL.flip,
    rewrite writeAL.N,
    cases hs_ih_ns with l1 hh1,
    cases hs_ih_ps with l2 hh2,
    have hh3 := addNs.concat2 hh1 hh2,
    existsi (hs_n_f :: (l2 ++ l1)),
    unfold addNs, unfold addN, simp,
    rewrite hh3,
  }, { -- cache
    cases hs_ih with l1 hh1,
    rewrite extendCN.N,
    existsi _, apply hh1,
  },
end



/-
    ######## ##     ## ########  ######  ##    ##
    ##        ##   ##     ##    ##    ## ###   ##
    ##         ## ##      ##    ##       ####  ##
    ######      ###       ##    ##       ## ## ##
    ##         ## ##      ##    ##       ##  ####
    ##        ##   ##     ##    ##    ## ##   ###
    ######## ##     ##    ##     ######  ##    ##
-/


inductive napv {cfg:saigaconfig}
| mk : ∀ (n:node) (a:cfg.A) (p:cfg.ρ a) (v:cfg.τ a), napv

def extendCNs {cfg:saigaconfig} (c:ctxN cfg) : list napv -> ctxN cfg
| (list.nil) := c
| (list.cons (napv.mk n a p v) r) := extendCN cfg n a p v (extendCNs r)

lemma extendCNs.N {cfg:saigaconfig} : ∀ {c:ctxN cfg} {l:list _},
  (extendCNs c l).N = c.N
:= begin
  introv, induction l, {
    unfold extendCNs,
  }, {
    cases l_hd with n a p v,
    unfold extendCNs,
    rewrite extendCN.N,
    rewrite l_ih,
  }
end


lemma bigstep.middle.N2 : ∀ {cfg:_} {e1 e2:exp cfg}
        {c1 c2 c3:ctxN cfg} {T1 T2:Type} {v1:T1} {v2:T2},
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c3 ⊨ v2) ->
  c3.N = c1.N ->
  c2.N = c1.N
:= begin
  introv, intros hs1 hs2 he,
  have hh1 := bigstep.addNs hs1,
  cases hh1 with l1 hh1,
  have hh2 := bigstep.addNs hs2,
  cases hh2 with l2 hh2,
  have hh3 := addNs.concat2 hh1 hh2,
  rewrite addNs.rewrite hh1 at hh2,
  rewrite he at hh3,
  have hh4 := addNs.nil hh3,
  have hh5 := list.append_eq_nil hh4,
  cases hh5 with hh5 hh6,
  rewrite hh6 at hh1,
  unfold addNs at hh1,
  apply hh1,
end


@[reducible]
def idf (T:Type): T -> bool := λ v, bool.tt

lemma bigstep.joint_step {cfg:saigaconfig} : ∀ {c1 c2 c3:ctxN cfg}
        {e1 e2:exp cfg} {T1:Type} {T2:Type} {v1:T1} {v2:T2},
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c3 ⊨ v2) ->
  (c1 ⊨ (IFF ({{idf T1}} OF e1) THEN e2 ELSE e2) »» c3 ⊨ v2)
:= begin
  introv, intros hs1 hs2,
  -- existsi (IFF ({{idf T1}} OF e1) THEN e2 ELSE e2),
  constructor, rotate 1, apply hs2,
  have hh := @bigstep.app cfg T1 bool {{idf T1}} e1 c1 c1 c2 (idf T1) v1 _ hs1,
  unfold idf at hh, apply hh,
  apply bigstep.refl,
end

lemma bigstep.middle3.N2 : ∀ {cfg:_} {e1 e2 e3:exp cfg} {c1 c2 c3 c4:ctxN cfg}
        {T1 T2 T3:Type} {v1:T1} {v2:T2} {v3:T3},
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c3 ⊨ v2) ->
  (c3 ⊨ e3 »» c4 ⊨ v3) ->
  c4.N = c1.N ->
  c3.N = c1.N ∧ c2.N = c1.N
:= begin
  introv, intros hs1 hs2 hs3 he,
  have hh1 := bigstep.middle.N2 (bigstep.joint_step hs1 hs2) hs3 he,
  have hh2 := bigstep.middle.N2 hs1 (bigstep.joint_step hs2 hs3) he,
  split; assumption,
end


lemma bigstep.middle3.N : ∀ {cfg:_} {e1 e2 e3:exp cfg} {c1 c2 c3:ctxN cfg}
        {T1 T2 T3:Type} {v1:T1} {v2:T2} {v3:T3},
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c3 ⊨ v2) ->
  (c3 ⊨ e3 »» c1 ⊨ v3) ->
  c3.N = c1.N ∧ c2.N = c1.N
:= begin
  introv, intros hs1 hs2 hs3,
  have hh1 := bigstep.middle3.N2 hs1 hs2 hs3 rfl, apply hh1,
end

lemma addN.extendCNs.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n:node} {l:list _},
    addN n (extendCNs c l) = extendCNs (addN n c) l
:= begin
  introv,
  induction l, {
    unfold extendCNs,
  },
  apply ctxN.mk.inj2, {
    rewrite addN.c,
    funext n'' a'' p'',
    cases l_hd with n a p v,
    destruct (napmatch cfg n n'' a a'' p p''); intros hnap, {
      unfold extendCNs,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite <- l_ih, rewrite addN.c,
    }, {
      napmatch_rewrites `hnap,
      unfold extendCNs,
      rewrite extendCN.match,
      rewrite extendCN.match,
    },
  }, {
    unfold addN, simp,
    rewrite extendCNs.N,
    rewrite extendCNs.N,
  },
end

lemma extendCNs.concat: ∀ {cfg:_} {c:ctxN cfg} {l1 l2:list napv},
  extendCNs (extendCNs c l1) l2 = extendCNs c (l2 ++ l1)
:= begin
  intros, induction l2 generalizing l1,
  {
    simp, unfold extendCNs,
  }, {
    cases l2_hd with n a p v,
    unfold extendCNs, simp,
    unfold extendCNs, rewrite l2_ih,
  },
end


lemma extendCNs.ff_unused : ∀ {cfg:_} {c:ctxN cfg} {l:list napv} {n' a' p'} {n a p v},
  napmatch _ n n' a a' p p' = ff ->
  (extendCNs (extendCN cfg n a p v c) l).c n' a' p'
  = (extendCNs c l).c n' a' p'
:= begin
  introv, intros hnp,
  induction l,
  {
    unfold extendCNs, apply napmatch.ff_update_unused hnp,
  }, {
    cases l_hd with n'' a'' p'' v'',
    unfold extendCNs,
    destruct (napmatch _ n'' n' a'' a' p'' p'); intro hnp2, {
      -- different
      rewrite napmatch.ff_update_unused_CN hnp2,
      rewrite napmatch.ff_update_unused_CN hnp2,
      apply l_ih,
    }, {
      -- same
      napmatch_rewrites `hnp2,
      rewrite extendCN.match,
      rewrite extendCN.match,
    }
  }
end

lemma extendCNs.redundant : ∀ {cfg:_} {c:ctxN cfg} {l:list napv},
  extendCNs (extendCNs c l) l = extendCNs c l
:= begin
  introv,
  induction l generalizing c,
  {
    unfold extendCNs,
  }, {
    apply ctxN.mk.inj2, {
      cases l_hd with n a p v,
      unfold extendCNs,
      funext,
      destruct (napmatch _ n n' a a' p p'); intro hnp, {
        -- different
        rewrite napmatch.ff_update_unused_CN hnp,
        rewrite napmatch.ff_update_unused_CN hnp,
        rewrite extendCNs.ff_unused hnp,
        rewrite l_ih,
      }, {
        -- same
        napmatch_rewrites `hnp,
        rewrite extendCN.match,
        rewrite extendCN.match,
      },
    }, {
      rewrite extendCNs.N,
    }
  },
end



lemma bigstep.extendCNs.remains : ∀ {cfg:saigaconfig} {l:list napv} {e:exp cfg} {c1 c2:ctxN cfg} {T:Type} {v':T},
  legal (extendCNs c1 l) e ->
  ((extendCNs c1 l) ⊨ e »» c2 ⊨ v') ->
  c2.N = (extendCNs c1 l).N ->
  c2 = (extendCNs c2 l)
:= begin
  introv, intros hl hs hn,
  generalize he' : (extendCNs c1 l) = ec,
  have he := eq.symm he', clear he',
  rewrite <- he at hs,
  rewrite <- he at hn,
  induction hs generalizing c1 l,
  { -- refl
    rewrite he, rewrite extendCNs.redundant,
  }, { -- condT,
    rewrite hn at *,
    have hh1 := bigstep.middle.N2 hs_cs hs_ts hn,
    rewrite hh1 at *,
    have hh2 := hs_ih_cs rfl (lg.condC hl) he,
    have hh3 := hs_ih_ts rfl (lg.condT hl _) hh2,
    apply hh3, rewrite <- hh2, rewrite <- he, assumption,
  }, { -- condF,
    rewrite hn at *,
    have hh1 := bigstep.middle.N2 hs_cs hs_fs hn,
    rewrite hh1 at *,
    have hh2 := hs_ih_cs rfl (lg.condC hl) he,
    have hh3 := hs_ih_fs rfl (lg.condF hl _) hh2,
    apply hh3, rewrite <- hh2, rewrite <- he, assumption,
  }, { -- app,
    rewrite hn at *,
    have hh1 := bigstep.middle.N2 hs_fs hs_ps hn,
    rewrite hh1 at *,
    have hh2 := hs_ih_fs rfl (lg.appF hl) he,
    have hh3 := hs_ih_ps rfl (lg.appP hl _) hh2,
    apply hh3, rotate 2, rewrite <- hh2, rewrite <- he, assumption,
  }, { -- attrValue
    rewrite hn at *,
    have hh1 := bigstep.middle3.N2 hs_ns hs_ps hs_cs hn,
    splith `hh1,
    rewrite hn at *,
    rewrite hh1a at *, rewrite hh1b at *,
    have hh2 := hs_ih_ns rfl (lg.attrN hl) he,
    have hh3 := hs_ih_ps rfl (lg.attrP hl _) hh2,
    rotate 3,rewrite <- hh2, rewrite <- he, assumption,
    have hh4 := hs_ih_cs rfl _ hh3,
    apply hh4, rewrite <- hh3,
    apply legal_context_axiom,
    -- apply hs_ih_cs rfl _ (hs_ih_ps rfl (lg.attrP hl) (hs_ih_ns rfl (lg.attrN hl) he)),
    -- rewrite <- hs_ih_ps rfl, apply legal_context_axiom,
    -- apply lg.attrP hl, apply hs_ih_ns rfl, apply lg.attrN hl, apply he,
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v],
    rewrite hn at *,
    have hh1 := bigstep.middle3.N2 hs_ns hs_ps hs_cs hn,
    splith `hh1,
    rewrite extendCN.N at hn, rewrite hn at *,
    rewrite hh1a at *, rewrite hh1b at *,
    have hh1 := hs_ih_ns rfl (lg.attrN hl) he,
    have hh2 := hs_ih_ps rfl (lg.attrP hl _) hh1,
    rotate 3, rewrite <- hh1, rewrite <- he, assumption,
    have hh3 := hs_ih_cs rfl _ hh2, rotate 1, {
      rewrite <- hh2,
      apply legal_context_axiom,
    },
    apply ctxN.mk.inj2, rotate 1, {
      rewrite extendCN.N,
      rewrite extendCNs.N,
      rewrite extendCN.N,
    },
    funext n' a' p',
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      -- different
      rewrite napmatch.ff_update_unused_CN hnp,
      rewrite extendCNs.ff_unused hnp,
      rewrite <- hh3,
    }, {
      -- same
      napmatch_rewrites `hnp,
      rewrite extendCN.match,
      clear hs_ih_ns hs_ih_ps hs_ih_cs hnp,
      clear hs_ns hs_ps hh1 hh3,
      clear hh1a hh1b hn,
      clear he hs_ctx1 hs_nv hs_ctx2 hl c1 ec v' T hs_eN hs_eP e c2,
      have hh : ((hs_ctx3.c) n a p = (extendCNs hs_ctx3 l).c n a p), {
        rewrite <- hh2,
      },
      rewrite hh at hs_cs, clear hh2 hh,
      {
        induction l generalizing n a p v hs_ctx3 hs_ctx4,
        {
          unfold extendCNs,
          rewrite extendCN.match,
        }, {
          cases l_hd with n' a' p' v',
          unfold extendCNs,
          destruct (napmatch _ n' n a' a p' p); intro hnp, {
            -- different
            rewrite napmatch.ff_update_unused_CN hnp,
            apply l_ih,
            unfold extendCNs at hs_cs,
            rewrite napmatch.ff_update_unused_CN hnp at hs_cs,
            apply hs_cs,
          }, {
            -- same
            napmatch_rewrites `hnp,
            rewrite extendCN.match,
            unfold extendCNs at hs_cs,
            rewrite extendCN.match at hs_cs,
            have hh := bigstep.val.same.v hs_cs,
            cases hh, reflexivity,
          },
        },
      },
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n,`a,`p],
    exfalso,
    rewrite extendCN.N at hn,
    rewrite addN.writeAL.flip at hn,
    rewrite writeAL.N at hn,
    have hh1 := bigstep.addNs hs_ns,
    have hh2 := bigstep.addNs hs_ps,
    cases hh1 with l1 hh1,
    cases hh2 with l2 hh2,
    rewrite <- addNs.rewrite hn at hh1,
    rewrite addNs.rewrite hh1 at hh2,
    rewrite <- addNs.addN.flip at hh2,
    rewrite <- addNs.addN.flip at hh2,
    have hh := addN.addNs.contra hh2, cases hh,
    assumption, assumption,
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v],
    rewrite extendCN.N at hn, rewrite hn at *,
    have hh : (hs_eC = hs_ctx1.c n a p), {
      rewrite <- he at hl, cases hl,
      reflexivity,
    },
    rewrite hh at hs_c, clear hh,
    apply ctxN.mk.inj2, rotate 1, {
      rewrite extendCN.N,
      rewrite extendCNs.N,
      rewrite extendCN.N,
    },
    funext n' a' p',
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      -- different
      rewrite napmatch.ff_update_unused_CN hnp,
      rewrite extendCNs.ff_unused hnp,
      rewrite hs_ih rfl _ he,
      rewrite extendCNs.redundant,
      cases hl, 
      apply legal_context_axiom,
    }, {
      -- same
      napmatch_rewrites `hnp,
      rewrite extendCN.match,
      have hh : (hs_ctx1.c n a p = (extendCNs c1 l).c n a p), {
        rewrite he,
      },
      rewrite hh at hs_c,
      clear hs_ih hnp he hh,
      clear hl ec v' T e hs_eC c2,
      {
        induction l generalizing n a p v hs_ctx1 hs_ctx2 c1,
        {
          unfold extendCNs,
          rewrite extendCN.match,
        }, {
          cases l_hd with n' a' p' v',
          unfold extendCNs,
          destruct (napmatch _ n' n a' a p' p); intro hnp, {
            -- different
            rewrite napmatch.ff_update_unused_CN hnp,
            unfold extendCNs at hs_c,
            rewrite napmatch.ff_update_unused_CN hnp at hs_c,
            apply l_ih hn hs_c,
          }, {
            -- same
            napmatch_rewrites `hnp,
            rewrite extendCN.match,
            unfold extendCNs at hs_c,
            rewrite extendCN.match at hs_c,
            have hh := bigstep.val.same.v hs_c,
            cases hh, reflexivity,
          },
        },
      },
    },
  },
end

lemma bigstep.extendCNs.remains2 : ∀ {cfg:saigaconfig} {l:list napv} {e:exp cfg} {c1 c2:ctxN cfg} {T:Type} {v':T},
  legal (extendCNs c1 l) e ->
  ((extendCNs c1 l) ⊨ e »» c2 ⊨ v') ->
  c2.N = c1.N ->
  c2 = (extendCNs c2 l)
:= begin
  introv, intros hl hs he,
  apply bigstep.extendCNs.remains hl hs,
  rewrite extendCNs.N, apply he,
end


lemma bigstep.extendCNs: ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg}
        {e:exp cfg} {T:Type} {v:T},
  c2.N = c1.N ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  exists l:(list napv),
    c2 = extendCNs c1 l
:= begin
  introv, intros hn hs,
  induction hs, { -- refl
    existsi (list.nil),
    unfold extendCNs,
  }, { -- condT
    renames [`hs_ih_cs,`hs_ih_ts] [`hh1,`hh2],
    have hh3 := bigstep.middle.N2 hs_cs hs_ts hn,
    rewrite hh3 at *, rewrite hn at *,
    cases hh1 rfl with l1 hh1, cases hh2 rfl with l2 hh2,
    existsi (l2 ++ l1),
    rewrite hh1 at hh2,
    rewrite extendCNs.concat at hh2,
    exact hh2,
  }, { -- condF
    renames [`hs_ih_cs,`hs_ih_fs] [`hh1,`hh2],
    have hh3 := bigstep.middle.N2 hs_cs hs_fs hn,
    rewrite hh3 at *, rewrite hn at *,
    cases hh1 rfl with l1 hh1, cases hh2 rfl with l2 hh2,
    existsi (l2 ++ l1),
    rewrite hh1 at hh2,
    rewrite extendCNs.concat at hh2,
    exact hh2,
  }, { -- app
    renames [`hs_ih_fs,`hs_ih_ps] [`hh1,`hh2],
    have hh3 := bigstep.middle.N2 hs_fs hs_ps hn,
    rewrite hh3 at *, rewrite hn at *,
    cases hh1 rfl with l1 hh1, cases hh2 rfl with l2 hh2,
    existsi (l2 ++ l1),
    rewrite hh1 at hh2,
    rewrite extendCNs.concat at hh2,
    exact hh2,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v],
    have hh1 := bigstep.middle3.N2 hs_ns hs_ps hs_cs hn,
    cases hh1 with hh1 hh2,
    rewrite hh1 at *, rewrite hh2 at *, rewrite hn at *,
    have hh3 := hs_ih_ns rfl,
    have hh4 := hs_ih_ps rfl,
    have hh5 := hs_ih_cs rfl,
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    cases hh3 with l1 hh3,
    cases hh4 with l2 hh4,
    cases hh5 with l3 hh5,
    existsi (l3 ++ l2 ++ l1),
    rewrite hh3 at hh4,
    rewrite hh4 at hh5,
    rewrite extendCNs.concat at hh5,
    rewrite extendCNs.concat at hh5,
    rewrite hh5,
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v],
    rewrite extendCN.N at hn,
    have hh1 := bigstep.middle3.N2 hs_ns hs_ps hs_cs hn,
    cases hh1 with hh1 hh2,
    rewrite hh1 at *, rewrite hh2 at *, rewrite hn at *,
    have hh3 := hs_ih_ns rfl,
    have hh4 := hs_ih_ps rfl,
    have hh5 := hs_ih_cs rfl,
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    cases hh3 with l1 hh3,
    cases hh4 with l2 hh4,
    cases hh5 with l3 hh5,
    existsi ((napv.mk n a p v) :: l3 ++ l2 ++ l1),
    rewrite hh3 at hh4,
    rewrite hh4 at hh5,
    rewrite extendCNs.concat at hh5,
    rewrite extendCNs.concat at hh5,
    rewrite hh5, simp, unfold extendCNs,
  }, { -- attrHO
    rewrite extendCN.N at hn,
    rewrite addN.writeAL.flip at hn,
    rewrite writeAL.N at hn,
    have hh1 := bigstep.addNs hs_ns,
    have hh2 := bigstep.addNs hs_ps,
    cases hh1 with l1 hh1,
    cases hh2 with l2 hh2,
    rewrite <- addNs.rewrite hn at hh1,
    rewrite addNs.rewrite hh1 at hh2,
    rewrite <- addNs.addN.flip at hh2,
    rewrite <- addNs.addN.flip at hh2,
    -- have hh3 : (hs_ctx3.N = (addN hs_n_f (addNs (addNs hs_ctx3 l1) l2)).N)
    rewrite addNs.concat_simple at hh2,
    have hh := addN.addNs.contra hh2, cases hh,
  }, { -- cache
    rewrite extendCN.N at hn,
    have hh1 := hs_ih hn,
    cases hh1 with l1 hh1,
    existsi (napv.mk hs_n hs_a hs_p hs_v :: l1),
    unfold extendCNs, rewrite hh1,
  },
end


lemma bigstep.extendCN.remains : ∀ {cfg:saigaconfig} {n a p} {v:cfg.τ a} {e:exp cfg} {c1 c2:ctxN cfg} {T:Type} {v':T},
  contained n (extendCN _ n a p v c1).N ->
  legal (extendCN _ n a p v c1) e ->
  (extendCN _ n a p v c1 ⊨ e »» c2 ⊨ v') ->
  c2 = extendCN _ n a p v c2
:= begin
  introv, intros hc hl hs,
  generalize he' : (extendCN _ n a p v c1) = ec1,
  have he := eq.symm he', clear he',
  rewrite <- he at hs,
  induction hs generalizing c1,
  { -- refl
    rewrite he, rewrite extendCN.redundant,
  }, { -- condT
    have hh1 := hs_ih_cs hc (lg.condC hl) he,
    have hh2 := hs_ih_ts _ (lg.condT hl _) hh1,
    apply hh2, rewrite he at hs_cs, rewrite <- hh1,
    apply bigstep.contained hc hs_cs,
    rewrite <- he, rewrite <- hh1,
    assumption,
  }, { -- condF
    have hh1 := hs_ih_cs hc (lg.condC hl) he,
    have hh2 := hs_ih_fs _ (lg.condF hl _) hh1,
    apply hh2, rewrite he at hs_cs, rewrite <- hh1,
    apply bigstep.contained hc hs_cs,
    rewrite <- he, rewrite <- hh1,
    assumption,
  }, { -- app
    have hh1 := hs_ih_fs hc (lg.appF hl) he,
    have hh2 := hs_ih_ps _ (lg.appP hl _) hh1,
    apply hh2, rewrite he at hs_fs, rewrite <- hh1,
    apply bigstep.contained hc hs_fs, rotate 2,
    rewrite <- he, rewrite <- hh1,
    assumption,
  }, { -- attrValue
    have hh1 := hs_ih_ns hc (lg.attrN hl) he,
    have hh2 := hs_ih_ps _ (lg.attrP hl _) hh1,
    rotate 4, rewrite <- he, rewrite <- hh1, assumption,
    rotate 1, {
      rewrite <- hh1,
      apply bigstep.contained _ hs_ns,
      rewrite he, apply hc,
    },
    clear hs_ih_ns hs_ih_ps,
    have hh3 := hs_ih_cs _ _ hh2, apply hh3, {
      rewrite <- hh2, apply bigstep.contained _ hs_ps,
      apply bigstep.contained _ hs_ns,
      rewrite he, apply hc,
    },
    rewrite <- hh2, apply legal_context_axiom,
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns hc (lg.attrN hl) he,
    have hh2 := hs_ih_ps _ (lg.attrP hl _) hh1, rotate 4,
    rewrite <- he, rewrite <- hh1, assumption,
    have hh3 := hs_ih_cs _ _ hh2, 
    rotate 1, {
      rewrite <- hh2,
      apply bigstep.contained _ hs_ps,
      apply bigstep.contained _ hs_ns,
      rewrite he, apply hc,
    }, rotate 1, {
      rewrite <- hh1,
      apply bigstep.contained _ hs_ns,
      rewrite he, apply hc,
    }, rotate 1, {
      rewrite <- hh2,
      apply legal_context_axiom,
    },
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      -- different
      rewrite napmatch.ff_extend_flip_CN hnp,
      rewrite <- hh3,
    }, {
      -- same
      napmatch_rewrites `hnp,
      rewrite hh2 at hs_cs,
      rewrite extendCN.match at hs_cs,
      have hh := bigstep.val.same.v hs_cs,
      ccases `hh,
      rewrite extendCN.redundant,
    },
  }, { -- attrHO
    have hh1 := hs_ih_ns hc (lg.attrN hl) he,
    have hh2 := hs_ih_ps _ (lg.attrP hl _) hh1,
    rotate 4, rewrite <- he, rewrite <- hh1, assumption,
    rotate 1, {
      rewrite <- hh1,
      apply bigstep.contained _ hs_ns,
      rewrite he, apply hc,
    },
    clear hs_ih_ns hs_ih_ps,
    rewrite <- he at hc,
    have hh3 := bigstep.contained hc  hs_ns,
    have hh4 := bigstep.contained hh3 hs_ps,
    have hh5 := fresh_for.contained hh4,
    rewrite <- hs_isfresh at hh5,
    have hh6 := bigstep.contained hh3 hs_ps,
    have hh7 := fresh_for.contained hh6,
    rewrite <- hs_isfresh at hh7,
    apply ctxN.mk.inj2, {
      funext n'' a'' p'',
      destruct (napmatch cfg hs_n n'' hs_a a'' hs_p p''); intros hnap1, {
        rewrite napmatch.ff_update_unused_CN hnap1,
        rewrite addN.c,
        destruct (napmatch cfg n n'' a a'' p p''); intros hnap2, {
          rewrite napmatch.ff_update_unused_CN hnap2,
          rewrite napmatch.ff_update_unused_CN hnap1,
          rewrite addN.c,
        }, {
          napmatch_rewrites `hnap2,
          rewrite extendCN.match,
          rewrite writeAL.unused hh5,
          rewrite hh2, rewrite extendCN.match,
        },
      }, {
        napmatch_rewrites `hnap1,
        rewrite extendCN.match,
        destruct (napmatch cfg n hs_n a hs_a p hs_p); intros hnap2, {
          rewrite napmatch.ff_update_unused_CN hnap2,
          rewrite extendCN.match,
        }, {
          napmatch_rewrites `hnap2,
          rewrite hh2 at hs_ismk,
          rewrite extendCN.match at hs_ismk,
          cases hs_ismk,
        }
      }
    }, {
      rewrite extendCN.N, rewrite extendCN.N,
      rewrite extendCN.N,
    },
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih hc _ he, clear hs_ih,
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      -- different
      rewrite napmatch.ff_extend_flip_CN hnp,
      rewrite <- hh1,
    }, {
      -- same
      napmatch_rewrites `hnp,
      rewrite extendCN.redundant,
      have hl : (hs_eC = {{v}}), {
        cases hl,
        rewrite napmatch.id,
      }, ccases `hl,
      have hh := bigstep.val.same.v hs_c,
      ccases `hh, reflexivity,
    }, {
      cases hl,
      have hh : (bite (napmatch cfg n n' a a' p p') {{v}} (c1.c n' a' p') = (extendCN cfg n a p v c1).c n' a' p'), {
        reflexivity,
      },
      rewrite hh, rewrite <- he,
      apply legal_context_axiom,
    },
  }, 
end





/-
    ##     ## #### ########  ########  ##       ########
    ###   ###  ##  ##     ## ##     ## ##       ##
    #### ####  ##  ##     ## ##     ## ##       ##
    ## ### ##  ##  ##     ## ##     ## ##       ######
    ##     ##  ##  ##     ## ##     ## ##       ##
    ##     ##  ##  ##     ## ##     ## ##       ##
    ##     ## #### ########  ########  ######## ########
-/



lemma bigstep.middle.N : ∀ {cfg:_} {e1 e2:exp cfg}
        {c1 c2:ctxN cfg} {T1 T2:Type} {v1:T1} {v2:T2},
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c1 ⊨ v2) ->
  c2.N = c1.N
:= begin
  introv, intros hs1 hs2,
  have hh1 := bigstep.addNs hs1,
  cases hh1 with l1 hh1,
  have hh2 := bigstep.addNs hs2,
  cases hh2 with l2 hh2,
  have hh3 := addNs.concat2 hh1 hh2,
  rewrite addNs.rewrite hh1 at hh2,
  have hh4 := addNs.nil hh3,
  have hh5 := list.append_eq_nil hh4,
  cases hh5 with hh5 hh6,
  rewrite hh6 at hh1,
  unfold addNs at hh1,
  apply hh1,
end

lemma bigstep.middle.ctx : ∀ {cfg:_} {e1 e2:exp cfg} {c1 c2:ctxN cfg} {T1 T2:Type} {v1:T1} {v2:T2},
  legal c1 e1 ->
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c1 ⊨ v2) ->
  c2 = c1
:= begin
  introv, intros hl hs1 hs2,
  have hh1 := bigstep.middle.N hs1 hs2,
  have hh2 := bigstep.extendCNs hh1 hs1, cases hh2 with l1 hh2,
  have hh3 := bigstep.extendCNs (eq.symm hh1) hs2, cases hh3 with l2 hh3,
  rewrite hh3 at hs1, rewrite hh3 at hl,
  have hh4 := bigstep.extendCNs.remains2 hl hs1 rfl,
  rewrite <- hh4 at hh3, rewrite hh3,
end

lemma bigstep.middle3.ctx : ∀ {cfg:_} {e1 e2 e3:exp cfg} {c1 c2 c3:ctxN cfg} {T1 T2 T3:Type} {v1:T1} {v2:T2} {v3:T3},
  legal c1 e1 ->
  legal c2 e2 ->
  (c1 ⊨ e1 »» c2 ⊨ v1) ->
  (c2 ⊨ e2 »» c3 ⊨ v2) ->
  (c3 ⊨ e3 »» c1 ⊨ v3) ->
  c3 = c1 ∧ c2 = c1
:= begin
  introv, intros hl1 hl2 hs1 hs2 hs3,
  have hh0 := bigstep.middle3.N hs1 hs2 hs3,
  splith `hh0,
  have hh0c : c3.N = c2.N := by {rewrite hh0a, rewrite hh0b,},
  have hh : (c3 = c1), {
    have hh1 := bigstep.extendCNs hh0b hs1, cases hh1 with l1 hh1,
    have hh2 := bigstep.extendCNs hh0c hs2, cases hh2 with l2 hh2,
    have hh3 := bigstep.extendCNs (eq.symm hh0a) hs3, cases hh3 with l3 hh3,

    rewrite hh1 at hl2,
    rewrite hh3 at hl1,
    rewrite hh3 at hs1, rewrite hh1 at hs2, rewrite hh2 at hs3,
    have hh1b := bigstep.extendCNs.remains hl1 hs1 _, rotate 1, {
      rewrite extendCNs.N, rewrite hh0c,
    },
    rewrite <- hh3 at hs1, rewrite <- hh1 at hs2, rewrite <- hh2 at hs3,
    rewrite hh1b at hs2,
    have hh4 := bigstep.extendCNs.remains _ hs2,
    rewrite hh4, rewrite hh3,
    rewrite <- hh1b, apply hh0c,
    rewrite <- hh1b, rewrite hh1, apply hl2,
  }, {
    apply (and2 hh),
    ccases `hh,
    apply bigstep.middle.ctx hl1 hs1 hs2,
  }
end











lemma bigstep.HOwrite.unused.aux {cfg:saigaconfig} : ∀ {c c':ctxN cfg}
        {n:node} {a:cfg.A} {p:cfg.ρ a} {e:exp cfg} {T:Type} {v':T}
        {n_f:node} {al al':_} {v:cfg.τ a},
  legal c e ->  
  (c ⊨ e »» c' ⊨ v') ->
  c' = c ->
  c.c n a p = exp.eMk al' ->
  n_f = fresh_for c ->
  (extendCN cfg n a p v (addN n_f (writeAL c n_f al)) ⊨
   e »» extendCN cfg n a p v (addN n_f (writeAL c n_f al)) ⊨ v')
:= begin
  introv, intros hl hs he hm hf,
  induction hs,
  { -- refl
    apply bigstep.refl,
  }, { -- condT
    ccases `he,
    have hh1 := bigstep.middle.ctx (lg.condC hl) hs_cs hs_ts,
    ccases `hh1,
    have hh1 := hs_ih_cs (lg.condC hl) rfl hm hf, clear hs_ih_cs,
    have hh2 := hs_ih_ts (lg.condT hl hs_cs) rfl hm hf, clear hs_ih_ts,
    apply bigstep.condT hh1 hh2,
  }, { -- condF
    ccases `he,
    have hh1 := bigstep.middle.ctx (lg.condC hl) hs_cs hs_fs,
    ccases `hh1,
    have hh1 := hs_ih_cs (lg.condC hl) rfl hm hf, clear hs_ih_cs,
    have hh2 := hs_ih_fs (lg.condF hl hs_cs) rfl hm hf, clear hs_ih_fs,
    apply bigstep.condF hh1 hh2,
  }, { -- app
    ccases `he,
    have hh1 := bigstep.middle.ctx (lg.appF hl) hs_fs hs_ps,
    ccases `hh1,
    have hh1 := hs_ih_fs (lg.appF hl) rfl hm hf, clear hs_ih_fs,
    have hh2 := hs_ih_ps (lg.appP hl hs_fs) rfl hm hf, clear hs_ih_ps,
    apply bigstep.app hh1 hh2,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    ccases `he,    
    have hh1 := bigstep.middle3.ctx (lg.attrN hl) (lg.attrP hl hs_ns) hs_ns hs_ps hs_cs,
    doublecases `hh1,
    have hh1 := hs_ih_ns (lg.attrN hl) rfl hm hf, clear hs_ih_ns,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) rfl hm hf, clear hs_ih_ps,
    have hh3 := hs_ih_cs legal_context_axiom rfl hm hf, clear hs_ih_cs,

    have hh4 : ((extendCN cfg n a p v (addN n_f (writeAL hs_ctx1 n_f al))).c n' a' p' = hs_ctx1.c n' a' p'), {
      destruct (napmatch cfg n n' a a' p p'); intros hnap, rotate 1, {
        napmatch_rewrites `hnap,
        rewrite extendCN.match, rewrite hm at hs_hv,
        cases hs_hv,
      },
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c,
      rewrite writeAL.unused,
      have hhc := lg.attrCont hl hs_ns,
      have hh5 := fresh_for.contained hhc,
      rewrite <- hf at hh5, assumption,
    },
    rewrite <- hh4 at hh3,
    apply bigstep.attrValue _ hh1 hh2 hh3, {
      rewrite hh4, assumption,
    },
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite he at *,
    rewrite <- he at hs_ns,
    have hh1 := bigstep.extendCN.remains _ _ hs_ns, rotate 1, {
      rewrite extendCN.N,
      apply bigstep.contained _ hs_cs,
      apply bigstep.contained _ hs_ps,
      rewrite he at hs_ns,
      apply lg.attrCont hl hs_ns,
    }, {
      rewrite he at hs_ns,
      rewrite he,
      apply lg.attrN hl,
    },
    rewrite hh1 at hs_ps,
    have hh2 := bigstep.extendCN.remains _ _ hs_ps, rotate 1, {
      rewrite extendCN.N,
      rewrite he at hs_ns,
      apply lg.attrCont hl hs_ns,
    }, {
      rewrite he at hs_ns,
      rewrite <- hh1,
      apply lg.attrP hl hs_ns,
    },
    rewrite hh2 at hs_nv,
    rewrite extendCN.match at hs_nv,
    cases hs_nv,
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    clear hs_ih_ns hs_ih_ps,
    rewrite he at *,
    have hh : ((extendCN cfg n' a' p' (eq.mpr hs_tp_a hs_n_f) (addN hs_n_f (writeAL hs_ctx3 hs_n_f (hs_fl hs_n_f)))).N = hs_ctx1.N), {
      rewrite he,
    },
    rewrite extendCN.N at hh,
    rewrite addN.writeAL.flip at hh,
    rewrite writeAL.N at hh,
    have hh1 := bigstep.addNs hs_ns,
    have hh2 := bigstep.addNs hs_ps,
    cases hh1 with l1 hh1,
    cases hh2 with l2 hh2,
    rewrite <- addNs.rewrite hh at hh1,
    rewrite addNs.rewrite hh1 at hh2,
    rewrite <- addNs.addN.flip at hh2,
    rewrite <- addNs.addN.flip at hh2,
    have hh3 := addN.addNs.contra hh2, cases hh3,
    assumption, assumption,
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite <- he at hs_c,
    have hh1 := bigstep.extendCN.remains _ _ hs_c, rotate 1, {
      rewrite he, cases hl, assumption,
    }, {
      rewrite he, cases hl,
      apply legal_context_axiom,
    },
    rewrite <- hh1 at he, ccases `he,
    have hh2 : (extendCN cfg n a p v (addN n_f (writeAL hs_ctx1 n_f al)) = extendCN cfg n' a' p' v'' (extendCN cfg n a p v (addN n_f (writeAL hs_ctx1 n_f al)))), {
      rewrite extendCN.HOwrite.flip2 _ _, {
        rewrite <- hh1,
      }, {
        destruct (napmatch cfg n' n a' a p' p); intros hnap, assumption, {
          napmatch_rewrites `hnap,
          rewrite hh1 at hm,
          rewrite extendCN.match at hm,
          cases hm,
        },
      }, {
        cases hl,
        have hh2 := fresh_for.contained hl_a_1,
        rewrite <- hf at hh2, assumption,
      }
    },

    have hh3 : (extendCN cfg n a p v (addN n_f (writeAL hs_ctx1 n_f al)) ⊨
    n' / a' / p' ;=hs_eC »»
    extendCN cfg n' a' p' v'' (extendCN cfg n a p v (addN n_f (writeAL hs_ctx1 n_f al))) ⊨
    v''), {
      apply bigstep.cache,
      apply hs_ih _ rfl hm hf, {
        cases hl, apply legal_context_axiom,
      },
    },
    rewrite <- hh2 at hh3, apply hh3,
  },
end

lemma bigstep.HOwrite.unused {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n:node} {a:cfg.A} {p:cfg.ρ a} {e:exp cfg} {T:Type} {v':T}
        {n_f:node} {al al':_} {v:cfg.τ a},
  legal c e ->  
  (c ⊨ e »» c ⊨ v') ->
  c.c n a p = exp.eMk al' ->
  n_f = fresh_for c ->
  (extendCN cfg n a p v (addN n_f (writeAL c n_f al)) ⊨
   e »» extendCN cfg n a p v (addN n_f (writeAL c n_f al)) ⊨ v')
:= begin
  introv, intros hl hs hm hf,
  apply bigstep.HOwrite.unused.aux hl hs rfl hm hf,
end




























/-
     ######  ########  ######## ########  ##     ##
    ##    ## ##     ## ##       ##     ## ###   ###
    ##       ##     ## ##       ##     ## #### ####
    ##       ########  ######   ########  ## ### ##
    ##       ##        ##       ##   ##   ##     ##
    ##    ## ##        ##       ##    ##  ##     ##
     ######  ##        ######## ##     ## ##     ##
-/

lemma bigstep.cacheperm.crux_aux : ∀ {cfg:_} {c3 c3' c4:ctxN cfg} {e e2:exp cfg}
        {T T2:Type} {v:T} {v2:T2},
  legal c3 e ->
  legal c3 e2 ->
  (c3 ⊨ e2 »» c4 ⊨ v2) ->
  (c3 ⊨ e »» c3' ⊨ v) ->
  c3' = c3 ->
  (c4 ⊨ e »» c4 ⊨ v)
:= begin
  introv, intros hl1 hl2 hs1 hs2 he,
  induction hs2 generalizing e2 v2 T2 c4,
  { -- refl
    apply bigstep.refl,
  }, { -- condT
    ccases `he,
    have he2 := bigstep.middle.ctx (lg.condC hl1) hs2_cs hs2_ts, ccases `he2,
    have ih1 := @hs2_ih_cs (lg.condC hl1) rfl,
    have ih2 := @hs2_ih_ts (lg.condT hl1 hs2_cs) rfl,
    apply bigstep.condT (ih1 hl2 hs1) (ih2 hl2 hs1),
  }, { -- condF
    ccases `he,
    have he2 := bigstep.middle.ctx (lg.condC hl1) hs2_cs hs2_fs, ccases `he2,
    have ih1 := @hs2_ih_cs (lg.condC hl1) rfl,
    have ih2 := @hs2_ih_fs (lg.condF hl1 hs2_cs) rfl,
    apply bigstep.condF (ih1 hl2 hs1) (ih2 hl2 hs1),
  }, { -- app
    ccases `he,
    have he2 := bigstep.middle.ctx (lg.appF hl1) hs2_fs hs2_ps, ccases `he2,
    have ih1 := @hs2_ih_fs (lg.appF hl1) rfl,
    have ih2 := @hs2_ih_ps (lg.appP hl1 hs2_fs) rfl,
    apply bigstep.app (ih1 hl2 hs1) (ih2 hl2 hs1),
  }, { -- attrValue
    renames [`hs2_n, `hs2_a, `hs2_p, `hs2_v] [`n,`a,`p,`v],
    rewrite he at hs2_cs,
    have he2 := bigstep.middle3.ctx (lg.attrN hl1) (lg.attrP hl1 hs2_ns) hs2_ns hs2_ps hs2_cs,
    doublecases `he2, cases he,
    have hh := bigstep.value.remains hl2 _ hs1 hs2_hv, rotate 1, {
      apply lg.attrCont hl1 hs2_ns,
    },
    apply bigstep.attrValue _, {
      apply hs2_ih_ns (lg.attrN hl1) rfl hl2 hs1,
    }, {
      apply hs2_ih_ps (lg.attrP hl1 hs2_ns) rfl hl2 hs1,
    }, rotate 1,
    rewrite <- hh at hs2_hv, apply hs2_hv,
    clear hs2_ih_ns hs2_ih_ps,
    have hh3 := hs2_ih_cs legal_context_axiom rfl hl2 hs1,
    rewrite hh, apply hh3,
  }, { -- attrCache
    renames [`hs2_n, `hs2_a, `hs2_p, `hs2_v] [`n,`a,`p,`v],
    have hhc := lg.attrCont hl1 hs2_ns,
    rewrite <- he at hs2_ns,
    have he2 := bigstep.extendCN.remains _ _ hs2_ns, rotate 1, {
      rewrite he at hs2_ns,
      rewrite extendCN.N, apply bigstep.contained _ hs2_cs,
      apply bigstep.contained _ hs2_ps, apply hhc,
    }, {
      rewrite he, apply lg.attrN hl1,
    },
    rewrite he2 at hs2_ns hs2_ps,
    rewrite he at hs2_ns,
    have he3 := bigstep.extendCN.remains hhc (lg.attrP hl1 hs2_ns) hs2_ps,
    rewrite he3 at hs2_cs,
    have he4 := bigstep.extendCN.remains _ legal_context_axiom hs2_cs, 
    rotate 1, {
      rewrite extendCN.N,
      apply bigstep.contained _ hs2_ps,
      rewrite extendCN.N, apply hhc,
    },
    rewrite <- he4 at he, ccases `he,
    rewrite <- he2 at hs2_ns,
    rewrite <- he2 at hs2_ps,
    rewrite <- he3 at hs2_cs,
    have he2 := bigstep.middle3.ctx (lg.attrN hl1) (lg.attrP hl1 hs2_ns) hs2_ns hs2_ps hs2_cs,
    doublecases `he2,
    have hh1 := hs2_ih_ns (lg.attrN hl1) rfl hl2 hs1,
    have hh2 := hs2_ih_ps (lg.attrP hl1 hs2_ns) rfl hl2 hs1,
    have hh3 := hs2_ih_cs legal_context_axiom rfl hl2 hs1,
    rewrite he2 at hs1,
    have hh := bigstep.extendCN.remains _ _ hs1, rotate 1,
    rewrite extendCN.N, assumption, rewrite <- he2, assumption,
    apply bigstep.attrValue _ hh1 hh2 _,
    rewrite hh, rewrite extendCN.match, constructor,
    rewrite hh, rewrite extendCN.match, constructor,
  }, { -- attrHO
    renames [`hs2_n, `hs2_a, `hs2_p] [`n,`a,`p],
    rewrite <- he at hs2_ns,
    have hh1 : ((extendCN cfg n a p (eq.mpr hs2_tp_a hs2_n_f) (addN hs2_n_f (writeAL hs2_ctx3 hs2_n_f (hs2_fl hs2_n_f)))).N = hs2_ctx1.N), {
      rewrite he,
    },
    clear hs2_ih_ns hs2_ih_ps,
    rewrite extendCN.N at hh1,
    rewrite addN.writeAL.flip at hh1,
    rewrite writeAL.N at hh1,
    have hh2 := bigstep.addNs hs2_ns,
    cases hh2 with l1 hh2,
    have hh3 := bigstep.addNs hs2_ps,
    cases hh3 with l2 hh3,
    rewrite addNs.extendCN.flip at hh2,
    rewrite extendCN.N at hh2,
    rewrite addN.writeAL.flip at hh2,
    rewrite writeAL.addNs.flip at hh2,
    rewrite writeAL.N at hh2,
    rewrite addNs.rewrite hh2 at hh3,
    rewrite <- addNs.addN.flip at hh3,
    rewrite <- addNs.addN.flip at hh3,
    have hh4 := addN.addNs.contra hh3,
    cases hh4, assumption, assumption,
  }, { -- cache
    renames [`hs2_n, `hs2_a, `hs2_p, `hs2_v] [`n,`a,`p,`v'],
    rewrite <- he at hs2_c,
    cases hl1,
    have hh := bigstep.extendCN.remains _ _ hs2_c, rotate 1, {
      rewrite he, assumption,
    }, {
      rewrite he, apply legal_context_axiom,
    },
    have he1 : (hs2_ctx2 = hs2_ctx1), {
      rewrite hh, rewrite he,
    }, cases he1,
    have hs : (c4 ⊨ n / a / p ;=hs2_ctx1.c n a p »» extendCN cfg n a p v' c4 ⊨ v'), {
      apply bigstep.cache,
      -- apply bigstep.contained hs2_cont hs1,
      apply hs2_ih, apply legal_context_axiom,
      reflexivity, apply hl2, apply hs1,
    },
    have he2 : (extendCN cfg n a p v' c4 = c4), {
      rewrite hh at hs1,
      rewrite <- (bigstep.extendCN.remains _ _ hs1), {
        rewrite <- hh, assumption,
      }, {
        rewrite <- hh,
        assumption,
      }
    },
    rewrite he2 at hs, apply hs,
  },
end

lemma bigstep.cacheperm.crux : ∀ {cfg:_} {c3 c4:ctxN cfg} {e e2:exp cfg}
                        {T T2:Type} {v:T} {v2:T2},
  legal c3 e ->
  legal c3 e2 ->
  (c3 ⊨ e2 »» c4 ⊨ v2) ->
  (c3 ⊨ e »» c3 ⊨ v) ->
  (c4 ⊨ e »» c4 ⊨ v)
:= begin
  introv, intros hl1 hl2 hs1 hs2,
  apply bigstep.cacheperm.crux_aux hl1 hl2 hs1 hs2 rfl,
end

theorem bigstep.cacheperm : ∀ {cfg:_} {c3 c4:ctxN cfg} {e:exp cfg}
                        {T:Type} {v:T},
  legal c3 e ->
  (c3 ⊨ e »» c4 ⊨ v) ->
  (c4 ⊨ e »» c4 ⊨ v)
:= begin
  introv, intros hl hs, induction hs,
  { -- refl
    apply bigstep.refl,
  }, { -- condT
    have hh1 := hs_ih_cs (lg.condC hl1) (),
    have hh2 := hs_ih_ts (lg.condT hl1 hs_cs),
    clear hs_ih_cs hs_ih_ts,
    have hh3 := bigstep.cacheperm.crux _ _ hs_ts hh1,
    rotate 1, {

    },
    apply bigstep.condT,

    -- apply bigstep.cacheperm.crux (lg.condC hl) (lg.condT hl) hs_ts (hs_ih_cs (lg.condC hl)),
    -- apply hs_ih_a_1 (lg.condT hl),
  }, { -- condT
    apply bigstep.condF,
    apply bigstep.cacheperm.crux (lg.condC hl) (lg.condF hl) hs_a_1 (hs_ih_a (lg.condC hl)),
    apply hs_ih_a_1 (lg.condF hl),
  }, { -- app
    apply bigstep.app,
    apply bigstep.cacheperm.crux (lg.appF hl) (lg.appP hl) hs_a_1 (hs_ih_a (lg.appF hl)),
    apply hs_ih_a_1 (lg.appP hl),
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v'],
    apply @bigstep.attrValue, rotate 2, {
      have he : (hs_ctx3.c n a p = hs_ctx4.c n a p), {
        apply bigstep.self.same.unused _ hs_a_5,
        apply doesnt_call_self hs_a_5,
      },
      rewrite he at hs_a_5,
      have hh := @bigstep.cong.crux.crux _ hs_ctx4 hs_ctx4 n a p v' hs_eN _ n _ _ _ _,
      splith `hh,
      apply hha,
      {
        apply bigstep.contained _ hs_a_5,
        apply bigstep.contained hs_a_2 hs_a_4,
      }, {
        rewrite <- he, apply hs_ih_a_2,
        apply legal_context_axiom,
      }, {
        apply lg.attrN hl,
      }, {
        apply bigstep.cacheperm.crux _ _ hs_a_5,
        apply bigstep.cacheperm.crux _ _ hs_a_4,
        apply hs_ih_a, {
          apply lg.attrN hl,
        }, apply lg.attrN hl,
        apply lg.attrP hl,
        apply lg.attrN hl,
        rewrite <- he,
        apply legal_context_axiom,
      },
    }, {
      have he : (hs_ctx3.c n a p = hs_ctx4.c n a p), {
        apply bigstep.self.same.unused _ hs_a_5,
        apply doesnt_call_self hs_a_5,
      },
      have hh := @bigstep.cong.crux.crux _ hs_ctx4 hs_ctx4 n a p v' _ _ _ _ _ _ _,
      splith `hh, apply hha,
      {
        apply bigstep.contained _ hs_a_5,
        apply bigstep.contained hs_a_2 hs_a_4,
      },
      rewrite he at hs_ih_a_2,
      apply hs_ih_a_2,
      rewrite <- he, apply legal_context_axiom,
      apply lg.attrP hl,
      apply bigstep.cacheperm.crux _ _ hs_a_5,
      apply hs_ih_a_1,
      apply lg.attrP hl, apply lg.attrP hl,
      apply legal_context_axiom,
    }, {
      rewrite extendCN.match, apply bigstep.refl,
    }, {
      rewrite extendCN.match, constructor,
    },
    rewrite extendCN.N,
    apply bigstep.contained _ hs_a_5,
    apply bigstep.contained hs_a_2 hs_a_4,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v'],
    apply bigstep.attrValue _ _, {
      apply bigstep.cacheperm.crux _ _ hs_a_5,
      apply bigstep.cacheperm.crux _ _ hs_a_4,
      apply hs_ih_a,
      apply lg.attrN hl, apply lg.attrN hl,
      apply lg.attrP hl, apply lg.attrN hl,
      apply legal_context_axiom,
    }, {
      apply bigstep.cacheperm.crux _ _ hs_a_5,
      apply hs_ih_a_1,
      apply lg.attrP hl, apply lg.attrP hl,
      apply legal_context_axiom,
    }, {
      have he : (hs_ctx3.c n a p = hs_ctx4.c n a p), {
        apply bigstep.self.same.unused _ hs_a_5,
        apply doesnt_call_self hs_a_5,
      },
      rewrite he at hs_ih_a_2,
      apply hs_ih_a_2,
      rewrite <- he, apply legal_context_axiom,
    }, {
      have he : (hs_ctx3.c n a p = hs_ctx4.c n a p), {
        apply bigstep.self.same.unused _ hs_a_5,
        apply doesnt_call_self hs_a_5,
      },
      rewrite he at hs_a_1,
      apply hs_a_1,
    }, {
      apply bigstep.contained _ hs_a_5,
      apply bigstep.contained hs_a_2 hs_a_4,
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n,`a,`p],
    have hh1 := hs_ih_ns (lg.attrN hl),
    have hh2 := hs_ih_ps (lg.attrP hl),
    clear hs_ih_ns hs_ih_ps,
    have hh3 := bigstep.cacheperm.crux (lg.attrN hl) (lg.attrP hl) hs_ps hh1,
    have hh4 := bigstep.HOwrite.unused (lg.attrN hl) hh3 hs_ismk hs_isfresh,
    have hh5 := bigstep.HOwrite.unused (lg.attrP hl) hh2 hs_ismk hs_isfresh,
    apply bigstep.attrValue _ _ hh4 hh5, rotate 1, {
      rewrite extendCN.match, constructor,
    }, {
      rewrite extendCN.N, apply contained.addN,
      rewrite writeAL.N, apply bigstep.contained hs_cont hs_ps,
    }, {
      rewrite extendCN.match,
      rewrite eq.mpr.exp,
      apply bigstep.refl,
    }
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n,`a,`p,`v'],

    have hh : (extendCN cfg n a p v' hs_ctx2 ⊨ n / a / p ;=hs_eC »» extendCN cfg n a p v' (extendCN cfg n a p v' hs_ctx2) ⊨ v'), {
      apply bigstep.cache _,
      apply bigstep.withcache.unused, {
        cases hl, cases hl_a,
        have he : (hs_ctx1.c n a p = hs_ctx2.c n a p), {
          apply bigstep.self.same.unused _ hs_c,
          apply doesnt_call_self hs_c,
        },
        rewrite he, rewrite he at hs_c,
        apply doesnt_call_self,
        rewrite <- he, apply hs_ih,
        apply legal_context_axiom,
      },
      apply hs_ih,
      {
        cases hl, cases hl_a,
        apply legal_context_axiom,
      }, rewrite extendCN.N,
      apply bigstep.contained hs_cont hs_c,
    },
    rewrite extendCN.redundant at hh, apply hh,
  },
end




lemma bigstep.withcache.unused.samectx {cfg:_} {c1 c2:ctxN cfg} {e:exp cfg} {n a p} {v:cfg.τ a} {T:Type} {v':T} :
  legal c1 e ->
  (c1 ⊨ e »» c2 ⊨ v') ->
  c2 = c1 ->
  (c1 ⊨ c1.c n a p »» c1   ⊨ v) ->
  (extendCN _ n a p v c1 ⊨ e »» extendCN _ n a p v c1 ⊨ v')
:= begin
  introv, intros hl hs he hcs,
  induction hs, { -- refl
    apply bigstep.refl,
  }, { -- condT
    ccases `he, have hh := bigstep.middle.ctx (lg.condC hl) hs_cs hs_ts, ccases `hh,
    apply bigstep.condT (hs_ih_cs (lg.condC hl) rfl hcs) (hs_ih_ts (lg.condT hl hs_cs) rfl hcs),
  }, { -- condF
    ccases `he, have hh := bigstep.middle.ctx (lg.condC hl) hs_cs hs_fs, ccases `hh,
    apply bigstep.condF (hs_ih_cs (lg.condC hl) rfl hcs) (hs_ih_fs (lg.condF hl hs_cs) rfl hcs),
  }, { -- app
    ccases `he, have hh := bigstep.middle.ctx (lg.appF hl) hs_fs hs_ps, ccases `hh,
    apply bigstep.app (hs_ih_fs (lg.appF hl) rfl hcs) (hs_ih_ps (lg.appP hl hs_fs) rfl hcs),
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    ccases `he,
    have hh := bigstep.middle3.ctx (lg.attrN hl) (lg.attrP hl hs_ns) hs_ns hs_ps hs_cs,
    doublecases `hh,
    have hh1 := hs_ih_ns (lg.attrN hl) rfl hcs, clear hs_ih_ns,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) rfl hcs, clear hs_ih_ps,
    have hh3 := hs_ih_cs legal_context_axiom rfl hcs, clear hs_ih_cs,
    apply bigstep.attrValue _ hh1 hh2, rotate 1, {
      apply extendCN.value, assumption,
    },
    destruct (napmatch cfg n n' a a' p p'); intros hnap, {
      rewrite napmatch.ff_update_unused_CN hnap, assumption,
    }, {
      napmatch_rewrites `hnap,
      rewrite extendCN.match,
      have hh := bigstep.determinism hcs hs_cs,
      doublecases `hh, apply bigstep.refl,
    },
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite <- he at hs_ns,
    have hh1 := bigstep.extendCN.remains _ _ hs_ns, rotate 1, {
      rewrite extendCN.N,
      apply bigstep.contained _ hs_cs,
      apply bigstep.contained _ hs_ps,
      apply lg.attrCont hl,
      rewrite he at hs_ns, assumption,
    }, {
      rewrite he, apply lg.attrN hl,
    },
    rewrite he at hs_ns,
    rewrite hh1 at *,
    have hh2 := bigstep.extendCN.remains _ _ hs_ps, rotate 1, {
      rewrite extendCN.N,
      have hh := lg.attrCont hl hs_ns,
      rewrite extendCN.N at hh, assumption,
    }, {
      apply lg.attrP hl hs_ns,
    },
    rewrite hh2 at *,
    have hh3 := bigstep.extendCN.remains _ legal_context_axiom hs_cs, rotate 1, {
      rewrite extendCN.N, 
      have hh := bigstep.contained _ hs_ps,
      rewrite extendCN.N at hh, apply hh,
      rewrite extendCN.N, apply lg.attrCont hl hs_ns,
    },
    rewrite <- hh3 at he, ccases `he,

    have hh4 := bigstep.middle3.ctx (lg.attrN hl) (lg.attrP hl hs_ns) hs_ns hs_ps hs_cs,
    cases hh4 with hh4 hh5,
    have he1 : hs_ctx1 = hs_ctx2, {
      rewrite hh1, rewrite hh5,
    }, ccases `he1,
    have hh6 := hs_ih_ns (lg.attrN hl) hh5 hcs, clear hs_ih_ns,
    clear hh5,
    have he1 : hs_ctx1 = hs_ctx3, {
      rewrite hh2, rewrite <- hh4,
    }, ccases `he1,
    clear hh2 hh3 hh4,
    rewrite <- hh1 at *,
    have hh7 := hs_ih_ps (lg.attrP hl hs_ns) rfl hcs, clear hs_ih_ps,
    have hh8 := hs_ih_cs legal_context_axiom rfl,

    apply bigstep.attrValue _ hh6 hh7, rotate 1, {
      destruct (napmatch cfg n n' a a' p p'); intros hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite hh1, rewrite extendCN.match, constructor,
      napmatch_rewrites `hnap, rewrite extendCN.match,
      constructor,
    }, {
      destruct (napmatch cfg n n' a a' p p'); intros hnap, {
        rewrite napmatch.ff_update_unused_CN hnap,
        rewrite hh1, rewrite extendCN.match,
        apply bigstep.refl,
      }, {
        napmatch_rewrites `hnap, rewrite extendCN.match,
        rewrite hh1 at hcs, rewrite extendCN.match at hcs,
        have hh := bigstep.val.same.v hcs, cases hh,
        apply bigstep.refl,
      },
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    clear hs_ih_ns hs_ih_ps,
    rewrite <- he at hs_ns,
    have hh1 := bigstep.addNs hs_ns,
    have hh2 := bigstep.addNs hs_ps,
    cases hh1 with l1 hh1,
    cases hh2 with l2 hh2,
    rewrite addNs.rewrite hh1 at hh2,
    rewrite addNs.extendCN.flip at hh2,
    rewrite addNs.extendCN.flip at hh2,
    rewrite addN.writeAL.flip at hh2,
    rewrite writeAL.addNs.flip at hh2,
    rewrite writeAL.addNs.flip at hh2,
    rewrite extendCN.N at hh2,
    rewrite writeAL.N at hh2,
    rewrite <- addNs.addN.flip at hh2,
    rewrite <- addNs.addN.flip at hh2,
    have hh := addN.addNs.contra hh2,
    cases hh, assumption, assumption,
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite <- he at hs_c,
    have hh1 := bigstep.extendCN.remains _ _ hs_c, rotate 1, {
      cases hl, rewrite he, assumption,
    }, {
      rewrite he, cases hl, apply legal_context_axiom,
    },
    have hh2 : (hs_ctx1 = hs_ctx2), {
      rewrite hh1, rewrite he,
    }, ccases `hh2, clear hh1,
    have hh1 := hs_ih _ rfl hcs, rotate 1, {
      cases hl, apply legal_context_axiom,
    }, clear hs_ih,
    have hh2 := bigstep.cache hh1,
    destruct (napmatch cfg n n' a a' p p'); intros hnap, {
      rewrite <- napmatch.ff_extend_flip_CN hnap at hh2,
      rewrite he at hh2, apply hh2,
    }, {
      napmatch_rewrites `hnap,
      rewrite extendCN.redundant at hh2,
      rewrite <- he at hcs,
      rewrite extendCN.match at hcs,
      have hh := bigstep.val.same.v hcs, cases hh,
      apply hh2,
    }
  },
end



lemma bigstep.withHOwrite.unused.samectx {cfg:_} {c1 c2:ctxN cfg} {e:exp cfg} {n a p} {T:Type} {v':T} {fl:_} {n_f:node} {tp_a:cfg.τ a = node} :
  legal c1 e ->
  (c1 ⊨ e »» c2 ⊨ v') ->
  c2 = c1 ->
  (c1.c n a p = exp.eMk fl) ->
  (n_f = fresh_for c1) ->
  (extendCN cfg n a p (eq.mpr tp_a n_f) (addN n_f (writeAL c1 n_f (fl n_f))) ⊨ e »» extendCN cfg n a p (eq.mpr tp_a n_f) (addN n_f (writeAL c1 n_f (fl n_f))) ⊨ v')
:= begin
  introv, intros hl hs he hmk hf,
  induction hs, { -- refl
    apply bigstep.refl,
  }, { -- condT
    ccases `he, have hh := bigstep.middle.ctx (lg.condC hl) hs_cs hs_ts, ccases `hh,
    apply bigstep.condT (hs_ih_cs (lg.condC hl) rfl hmk hf) (hs_ih_ts (lg.condT hl hs_cs) rfl hmk hf),
  }, { -- condF
    ccases `he, have hh := bigstep.middle.ctx (lg.condC hl) hs_cs hs_fs, ccases `hh,
    apply bigstep.condF (hs_ih_cs (lg.condC hl) rfl hmk hf) (hs_ih_fs (lg.condF hl hs_cs) rfl hmk hf),
  }, { -- app
    ccases `he, have hh := bigstep.middle.ctx (lg.appF hl) hs_fs hs_ps, ccases `hh,
    apply bigstep.app (hs_ih_fs (lg.appF hl) rfl hmk hf) (hs_ih_ps (lg.appP hl hs_fs) rfl hmk hf),
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    ccases `he,
    have hh := bigstep.middle3.ctx (lg.attrN hl) (lg.attrP hl hs_ns) hs_ns hs_ps hs_cs,
    doublecases `hh,
    have hh1 := hs_ih_ns (lg.attrN hl) rfl hmk hf, clear hs_ih_ns,
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns) rfl hmk hf, clear hs_ih_ps,
    have hh3 := hs_ih_cs legal_context_axiom rfl hmk hf, clear hs_ih_cs,
    have hh := lg.attrCont hl hs_ns,
    have hne := fresh_for.contained hh, rewrite <- hf at hne,
    apply bigstep.attrValue _ hh1 hh2, rotate 1, {
      apply extendCN.value,
      rewrite addN.c,
      rewrite writeAL.unused hne,
      assumption,
    },
    destruct (napmatch cfg n n' a a' p p'); intros hnap, {
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c, rewrite writeAL.unused hne,
      assumption,
    }, {
      napmatch_rewrites `hnap,
      rewrite hmk at hs_hv,
      cases hs_hv,
    },
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite <- he at hs_ns,
    have hh1 := bigstep.extendCN.remains _ _ hs_ns, rotate 1, {
      rewrite extendCN.N, apply bigstep.contained _ hs_cs,
      apply bigstep.contained _ hs_ps,
      rewrite he at hs_ns,
      apply lg.attrCont hl hs_ns,
    }, {
      rewrite he, apply lg.attrN hl,
    },
    rewrite hh1 at hs_ps,
    have hh2 := bigstep.extendCN.remains _ _ hs_ps, rotate 1, {
      rewrite extendCN.N, rewrite he at hs_ns,
      apply lg.attrCont hl hs_ns,
    }, {
      rewrite <- hh1, apply lg.attrP hl,
      rewrite he at hs_ns, apply hs_ns,
    },
    rewrite hh2 at hs_cs,
    have hh3 := bigstep.extendCN.remains _ legal_context_axiom hs_cs, rotate 1, {
      rewrite extendCN.N, rewrite hh2,
      rewrite extendCN.N,
      apply bigstep.contained _ hs_ps,
      rewrite extendCN.N, 
      rewrite he at hs_ns, apply lg.attrCont hl hs_ns,
    },
    rewrite hh3 at hs_cs,
    have he1 : (hs_ctx1 = hs_ctx4), {
      rewrite hh3, rewrite <- he,
    }, ccases `he1,
    rewrite <- hh1 at *,
    rewrite <- hh2 at *,
    rewrite <- hh3 at *,
    have hh4 := bigstep.middle3.ctx (lg.attrN hl) _ hs_ns hs_ps hs_cs,
    rotate 1, {
      apply lg.attrP hl hs_ns,
    },
    doublecases `hh4,
    clear hh2 hh3,
    have hh6 := hs_ih_ns (lg.attrN hl) rfl hmk hf, clear hs_ih_ns,
    have hh7 := hs_ih_ps (lg.attrP hl hs_ns) rfl hmk hf, clear hs_ih_ps,
    have hh8 := hs_ih_cs legal_context_axiom rfl hmk hf, clear hs_ih_cs,
    have hhc := lg.attrCont hl hs_ns,
    have hne := fresh_for.contained hhc, rewrite <- hf at hne,
    apply bigstep.attrValue _ hh6 hh7, rotate 1, {
        destruct (napmatch cfg n n' a a' p p'); intros hnap, {
        rewrite napmatch.ff_update_unused_CN hnap,
        rewrite addN.c, rewrite writeAL.unused hne,
        rewrite hh1, rewrite extendCN.match, constructor,
      }, {
        napmatch_rewrites `hnap, rewrite extendCN.match,
        constructor,
      },
    }, {
      destruct (napmatch cfg n n' a a' p p'); intros hnap, {
        rewrite napmatch.ff_update_unused_CN hnap,
        rewrite addN.c, rewrite writeAL.unused hne,
        rewrite hh1, rewrite extendCN.match,
        apply bigstep.refl,
      }, {
        napmatch_rewrites `hnap,
        rewrite hh1 at hs_nv,
        rewrite extendCN.match at hs_nv,
        cases hs_nv,
      },
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    clear hs_ih_ns hs_ih_ps,
    rewrite <- he at hs_ns,
    have hh1 := bigstep.addNs hs_ns,
    have hh2 := bigstep.addNs hs_ps,
    cases hh1 with l1 hh1,
    cases hh2 with l2 hh2,
    rewrite addNs.rewrite hh1 at hh2,
    rewrite addNs.extendCN.flip at hh2,
    rewrite addNs.extendCN.flip at hh2,
    rewrite addN.writeAL.flip at hh2,
    rewrite writeAL.addNs.flip at hh2,
    rewrite writeAL.addNs.flip at hh2,
    rewrite extendCN.N at hh2,
    rewrite writeAL.N at hh2,
    rewrite <- addNs.addN.flip at hh2,
    rewrite <- addNs.addN.flip at hh2,
    have hh := addN.addNs.contra hh2,
    cases hh, assumption, assumption,
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite <- he at hs_c,
    have hs_cont : (contained n' (hs_ctx1.N)), {
      cases hl, assumption,
    },
    have hh1 := bigstep.extendCN.remains _ _ hs_c, rotate 1, {
      rewrite extendCN.N, rewrite he at hs_c, apply bigstep.contained hs_cont hs_c,
    }, {
      rewrite he,
      cases hl, apply legal_context_axiom,
    },
    have hh2 : (hs_ctx1 = hs_ctx2), {
      rewrite hh1, rewrite he,
    }, ccases `hh2, clear hh1,
    have hh1 := hs_ih _ rfl hmk hf, rotate 1, {
      cases hl, apply legal_context_axiom,
    }, clear hs_ih,
    have hne := fresh_for.contained hs_cont, rewrite <- hf at hne,
    have hh2 := bigstep.cache hh1,
    destruct (napmatch cfg n n' a a' p p'); intros hnap, {
      rewrite <- napmatch.ff_extend_flip_CN hnap at hh2,
      rewrite extendCN.addN.flip at hh2,
      rewrite extendCN.addN.flip at hh2,
      rewrite writeAL.extendCN.flip hne at hh2,
      rewrite <- extendCN.addN.flip at hh2,
      rewrite he at hh2, 
      apply hh2,
    }, {
      napmatch_rewrites `hnap,
      rewrite <- he at hmk,
      rewrite extendCN.match at hmk,
      cases hmk,
    },
  },
end