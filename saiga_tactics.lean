import .saiga
import .saiga_lemmas
open saiga tactic


-- saiga configuration
variable cfg: saigaconfig


-- can't step from a value. lemma and tactic
-- theorem stepcontra : forall {T:sType} {k:nat} {b:bool}
--                           {ctx ctx':context cfg} {v:typeof T} {e:exp cfg} {any:Prop},
--     (cfg; b ▹▹ k ▹ ctx ⊨ {{v}} ⟶ ctx' ⊨ e) -> any
-- := begin
--   introv, intros hs, cases hs,
-- end

-- theorem stepcontraMUT : forall {T:Type} {k:nat} {b:bool}
--                           {ctx ctx':context cfg} {v:T} {ve:vexp T v} {e:exp cfg} {any:Prop},
--     (cfg; b ▹▹ k ▹ ctx ⊨ !{?ve}} ⟶ ctx' ⊨ e) -> any
-- := begin
--   intros, cases a,
-- end

def isSome {T:Type}: option T -> bool
| none := ff
| _    := tt

lemma value_nostep_quod {cfg:saigaconfig}:
  ∀ {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T} {P:Prop},
    (c1 ⊨ {{v}} ⟶ c2 ⊨ e) -> P
:= begin
  introv, intros hs, cases hs,
end

meta def contra_value_step_aux : list expr -> tactic unit
| []        := failed
| (h :: hs) := do t <- infer_type h,
                  match t with
                  | `(saiga.step _ {{_}} _ _) := do zz <- to_expr ``(value_nostep_quod %%h),
                                                  apply zz, skip
                  | _                       := contra_value_step_aux hs
                  end

meta def value_step_contra : tactic unit :=
do ctx <- local_context,
   contra_value_step_aux ctx




lemma andL: ∀ {P1 P2:Prop},
  P1 ∧ P2 -> P1
:= begin
  intros P1 P2 h, cases h, apply h_left,
end

lemma andR: ∀ {P1 P2:Prop},
  P1 ∧ P2 -> P2
:= begin
  intros P1 P2 h, cases h, apply h_right,
end

lemma and2 {P1 P2:Prop} (p1:P1) (p2:P2) : P1 ∧ P2
:= and.intro p1 p2

lemma and3 {P1 P2 P3:Prop} (p1:P1) (p2:P2) (p3:P3) : (P1 ∧ P2 ∧ P3)
:= and.intro p1 (and.intro p2 p3)



-- solve both sides of a conjuction with one tactic
meta def and_both (z : tactic unit) : tactic unit :=
do g :: gs <- get_goals,
   gg <- infer_type g,
   match gg with
    | `(_ ∧ _) := do e <- to_expr ``(and.intro), apply e, solve1 z, solve1 z
    | _ := failed
   end

-- split conjunction of equalities (hypothesis) and rewrite with both
meta def doublerw (i:name) : tactic unit :=
do ih <- get_local i,
   it <- infer_type ih,
   ha <- mk_fresh_name, hb <- mk_fresh_name,
   match it with
    | `(_ = _ ∧ _ = _) := do (cases ih [ha,hb]),
                                hal <- get_local ha, rewrite_target hal,
                                hbl <- get_local hb, rewrite_target hbl,
                                clear hal, clear hbl
    | _ := failed
   end

meta def exists_var_name : name -> expr -> string
| hn (expr.lam n _ _ _) := to_string hn ++ "_" ++ to_string n
| _  _                  := "_unknown_"

-- for some reason i need a function like this to stop "." being added to names
meta def combine : name -> name -> string
| h l := to_string h ++ to_string l

meta def splith (i:name) : tactic unit :=
do ih <- get_local i,
   do cases ih [combine i "a", combine i "b"], skip

meta def renames : list name -> list name -> tactic unit
| (list.cons fh fr) (list.cons th tr) := do rename fh th, renames fr tr
| _ _ := skip


meta def clears : list name -> tactic unit
| (list.nil) := skip
| (list.cons i r) := do ih <- get_local i, clear ih, clears r


meta def clear_refl_expr : expr -> tactic unit
| e := 
do it <- infer_type e,
match it with
      | `(%%a1 = %%a2)  := do (do unify a1 a2, clear e, skip) <|> skip
      | `(%%a1 == %%a2) := do (do unify a1 a2, clear e, skip) <|> skip
      | _ := skip
end

meta def clear_refls_name : list name -> tactic unit
| (list.nil) := skip
| (list.cons i r) :=
      do
      ih <- get_local i,
      it <- infer_type ih,
      match it with
      | `(%%a1 = %%a2)  := do (do unify a1 a2, clear ih, skip) <|> skip
      | `(%%a1 == %%a2) := do (do unify a1 a2, clear ih, skip) <|> skip
      | _ := skip
      end,
      clear_refls_name r

meta def clear_refls_exprs : list expr -> tactic unit
| (list.cons h r) := do
  match h with
  | i := do clear_refl_expr i
  end, clear_refls_exprs r
| (list.nil) := skip

meta def clear_refls : tactic unit :=
  do ctx <- local_context,
  clear_refls_exprs ctx


meta def ccases (i:name) : tactic unit :=
do ih <- get_local i,
   cases ih,
   clear_refls_name [i]


meta def napmatch_rewrite_a (i:name) : tactic unit :=
  do ih <- get_local i,
    it <- infer_type ih,
    match it with
    | `(napmatch %%cfg %%n1 %%n2 %%a1 %%a2 %%p1 %%p2 = tt) := do
        ia <- mk_fresh_name,
        pa <- mk_mapp ``napmatch.asame [cfg, n1, n2, a1, a2, p1, p2, ih],
        tactic.note ia none pa,
        iah <- get_local ia,
        cases iah,
        skip
    | _ := fail "the hypothesis provided is not an napmatch truth"
    end

meta def napmatch_rewrite_np (i:name) : tactic unit :=
  do ih <- get_local i,
    it <- infer_type ih,
    match it with
    | `(napmatch %%cfg %%n1 %%n2 %%a1 %%a2 %%p1 %%p2 = tt) := do
        ia <- mk_fresh_name,
        pa <- mk_mapp ``napmatch.npsame [cfg, n1, n2, a2, p1, p2, ih],
        tactic.note ia none pa,
        iah <- get_local ia,
        i1 <- mk_fresh_name, i2 <- mk_fresh_name,
        cases iah [i1, i2],
        ih1 <- get_local i1, ih2 <- get_local i2,
        cases ih1, cases ih2,
        clear_refls
    | _ := fail "the hypothesis provided is not an napmatch truth"
    end



meta def napmatch_rewrites (i:name) : tactic unit :=
  do napmatch_rewrite_a i, napmatch_rewrite_np i



meta def splith3 (i:name) : tactic unit :=
do ih <- get_local i,
   cases ih [combine i "a", i],
   ih <- get_local i,
   cases ih [combine i "b", combine i "c"], skip

meta def splith4 (i:name) : tactic unit :=
do ih <- get_local i,
   cases ih [combine i "a", i],
   ih <- get_local i,
   cases ih [combine i "b", i],
   ih <- get_local i,
   cases ih [combine i "c", combine i "d"], skip

meta def splith5 (i:name) : tactic (list name) :=
let ha := combine i "a" in
let hb := combine i "b" in
let hc := combine i "c" in
let hd := combine i "d" in
let he := combine i "e" in
do ih <- get_local i,
   cases ih [ha, i],
   ih <- get_local i,
   cases ih [hb, i],
   ih <- get_local i,
   cases ih [hc, i],
   ih <- get_local i,
   cases ih [hd, he]

meta def splith6 (i:name) : tactic (list name) :=
let ha := combine i "a" in
let hb := combine i "b" in
let hc := combine i "c" in
let hd := combine i "d" in
let he := combine i "e" in
let hf := combine i "f" in
do ih <- get_local i,
   cases ih [ha, i],
   ih <- get_local i,
   cases ih [hb, i],
   ih <- get_local i,
   cases ih [hc, i],
   ih <- get_local i,
   cases ih [hd, i],
   ih <- get_local i,
   cases ih [he, hf]

lemma splith_test : ∀ {A B:Prop},
  A ∧ B -> A
:= begin
  intros A B hh,
  splith `hh,
  assumption,
end

meta def doex (i:name) : tactic unit :=
do ih <- get_local i,
   it <- infer_type ih,
   match it with
     | `(Exists (%%z : _)) := do cases ih [(exists_var_name i z), to_string ih], skip
     | _ := failure
   end

meta def rdoex (i:name) : tactic unit :=
  repeat (doex i)

lemma doex_test:
  (exists (k:nat), k = k) -> true=true
:= begin
  intros hthing,
  doex `hthing,
  reflexivity,
end




meta def my_find_same_type : expr → list expr → tactic expr
| e []         := do fail "didnt find nothin"
| e (H :: Hs) :=
  do t ← infer_type H,
     (unify e t >> return H) <|> find_same_type e Hs

meta def getassumption : tactic expr :=
do { ctx ← local_context,
     t   ← target,
     H   ← my_find_same_type t ctx,
     return H }
<|> fail "could not find assumption"


@[hole_command]
meta def exact_cmd : hole_command :=
{ name   := "Exact",
  descr  := "Find assumption to apply",
  action := λ ps, do
    z <- getassumption,
    return [(to_string z, "that")]
}



meta def get_constructors_for2 (e : expr) : tactic (list name) :=
do env ← get_env,
   I   ← return e.get_app_fn.const_name,
   when (¬env.is_inductive I) (fail "constructor tactic failed, target is not an inductive datatype"),
   return $ env.constructors_of I

private meta def try_constructors2: list name → tactic name
| []      := fail "constructor tactic failed, none of the constructors is applicable"
| (c::cs) := (mk_const c >>= λ e, apply_core e >> return c) <|> try_constructors2 cs

private meta def target' : tactic expr :=
target >>= instantiate_mvars >>= whnf

meta def getconstructor : tactic name :=
target' >>= get_constructors_for2 >>= try_constructors2

@[hole_command]
meta def constructor_cmd : hole_command :=
{ name   := "Constructor",
  descr  := "Find constructor to apply",
  action := λ ps, do
    z <- getconstructor,
    return [("apply " ++ to_string z, "that")]
}

meta def unwrap (i:name) : tactic unit :=
  let ha := combine i "a" in
  let hb := combine i "b" in
  let hc := combine i "c" in
  let hd := combine i "d" in
  let hak := combine ha "_k" in
  let hbk := combine hb "_k" in
  let hck := combine hc "_k" in
  let haa := combine ha "a" in
  let hab := combine ha "b" in
  let hba := combine hb "a" in
  let hbb := combine hb "b" in
  let hca := combine hc "a" in
  let hcb := combine hc "b" in
  do ih <- get_local i,
     cases ih [ha, hc],
     h_ha <- get_local ha,
     cases h_ha [ha, hb],
     h_hc <- get_local hc,
     cases h_hc [hc, hd],

     h_ha <- get_local ha,
     cases h_ha [hak, ha],
     h_ha <- get_local ha,
     cases h_ha [haa, hab],
     h_hb <- get_local hb,
     cases h_hb [hbk, hb],
     h_hb <- get_local hb,
     cases h_hb [hba, hbb],
     h_hc <- get_local hc,
     cases h_hc [hck, hc],
     h_hc <- get_local hc,
     cases h_hc [hca, hcb], skip



meta def doublecases (i:name) : tactic unit :=
do ih <- get_local i,
   it <- infer_type ih,
   ha <- mk_fresh_name, hb <- mk_fresh_name,
   match it with
    | `(%%qq = _ ∧ _ = _) := do (cases ih [ha,hb]),
                                hal <- get_local ha, cases hal,
                                hbl <- get_local hb, cases hbl, clear_refls
                                -- hal2 <- get_local ha, clear hal2,
                                -- hbl2 <- get_local hb, clear hbl2
    | `(%%qq == _ ∧ _ = _) := do (cases ih [ha,hb]),
                                hal <- get_local ha, cases hal,
                                hbl <- get_local hb, cases hbl, clear_refls
                                -- hal2 <- get_local ha, clear hal2,
                                -- hbl2 <- get_local hb, clear hbl2
    | `(%%qq = _ ∧ _ == _) := do (cases ih [ha,hb]),
                                hal <- get_local ha, cases hal,
                                hbl <- get_local hb, cases hbl, clear_refls
                                -- hal2 <- get_local ha, clear hal2,
                                -- hbl2 <- get_local hb, clear hbl2
    | `(%%qq == _ ∧ _ == _) := do (cases ih [ha,hb]),
                                hal <- get_local ha, cases hal,
                                hbl <- get_local hb, cases hbl, clear_refls
                                -- hal2 <- get_local ha, clear hal2,
                                -- hbl2 <- get_local hb, clear hbl2
    | _ := failed
    end


meta def triplecases (i:name) : tactic unit :=
do ih <- get_local i,
   it <- infer_type ih,
   ha <- mk_fresh_name, hb <- mk_fresh_name, hc <- mk_fresh_name,
   match it with
    | `(_ ∧ _ ∧ _) := do (cases ih [ha,hb]),
                                    ihb <- get_local hb, (cases ihb [hb, hc]),
                                    iha <- get_local ha, cases iha, iha <- get_local ha, clear iha,
                                    ihb <- get_local hb, cases ihb, ihb <- get_local hb, clear ihb,
                                    ihc <- get_local hc, cases ihc, ihc <- get_local hc, clear ihc

    | _ := fail "target is not a triple conjunction"
   end

lemma test_triplecases : ∀ {A:Type} {a1 a2 b1 b2 c1 c2:A},
  a1=a2 ∧ b1=b2 ∧ c1=c2 -> a1 = b1 ∧ c1 = a2 ∧ b2 = c2 -> true
:= begin
  intros,
  -- cases a with aa bb,
  -- cases bb with bb cc,
  triplecases `a,
  constructor,
end





set_option eqn_compiler.max_steps 8192

meta def getValExpr (e:expr) : tactic (option expr) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  T <- mk_meta_var `(sType),
  vT <- mk_mapp ``saiga.sType.tp [some T],
  v <- mk_meta_var vT,
  exT <- mk_mapp ``saiga.exp.eVal [some cfg, some T, some v],
  ((do unify exT e, return (some v)) <|> return none)

meta def isValExpr (e:expr) : tactic (bool) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  T <- mk_meta_var `(sType),
  vT <- mk_mapp ``saiga.sType.tp [some T],
  v <- mk_meta_var vT,
  exT <- mk_mapp ``saiga.exp.eVal [some cfg, some T, some v],
  ((do unify exT e, return tt) <|> return ff)


meta def testval : tactic unit :=
  do g1 :: gr <- get_goals,
     env ← get_env,
     g <- infer_type g1,
     match g with
     | `(%%c;%%b ▹▹ _ ▹ _ ⊨ %%e ⟶ _ ⊨ _) := do val <- isValExpr e, trace val
     | _ := fail "not a step"
     end


meta def getCondExpr (e:expr) : tactic (option (expr × expr × expr)) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  Texp <- mk_mapp ``saiga.exp [some cfg],
  c <- mk_meta_var Texp,
  t <- mk_meta_var Texp,
  f <- mk_meta_var Texp,
  condexpr <- mk_mapp ``saiga.exp.eCond [some cfg, some c, some t, some f],
  (do unify condexpr e, return (c, t, f)) <|> return none

meta def isCondExpr (e:expr) : tactic bool :=
  do opt <- getCondExpr e, return(isSome opt)


meta def getAppExpr (e:expr) : tactic (option expr) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  T1 <- mk_meta_var `(sType),
  T2 <- mk_meta_var `(sType),
  Texp <- mk_mapp ``saiga.exp [some cfg],
  -- Pexp <- mk_mapp ``saiga.exp [some cfg],
  -- T1_to_exp <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) T1 Texp),
  f <- mk_meta_var Texp,
  p <- mk_meta_var Texp,
  appexpr <- mk_mapp ``saiga.exp.eApp [some cfg, some f, some p],
  (do unify appexpr e, return p) <|> return none

meta def isAppExpr (e:expr) : tactic bool :=
  do opt <- getAppExpr e, return(isSome opt)

-- meta def isConfig (e:expr) : tactic unit :=
--   do cfg <- mk_meta_var `(saiga.saigaconfig),
--   N <- mk_meta_var `(Type),
--   A <- mk_meta_var `(Type),
--   TAU <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) A `(Type)),
--   RHO <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) A `(Type)),
--   GAM <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) A `(bool)),
--   NN <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) `(nat) N),
--   NDEC <- mk_app `decidable_eq [N],
--   mkcfg <- mk_mapp `saiga.saigaconfig.mk [some N, some A, some TAU, some RHO, some GAM, some NN, some NDEC],
--     unify mkcfg e

meta def getAttrExpr (e:expr) : tactic (option (expr × expr)) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  Texp <- mk_mapp ``saiga.exp [some cfg],
  n <- mk_meta_var Texp,
  a <- mk_meta_var (expr.app `(saiga.saigaconfig.A) (cfg)),
  p <- mk_meta_var Texp,
  attrexpr <- mk_mapp ``saiga.exp.eAttr [some cfg, some n, some a, some p],
  (do unify attrexpr e, return (n, p)) <|> return none

meta def isAttrExpr (e:expr) : tactic bool :=
  do opt <- getAttrExpr e, return(isSome opt)

-- inductive exp : Type 1
-- | eVal:   ∀ {T:sType}, typeof T -> exp
-- | eCond:  exp -> exp -> exp -> exp
-- | eApp:   sType -> sType -> exp -> exp -> exp
-- | eAttr:  exp -> cfg.A -> exp -> exp
-- | eCache: ∀ (n:node) (a:cfg.A), typeof cfg.ρ a -> exp -> exp
-- | eMk:    napply cfg exp -> exp

meta def getCacheExpr (e:expr) : tactic (option expr) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  Texp <- mk_mapp ``saiga.exp [some cfg],
  n <- mk_meta_var `(saiga.node),
  a <- mk_meta_var (expr.app `(saiga.saigaconfig.A) (cfg)),
  p <- mk_meta_var (expr.app `(saiga.sType.tp) (expr.app (expr.app `(saiga.saigaconfig.ρ) (cfg)) a)),
  c <- mk_meta_var Texp,
  cacheexpr <- mk_mapp ``saiga.exp.eCache [some cfg, some n, some a, some p, some c],
  (do unify cacheexpr e, return c) <|> return none

meta def isCacheExpr (e:expr) : tactic bool :=
  do opt <- getCacheExpr e, return(isSome opt)


meta def isNApply (e:expr) : tactic (option expr × option expr) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),   -- a config instance
  Texp <- mk_mapp `saiga.exp [some cfg],        -- the Type "exp cfg"
  n <- mk_meta_var `(nat),                      -- a nat instance
  ap <- mk_mapp ``saiga.ap [some cfg],          -- the Type "ap cfg"
  prod <- mk_mapp ``prod [some ap, some Texp],  -- the Type (ap cfg × exp cfg)
  ap_instance <- mk_meta_var ap,
  e_instance <- mk_meta_var Texp,
  prod_instance <- mk_mapp `prod.mk [none, none, some ap_instance, some e_instance],
  listprod <- mk_mapp ``list [some prod],       -- the Type (list (ap cfg × exp cfg))
  listprod_instance <- mk_meta_var listprod,    -- an instance of (list (ap cfg × exp cfg))
  listprod_instance_tail <- mk_meta_var listprod,    -- another instance of (list (ap cfg × exp cfg))
  listprod_notnil <- mk_mapp ``list.cons [some prod, some prod_instance, some listprod_instance_tail],
  -- an instance of (nat -> (list (ap cfg × exp cfg)))
  nat_to_listprod <- mk_meta_var (expr.pi (name.anonymous) (binder_info.default) `(nat) listprod),
  -- the type "napply.empty cfg nat_to_listprod"
  nap_empty <- mk_mapp `saiga.napply.empty [some cfg, none, some nat_to_listprod],
  -- the type "napply.full cfg n listprod_instance"
  nap_full <- mk_mapp `saiga.napply.full [some cfg, some Texp, some n, some listprod_instance],
  -- attempt to unify, return contents in "option" form
  (do unify nap_empty e, return (none, none))
  <|>
  (do unify nap_full e, unify listprod_notnil listprod_instance, return (some listprod_instance, some e_instance))
  <|>
  (do unify nap_full e, return (some listprod_instance, none))

meta def isNewExpr (e:expr) : tactic (bool) :=
  do cfg <- mk_meta_var `(saiga.saigaconfig),
  newexpr <- mk_mapp `saiga.exp.eNew [some cfg],
  (do unify newexpr e, return tt) <|> return ff

-- meta def isMkExpr (e:expr) : tactic (option expr) :=
--   do cfg <- mk_meta_var `(saiga.saigaconfig),
--   -- Texp <- mk_meta_var (expr.app `(saiga.exp) cfg),
--   Texp <- mk_mapp `saiga.exp [some cfg],
--   napp <- mk_meta_var (expr.app (expr.app `(saiga.napply) (cfg)) Texp),
--   mkexpr <- mk_mapp `saiga.exp.eMk [some cfg, some napp],
--   (do unify mkexpr e, isNApply napp, return napp) <|> return none


def prod2 := @prod.mk



-- meta def testing : tactic unit :=
--   do g1 :: gr <- get_goals,
--      env ← get_env,
--      g <- infer_type g1,
--      match g with
--      | `(%%c; _|_ ▹ _ ▹ _ ⊨ %%e ⟹ _ ⊨ _) := do x <- isMkExpr e, j <- isNApply x, trace j
--      | _ := fail "not a multistep"
--      end

-- set_option trace.app_builder true
-- lemma auto_unfold_contsdfext: ∀ {ctx'} {n:cfg.N} {a:cfg.A} {p:cfg.ρ a},
--   (cfg; 1|50 ▹ ff ▹ emptyC cfg ⊨  (MK 6 ! []) ⟹ ctx' ⊨ 6)
-- := begin
--   intros,
--   testing,
--   admit,
-- end

-- meta def isValExpr (e:expr) : tactic unit :=
--   do type ← to_expr ``(_),
--   c ← mk_meta_var type,
--   to_expr ``(‹_›) >>= unify e

meta def isTrueValExpr (e:expr) : tactic bool :=
  (do to_expr ``({{stt}}) >>= unify e, return true) <|> (return false)

meta def isFalseValExpr (e:expr) : tactic bool :=
  (do to_expr ``({{sff}}) >>= unify e, return true) <|> (return false)


     

meta def takemultistep : tactic unit :=
  do g1 :: gr <- get_goals,
     env ← get_env,
     g <- infer_type g1,
     match g with
     | `(%%c; _|_ ▹ _ ▹ _ ⊨ %%e ⟹ _ ⊨ _) := (do isValExpr e, to_expr ``(saiga.multistep.refl) >>= apply, skip)
                                         <|> (do to_expr ``(saiga.multistep.multi) >>= apply, skip)
     | _ := fail "goal is not a multistep"
     end

meta def takestep : tactic unit :=
  do g1 :: gr <- get_goals,
     env ← get_env,
     g <- infer_type g1,
     match g with
     | `(%%c;%%b ▹▹ _ ▹ _ ⊨ %%e ⟶ _ ⊨ _) := 

      do val   <- isValExpr e,
         cond  <- getCondExpr e,
         app   <- getAppExpr e,
         attr  <- getAttrExpr e,
         cache <- getCacheExpr e,
         nw    <- isNewExpr e,

      match (val, cond, app, attr, cache, nw) with
      | (tt, _, _, _, _, _) := do fail "can't step value"
      | (_, some (c, t, e), _, _, _, _) := (do istt <- isTrueValExpr c, isff <- isFalseValExpr c, isv <- isValExpr c,
                                            if istt
                                          then do to_expr ``(saiga.step.condTrue) >>= apply, skip
                                          else if isff
                                             then do to_expr ``(saiga.step.condFalse)  >>= apply, skip
                                             else if isv
                                                then fail "non-boolean condition value"
                                                else do to_expr ``(saiga.step.condLeft)  >>= apply, skip
                                            )
      | (_, _, some p, _, _, _) := (do isv <- isValExpr p,
                                    if isv
                                  then do to_expr ``(saiga.step.appApp) >>= apply, skip
                                  else do to_expr ``(saiga.step.appRight) >>= apply, skip
                                   )
      | (_, _, _, some (n, p), _, _) := (do nv <- isValExpr n, pv <- isValExpr p,
                                         if nv
                                       then if pv
                                          then do to_expr ``(saiga.step.attrApp) >>= apply, skip
                                          else do to_expr ``(saiga.step.attrParam) >>= apply, skip
                                       else do to_expr ``(saiga.step.attrNode) >>= apply, skip
                                        )
      | (_, _, _, _, some c, _) := (do cv <- isValExpr c,
                                    if cv
                                  then do to_expr ``(saiga.step.cacheDiscard) >>= apply, any_goals (solve1 (do right, reflexivity)) -- TODO: THERE WILL BE ANOTHER THING ABOUT GAMMA ETC
                                  else do to_expr ``(saiga.step.cacheLeft) >>= apply, skip
                                   )
      | (_, _, _, _, _, tt) := (do to_expr ``(saiga.step.new) >>= apply, skip)
      | (_, _, _, _, _, _) := do fail "can't step value"
      end
    | _ := fail "goal is not a step"
    end

meta def takebigstep : tactic unit :=
  do g1 :: gr <- get_goals,
     env ← get_env,
     g <- infer_type g1,
     match g with
     | `(%%c;_|_ ▹ %%b ▹ _ ⊨ %%e »» _ ⊨ _) := 

      do val   <- isValExpr e,
         cond  <- getCondExpr e,
         app   <- getAppExpr e,
         attr  <- getAttrExpr e,
         cache <- getCacheExpr e,
         nw    <- isNewExpr e,

      match (val, cond, app, attr, cache, nw) with
      | (tt, _, _, _, _, _) := do fail "can't step value"
      | (_, some (c, t, e), _, _, _, _) := (do istt <- isTrueValExpr c, isff <- isFalseValExpr c, isv <- isValExpr c,
                                            if istt
                                          then do to_expr ``(saiga.bigstep.condTrue) >>= apply, skip
                                          else if isff
                                             then do to_expr ``(saiga.bigstep.condFalse)  >>= apply, skip
                                             else if isv
                                                then fail "non-boolean condition value"
                                                else do to_expr ``(saiga.bigstep.condLeft)  >>= apply, skip
                                            )
      | (_, _, some p, _, _, _) := (do isv <- isValExpr p,
                                    if isv
                                  then do to_expr ``(saiga.bigstep.appApp) >>= apply, skip
                                  else do to_expr ``(saiga.bigstep.appRight) >>= apply, skip
                                   )
      | (_, _, _, some (n, p), _, _) := (do nv <- isValExpr n, pv <- isValExpr p,
                                         if nv
                                       then if pv
                                          then do to_expr ``(saiga.bigstep.attrApp) >>= apply, skip
                                          else do to_expr ``(saiga.bigstep.attrParam) >>= apply, skip
                                       else do to_expr ``(saiga.bigstep.attrNode) >>= apply, skip
                                        )
      | (_, _, _, _, some c, _) := (do cv <- isValExpr c,
                                    if cv
                                  then do to_expr ``(saiga.bigstep.cacheDiscard) >>= apply, any_goals (solve1 (do right, reflexivity)) -- TODO: THERE WILL BE ANOTHER THING ABOUT GAMMA ETC
                                  else do to_expr ``(saiga.bigstep.cacheLeft) >>= apply, skip
                                   )
      | (_, _, _, _, _, tt) := (do to_expr ``(saiga.bigstep.new) >>= apply, skip)
      | (_, _, _, _, _, _) := do fail "can't step value"
      end
    | _ := fail "goal is not a step"
    end

-- meta def takestep_orElseMethod : tactic unit :=
--   do g1 :: gr <- get_goals,
--      env ← get_env,
--      g <- infer_type g1,
--      match g with
--      | `(%%c;%%b ▹▹ _ ▹ _ ⊨ %%e ⟶ _ ⊨ _) := do (

--       -- val
--       (do isValExpr e, fail "a value cannot step")

--       --TODO: I need to not fall down to app if anything fails after the "getCondExpr" (for example)
--       <|> -- cond
--       (do (c, t, e) <- getCondExpr e,
--             (do isTrueValExpr c,  to_expr ``(saiga.step.condTrue)  >>= apply)
--         <|> (do isFalseValExpr c, to_expr ``(saiga.step.condFalse) >>= apply)
--         <|> (do isValExpr c, fail "non-boolean condition value")
--         <|> (to_expr ``(saiga.step.condLeft) >>= apply)
--         -- (do v <- isValExpr c, trace v, match v with
--                               -- | `(tt) := do to_expr ``(saiga.step.condTrue) >>= apply
--                               -- | `(ff) := do to_expr ``(saiga.step.condFalse) >>= apply
--                               -- | _ := do trace "nonbool", fail "condition stepped to a non-boolean value"
--                               -- end
--         -- ) <|> (do to_expr ``(saiga.step.condLeft) >>= apply)
--       )

--       <|> -- app
--       (do p <- getAppExpr e, (
--             (do isValExpr p, to_expr ``(saiga.step.appApp) >>= apply)
--         <|> (do to_expr ``(saiga.step.appRight) >>= apply)
--       ))

--       <|> -- attr
--       (do (n, p) <- getAttrExpr e, (
--             (do isValExpr n, isValExpr p, to_expr ``(saiga.step.attrApp) >>= apply)
--         <|> (do isValExpr n, to_expr ``(saiga.step.attrParam) >>= apply)
--         <|> (do to_expr ``(saiga.step.attrNode) >>= apply)
--       ))

--       <|> -- cache
--       (do c <- getCacheExpr e, (
--             (do isValExpr c, (
--               match b with 
--               | `(tt) := fail "cache writing is not yet implemented"
--               | `(ff) := do to_expr ``(saiga.step.cacheDiscard) >>= apply, any_goals (solve1 (do right, reflexivity)) -- TODO: THERE WILL BE ANOTHER THING ABOUT GAMMA ETC
--               | _ := fail "don't recognise caching flag"
--               end
--             ))
--         <|> (do to_expr ``(saiga.step.cacheLeft) >>= apply)
--       ))

--       <|> -- mk
--       (do na <- isMkExpr e, (ol, oh) <- isNApply na,
--             match ol, oh with
--               | none, _          := do to_expr ``(saiga.step.mkNode) >>= apply
--               | some _, none     := do to_expr ``(saiga.step.mkReturn) >>= apply
--               | some _, some he  := (do isValExpr he, to_expr ``(saiga.step.mkWrite) >>= apply) <|> (to_expr ``(saiga.step.mkStep) >>= apply)
--               -- | _, _          := do fail "shit. this tactic has reached a state it should never reach. i guess i fucked something up."
--             end
--       )
--      )
--     | _ := fail "goal is not a step"
--     end

-- meta def takestep222 : tactic unit :=
--   do g1 :: gr <- get_goals,
--      env ← get_env,
--      g <- infer_type g1,
--      match g with
--      | `(%%c;%%b ▹▹ _ ▹ _ ⊨ %%e ⟶ _ ⊨ _) := do (

--       -- val
--       (do isValExpr e, fail "a value cannot step")

--       --TODO: I need to not fall down to app if anything fails after the "getCondExpr" (for example)
--       <|> -- cond
--       (do trace e, (c, t, e) <- getCondExpr e, trace "cond",
--         (do v <- isValExpr c, trace v, match v with
--                               -- | `(tt) := do to_expr ``(saiga.step.condTrue) >>= apply
--                               -- | `(ff) := do to_expr ``(saiga.step.condFalse) >>= apply
--                               | _ := fail "oh shit"
--                               end
--         ) <|> (do trace "doing condleft", to_expr ``(saiga.step.condLeft) >>= apply)
--       )
--      )
--     | _ := fail "goal is not a step"
--     end




















      -- <|>
      --  match e with
       

      -- | `(IFF %%c THEN _ ELSE _)  :=  (do isValExpr  c, to_expr ``(saiga.step.condTrue) >>= apply)
      --                             <|> (do isValExpr c, to_expr ``(saiga.step.condFalse) >>= apply)
      --                             <|> (do isValExpr c, fail "condition value is not a boolean")
      --                             <|> (do to_expr ``(saiga.step.condLeft) >>= apply)
      
      
      -- (do isValExpr c, match c with
      --                                 | `(‹tt›) := do to_expr ``(saiga.step.condTrue) >>= apply
      --                                 | `(‹ff›) := do to_expr ``(saiga.step.condTrue) >>= apply
      --                                 | _ := fail "condition contains a non-boolean value"
      --                                 end)
                                  -- <|> (do to_expr ``(saiga.step.condLeft) >>= apply)

      --  | `(IFF ‹tt› THEN _ ELSE _)   := do to_expr ``(saiga.step.condTrue) >>= apply
      --  | `(IFF ‹ff› THEN _ ELSE _)   := do to_expr ``(saiga.step.condFalse) >>= apply
      --  | `(IFF ‹_› THEN _ ELSE _)    := do fail "condition contains a non-boolean value"
      --  | `(IFF _ THEN _ ELSE _)      := do to_expr ``(saiga.step.condLeft) >>= apply

    --    | `(_ OF ‹_›)   := do to_expr ``(saiga.step.appApp) >>= apply
    --    | `(_ OF  _ )   := do to_expr ``(saiga.step.appRight) >>= apply

    --    | `(‹_› DOT _ WITH ‹_›)   := do to_expr ``(saiga.step.attrApp) >>= apply
    --    | `(‹_› DOT _ WITH  _ )   := do to_expr ``(saiga.step.attrParam) >>= apply
    --    | `( _  DOT _ WITH  _ )   := do to_expr ``(saiga.step.attrNode) >>= apply


    --    | `(_/_/_ ;= %%c)   := (do isValExpr c, match b with
    --                           | `(ff) := do to_expr ``(saiga.step.cacheDiscard) >>= apply, any_goals (solve1 (do right, reflexivity))
    --                           | `(tt) := do fail "cache write not yet implemented"
    --                           | _ := do fail "b is somehow not a boolean. this should never happen."
    --                           end)
    --                       <|> do to_expr ``(saiga.step.cacheLeft) >>= apply

    --    | `(MK? _)                            := do to_expr ``(saiga.step.mkNode) >>= apply
    --    | `(MK _ ! [])                        := do to_expr ``(saiga.step.mkReturn) >>= apply
    --    | `(MK _ ! ((_, ‹_›) :: _))   := do to_expr ``(saiga.step.mkWrite) >>= apply
    --    | `(MK _ ! ((_,  _ ) :: _))   := do to_expr ``(saiga.step.mkStep) >>= apply

    --    | _ := fail "Not enough info to infer step to take"
    --    end
    --  | _ := fail "goal is not a step"
    --  end


@[simp, reducible] lemma dite_neg {Ta} {Tret} {a1 a2:Ta} [h : decidable_eq Ta] (hnc : ¬(a1 = a2))
      {t : (a1 = a2) → Tret} {e : ¬ (a1 = a2) → Tret} : dite (a1 = a2) t e = e hnc
:= begin
  rewrite dif_neg,
end

@[simp, reducible] lemma dite_refl {T out:Type} [decidable_eq T] (a:T) {t : (a = a) → out} {e : ¬ (a = a) → out} : dite (a = a) t e = t (eq.refl a)
:= dif_pos (eq.refl a)

@[simp, reducible] lemma ite_neg {Ta} {Tret} {a1 a2:Ta} [h : decidable_eq Ta] (hnc : ¬(a1 = a2))
      {t e:Tret} : ite (a1 = a2) t e = e
:= begin
  cases h a1 a2, {
    unfold ite,
  }, { 
    cases (hnc h_1),
  },
end


meta def toname (s:string) : tactic name :=
  do return s

meta def dite_absurd: tactic unit :=
  do z <- toname "z",
    to_expr ``(dite_neg) >>= rewrite_target, any_goals (solve1 (do intro z, zz <- get_local z, cases zz, skip))

meta def ite_absurd: tactic unit :=
  do z <- toname "z",
    to_expr ``(ite_neg) >>= rewrite_target, any_goals (solve1 (do intro z, zz <- get_local z, cases zz, skip))

  -- do rewrite ``(dif_neg), rotate 1, intro z, solve1 {cases z},
set_option trace.app_builder true

lemma takestep_cond_tt: ∀ {ctx:context _} {b} {eT eF},
  exists ctx' n,
    (cfg; b ▹▹ 5 ▹ ctx ⊨ IFF {{stt}} THEN eT ELSE eF ⟶ ctx' ⊨ n)
:= begin
  intros,
  existsi _, existsi _,
  takestep,
end

lemma takestep_cond_ff: ∀ {ctx:context _} {b} {eT eF},
  exists ctx' n,
    (cfg; b ▹▹ 5 ▹ ctx ⊨ IFF {{sff}} THEN eT ELSE eF ⟶ ctx' ⊨ n)
:= begin
  intros,
  existsi _, existsi _,
  takestep,
end

-- lemma takestep_cond_step: ∀ {ctx:context_} {b} {eT eF} {e},
--   exists ctx' n,
--     (cfg; b ▹▹ 5 ▹ ctx ⊨ IFF e THEN eT ELSE eF ⟶ ctx' ⊨ n)
-- := begin
--   intros,
--   existsi _, existsi _,
--   takestep, -- right now this is failing as expected
-- end















