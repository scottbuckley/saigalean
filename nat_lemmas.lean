-- shorthand
local prefix S:100 := nat.succ


/-
    ##       ####  ######  ########  ######
    ##        ##  ##    ##    ##    ##    ##
    ##        ##  ##          ##    ##
    ##        ##   ######     ##     ######
    ##        ##        ##    ##          ##
    ##        ##  ##    ##    ##    ##    ##
    ######## ####  ######     ##     ######
-/

lemma list_neq_cons : ∀ {T:Type} {l:list T} {h:T},
  l ≠ h :: l
:= begin
  intros, induction l generalizing h,
  intro a, contradiction,
  intro a, have hh := list.cons.inj a,
  cases hh with hh1 hh2,
  have hh4 := l_ih hh2, apply hh4,
end

theorem append_nil_left {T:Type} (t : list T) :
  [] ++ t = t
:= rfl



lemma list.append_eq_nil {T:Type}: ∀ {l1 l2:list T},
  l1 ++ l2 = [] ->
  l1 = [] ∧ l2 = []
:= begin
  introv, intros hs,
  induction l1 generalizing l2, {
    simp at hs, split,
    reflexivity, assumption,
  }, {
    cases hs,
  },
end







/-
    ##    ##    ###    ########  ######
    ###   ##   ## ##      ##    ##    ##
    ####  ##  ##   ##     ##    ##
    ## ## ## ##     ##    ##     ######
    ##  #### #########    ##          ##
    ##   ### ##     ##    ##    ##    ##
    ##    ## ##     ##    ##     ######
-/

lemma succ_eq_succ: ∀ {a b:nat},
  a = b -> S a = S b
:= begin
  intros, rewrite a_1,
end

lemma add_succ_succ_add: ∀ {a b},
  a + S b = S a + b
:= begin
  intros, induction a,
  rewrite nat.zero_add, rewrite nat.succ_eq_add_one, rewrite nat.add_comm,
  rewrite nat.succ_add, rewrite a_ih, rewrite <-nat.succ_add,
end

lemma eq_add_eq_zero: ∀ {a b},
  a = a + b ->
  b = 0
:= begin
  intros, induction a generalizing b, {
    simp at a_1, symmetry, exact a_1,
  }, {
    apply a_ih, rewrite nat.succ_add at a_1,
    injection a_1,
  },
end

lemma le_lt_succ: forall {a b},
  a < nat.succ b -> a ≤ b
:= begin
  intros, cases a_1, constructor,
  exact nat.le_of_succ_le a_1_a,
end

lemma lt_succ_of_lt: ∀ {a b},
  a < b ->
  a < S b
:= begin
  intros, constructor,
  cases a_1, {
    constructor,
  }, {
    constructor, exact a_1_a,
  },
end

lemma lt_succ: ∀ {a},
  a < S a
:= begin
  intros, constructor,
end

lemma lt_succ_add: forall {a b},
  a < S (a + b)
:= begin
  intros,
  induction b, simp, constructor,
  rewrite nat.add_succ,
  apply nat.lt_succ_of_lt,
  exact b_ih,
end

lemma lt_succ_add3: forall {a b c},
  a < S (a + b + c)
:= begin
  intros, rewrite nat.add_assoc, apply lt_succ_add,
end

lemma lt_of_gt: forall {a b:nat},
  a > b ->
  b < a
:= begin
  introv, intros h, exact h,
end

lemma gt_of_lt: forall {a b:nat},
  a < b ->
  b > a
:= begin
  introv, intros h, exact h,
end




/-
    ########   #######   #######  ##          ########  #######
    ##     ## ##     ## ##     ## ##          ##       ##     ##
    ##     ## ##     ## ##     ## ##          ##       ##     ##
    ########  ##     ## ##     ## ##          ######   ##     ##
    ##     ## ##     ## ##     ## ##          ##       ##  ## ##
    ##     ## ##     ## ##     ## ##          ##       ##    ##
    ########   #######   #######  ########    ########  ##### ##
-/

@[reducible]
def nat.beq: nat -> nat -> bool
| 0 0                       := tt
| (nat.succ a) (nat.succ b) := nat.beq a b
| _ _                       := ff

-- boolean if/then/else
@[reducible, simp]
def bite {T}: bool -> T -> T -> T
| tt a _ := a
| ff _ b := b






/-
    ##     ## ########  #######
    ##     ## ##       ##     ##
    ##     ## ##       ##     ##
    ######### ######   ##     ##
    ##     ## ##       ##  ## ##
    ##     ## ##       ##    ##
    ##     ## ########  ##### ##
-/

lemma heq_type_eq: ∀ {T1 T2:Type} {v1:T1} {v2:T2},
  v1 == v2 ->
  T1 = T2
:= begin
  introv, intros he,
  cases he, reflexivity,
end

