import .saiga
open saiga

variable cfg: saigaconfig




/-
 #### ##    ## ########  ##     ##  ######  ######## ####  #######  ##    ##
  ##  ###   ## ##     ## ##     ## ##    ##    ##     ##  ##     ## ###   ##
  ##  ####  ## ##     ## ##     ## ##          ##     ##  ##     ## ####  ##
  ##  ## ## ## ##     ## ##     ## ##          ##     ##  ##     ## ## ## ##
  ##  ##  #### ##     ## ##     ## ##          ##     ##  ##     ## ##  ####
  ##  ##   ### ##     ## ##     ## ##    ##    ##     ##  ##     ## ##   ###
 #### ##    ## ########   #######   ######     ##    ####  #######  ##    ##
-/

-- strong induction for expressions
theorem exp.strong_induction_on: ∀ {P : exp cfg → Prop} (e : exp cfg),
  (∀ (e : exp cfg),
    (∀ {e' : exp cfg},
      sizeof e' < sizeof e → P e') → P e) → P e
:= begin
  introv, intros hMY,
  generalize hk: (sizeof e) = kk, revert e,
  induction kk using nat.strong_induction_on, rename kk_a ih,
  simp at *, intros eZ hn, rewrite <- hn at *, clear hn kk_n,
  apply hMY, intros, apply ih, exact a, reflexivity,
end


-- manual induction principle
theorem exp.induction_on: ∀ (C : exp cfg → Prop),
  (Π {T : Type} (v : T), C {{v}}) ->
  (Π (eC eT eF : exp cfg) (ihc: C eC) (iht:C eT) (ihf:C eF), C (IFF eC THEN eT ELSE eF)) ->
  (Π (eP eF : exp cfg) (ihp:C eP) (ihf:C eF), C (eF OF eP)) ->
  (Π (eN : exp cfg) (a : cfg.A) (eP : exp cfg) (ihn:C eN) (ihp:C eP), C (eN DOT a WITH eP)) ->
  (Π (n : node) (a : cfg.A) (p : cfg.ρ a) (eC : exp cfg) (ihc:C eC), C (n / a / p ;= eC)) ->
  (Π (fl : node → list (exp cfg × ap cfg)), C (exp.eMk fl)) → 
  Π (x : exp cfg), C x
:= begin
  intros P ihval ihcond ihapp ihattr ihcache ihmk e,
  induction e, {
    apply ihval,
  }, {
    apply ihcond; assumption,
  }, {
    apply ihapp; assumption,
  }, {
    apply ihattr; assumption,
  }, {
    apply ihcache; assumption,
  }, {
    apply ihmk,
  },
end


/-
 ##     ##    ###    ##       ##     ## ########
 ##     ##   ## ##   ##       ##     ## ##
 ##     ##  ##   ##  ##       ##     ## ##
 ##     ## ##     ## ##       ##     ## ######
  ##   ##  ######### ##       ##     ## ##
   ## ##   ##     ## ##       ##     ## ##
    ###    ##     ## ########  #######  ########
-/

lemma notvalue.value.contra {cfg:saigaconfig} : ∀ {T:Type} {v:T},
  @notvalue cfg {{v}} -> false
:= begin
  intros, cases a,
end

lemma value_or_notvalue_or_ho : ∀ {cfg:_} (e:exp cfg),
  value e ∨ notvalue e ∨ exists al, e = exp.eMk al
:= begin
  intros, cases e, {
    left, constructor,
  }, {
    right, left, constructor,
  }, {
    right, left, constructor,
  }, {
    right, left, constructor,
  }, {
    right, left, constructor,
  }, {
    right, right, constructor, constructor,
  }
end

lemma value_not_notvalue : ∀ {cfg:_} {e:exp cfg},
  value e ->
  ¬notvalue e
:= begin
  introv, intros h1 h2,
  cases h1, cases h2,
end




/-
  ######   #######  ##    ## ######## ######## ##     ## ########
 ##    ## ##     ## ###   ##    ##    ##        ##   ##     ##
 ##       ##     ## ####  ##    ##    ##         ## ##      ##
 ##       ##     ## ## ## ##    ##    ######      ###       ##
 ##       ##     ## ##  ####    ##    ##         ## ##      ##
 ##    ## ##     ## ##   ###    ##    ##        ##   ##     ##
  ######   #######  ##    ##    ##    ######## ##     ##    ##
-/

lemma ctxN.mk.inj2 {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg},
  c1.c = c2.c ->
  c1.N = c2.N ->
  c1 = c2
:= begin
  introv, intros heq1 heq2,
  cases c1, cases c2,
  simp at heq1, simp at heq2,
  rewrite heq1, rewrite heq2,
end



/-
 ##    ##    ###    ########  ##     ##    ###    ########  ######  ##     ##
 ###   ##   ## ##   ##     ## ###   ###   ## ##      ##    ##    ## ##     ##
 ####  ##  ##   ##  ##     ## #### ####  ##   ##     ##    ##       ##     ##
 ## ## ## ##     ## ########  ## ### ## ##     ##    ##    ##       #########
 ##  #### ######### ##        ##     ## #########    ##    ##       ##     ##
 ##   ### ##     ## ##        ##     ## ##     ##    ##    ##    ## ##     ##
 ##    ## ##     ## ##        ##     ## ##     ##    ##     ######  ##     ##
-/


-- useful lemmas about napmatch
lemma napmatch.asame: ∀ {cfg:saigaconfig} {n n':node} {a a':cfg.A} {p:cfg.ρ a} {p':cfg.ρ a'},
  napmatch cfg n n' a a' p p' = tt ->
  a = a'
:= begin
  intros, unfold napmatch at a_1,
  cases (node.dec_eq n n'),
  { cases a_1, },
  { cases h,
    simp at a_1,
    cases (cfg.Adec a a'),
    cases a_1, assumption,
  },
end

lemma napmatch.npsame: ∀ {cfg:saigaconfig} {n n':node} {a:cfg.A} {p p':cfg.ρ a},
  napmatch cfg n n' a a p p' = tt ->
  n' = n ∧ p' = p
:= begin
  intros,
  unfold napmatch at a_1,
  simp at a_1,
  cases (node.dec_eq n n'),
  {
    cases a_1, cases a_1_left, cases a_1_right,
    split, reflexivity, reflexivity,
  },
  {
    cases a_1, cases a_1_left, cases a_1_right,
    split; reflexivity,
  },
end

lemma napmatch.id: ∀ (n:node) (a:cfg.A) (p:cfg.ρ a),
  (napmatch cfg n n a a p p) = tt
:= begin
  intros, unfold napmatch, simp,
  rewrite cast_eq,
end

lemma napmatch.symm: ∀ {n n':node} {a a':cfg.A} {p:cfg.ρ a} {p':cfg.ρ a'},
  napmatch _ n n' a a' p p' = napmatch _ n' n a' a p' p
:= begin
  intros, unfold napmatch,
  cases (node.dec_eq n n'); cases (node.dec_eq n' n),
  unfold ite,
  cases h_1, contradiction,
  cases h, contradiction,
  cases h,
  simp,
  cases (cfg.Adec a a'); cases (cfg.Adec a' a),
  unfold dite,
  cases h_3, contradiction,
  cases h_2, contradiction,
  unfold dite,
  cases h_2,
  cases (cfg.ρdec a p p'); cases (cfg.ρdec a p' p),
  unfold ite,
  cases h_5, contradiction,
  cases h_4, contradiction,
  unfold ite,
end

lemma napmatch.trans: ∀ {n1 n2 n3:node} {a1 a2 a3:cfg.A}
    {p1:cfg.ρ a1} {p2:cfg.ρ a2} {p3:cfg.ρ a3},
  napmatch _ n1 n2 a1 a2 p1 p2 = tt ->
  napmatch _ n2 n3 a2 a3 p2 p3 = tt ->
  napmatch _ n1 n3 a1 a3 p1 p3 = tt
:= begin
  introv, intros hn1 hn2,
  cases (napmatch.asame hn1),
  cases (napmatch.asame hn2),
  cases (napmatch.npsame hn1),
  cases (napmatch.npsame hn2),
  cases left, cases right, cases left_1, cases right_1,
  apply napmatch.id,
end


lemma napmatch.ff_update_unused_CN: ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {v:cfg.τ a},
  napmatch cfg n n' a a' p p' = ff ->
  (extendCN cfg n a p v ctx).c n' a' p' = ctx.c n' a' p'
:= begin
  intros, cases ctx,
  unfold extendCN, unfold extendC, simp, rewrite a_1,
end

lemma napmatch.ff_update_unused_EN: ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {e:exp cfg},
  napmatch cfg n n' a a' p p' = ff ->
  (extendEN cfg n a p e ctx).c n' a' p' = ctx.c n' a' p'
:= begin
  intros, cases ctx,
  unfold extendEN, simp,
  unfold extendE, rewrite a_1,
end






/-
 ######## ##     ## ######## ######## ##    ## ########
 ##        ##   ##     ##    ##       ###   ## ##     ##
 ##         ## ##      ##    ##       ####  ## ##     ##
 ######      ###       ##    ######   ## ## ## ##     ##
 ##         ## ##      ##    ##       ##  #### ##     ##
 ##        ##   ##     ##    ##       ##   ### ##     ##
 ######## ##     ##    ##    ######## ##    ## ########
-/


lemma extendCN.inj {cfg:_} : ∀ {c:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a}
        {v:cfg.τ a},
  ((extendCN cfg n a p v c).c = c.c) ->
  (extendCN cfg n a p v c = c)
:= begin
  introv, intros he,
  cases c, unfold extendCN, simp,
  simp at he, unfold extendCN at he, simp at he,
  apply he,
end


-- useful lemmas about extendC
@[reducible]
lemma extendC.match: ∀ {ctx:context _} {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  (extendC cfg n a p v ctx) n a p = {{v}}
:= begin
  intros, unfold extendC, rewrite napmatch.id,
end

lemma extendC.notmatch: ∀ {ctx:context _} {n n':node} {a a':cfg.A} {p:cfg.ρ a} {p':cfg.ρ a'} {v:cfg.τ a},
  napmatch _ n n' a a' p p' = ff ->
  (extendC cfg n a p v ctx) n' a' p' = ctx n' a' p'
:= begin
  intros, unfold extendC, rewrite a_1,
end

lemma extendC.redundant: ∀ {ctx:context _} {n:node} {a:cfg.A} {p:cfg.ρ a} {v1 v2:cfg.τ a},
  (extendC cfg n a p v1 (extendC cfg n a p v2 ctx)) = extendC cfg n a p v1 ctx
:= begin
  intros,
  apply funext, intros n',
  apply funext, intros a',
  apply funext, intros p',
  unfold extendC,
  cases hnap: napmatch cfg n n' a a' p p',
  unfold bite, unfold bite,
end

lemma napmatch.ff_extend_flip: ∀ {ctx:context _} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {v:cfg.τ a} {v':cfg.τ a'},
  napmatch cfg n n' a a' p p' = ff ->
  extendC cfg n a p v (extendC cfg n' a' p' v' ctx) =
  extendC cfg n' a' p' v' (extendC cfg n a p v ctx)
:= begin
  intros ctx n n' a a' p p' v v' hm,
  apply funext, intros n_,
  apply funext, intros a_,
  apply funext, intros p_,
  unfold extendC,
  cases hmA: napmatch cfg n n_ a a_ p p_,
  { unfold bite, },
  {
    cases hmB: napmatch cfg n' n_ a' a_ p' p_,
    { unfold bite, },
    {
      exfalso,
      cases napmatch.asame hmA,
      cases napmatch.asame hmB,
      cases napmatch.npsame hmA,
      cases napmatch.npsame hmB,
      cases left, cases right, cases left_1, cases right_1,
      rewrite napmatch.id at hm, cases hm,
    },
  },
end


lemma napmatch.ff_extend_flip_CN: ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {v:cfg.τ a} {v':cfg.τ a'},
  napmatch cfg n n' a a' p p' = ff ->
  extendCN cfg n a p v (extendCN cfg n' a' p' v' ctx) =
  extendCN cfg n' a' p' v' (extendCN cfg n a p v ctx)
:= begin
  introv, intros hnap,
  cases ctx with cc cN, unfold extendCN,
  simp, apply napmatch.ff_extend_flip, exact hnap,
end

lemma napmatch.ff_update_unused: ∀ {cfg:saigaconfig} {ctx:context _} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {v:cfg.τ a},
  napmatch cfg n n' a a' p p' = ff ->
  extendC cfg n a p v ctx n' a' p' = ctx n' a' p'
:= begin
  intros,
  unfold extendC, rewrite a_1,
end


lemma extendCN.N : ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  (extendCN cfg n a p v ctx).N = ctx.N
:= begin
  introv,
  cases ctx, unfold extendCN,
end


lemma extendCN.match: ∀ {ctx:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  (extendCN cfg n a p v ctx).c n a p = {{v}}
:= begin
  intros, cases ctx, unfold extendCN, simp,
  unfold extendC, rewrite napmatch.id,
end

lemma extendEN.match: ∀ {ctx:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {e:exp cfg},
  (extendEN cfg n a p e ctx).c n a p = e
:= begin
  intros, cases ctx, unfold extendEN, simp,
  unfold extendE, rewrite napmatch.id,
end

lemma extendCN.redundant: ∀ {cfg:_} {ctx:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {v1 v2:cfg.τ a},
  (extendCN cfg n a p v1 (extendCN cfg n a p v2 ctx)) = extendCN cfg n a p v1 ctx
:= begin
  intros, cases ctx with cc cN,
  unfold extendCN, simp,
  rewrite extendC.redundant,
end

lemma extendEN.N : ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {e:exp cfg},
  (extendEN cfg n a p e ctx).N = ctx.N
:= begin
  introv,
  cases ctx, unfold extendEN,
end



lemma extendCN.addN.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg} {n:node} {a:cfg.A}
        {p:cfg.ρ a} {v:cfg.τ a} {n':node},
    extendCN cfg n a p v (addN n' c) = addN n' (extendCN cfg n a p v c)
:= begin
  introv, cases c with cc cN,
  apply ctxN.mk.inj2, {
    rewrite addN.c, funext n' a' p',
    unfold extendCN, simp, unfold extendC,
    destruct (napmatch cfg n n' a a' p p'); intros hnap, {
      rewrite hnap, simp, rewrite addN.c,
    }, {
      rewrite hnap,
    }
  }, {
    rewrite extendCN.N, unfold addN,
    simp, rewrite extendCN.N,
  }, 
end


lemma addNs.extendCN.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a} {l:list node},
    addNs (extendCN cfg n a p v c) l =
    (extendCN cfg n a p v (addNs c l))
:= begin
  introv, 
  induction l, {
    unfold addNs,
  },
  apply ctxN.mk.inj2, {
    unfold addNs,
    rewrite addN.c,
    rewrite l_ih,
    rewrite extendCN.addN.flip,
    rewrite addN.c,
  }, {
    unfold addNs, rewrite l_ih,
    rewrite extendCN.addN.flip,
  },
end



lemma extendCN.value {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n n':node} {a a':cfg.A} {p:cfg.ρ a} {p':cfg.ρ a'}
        {v:cfg.τ a},
  value (c.c n' a' p') ->
  value ((extendCN cfg n a p v c).c n' a' p')
:= begin
  introv, intros hv,
  destruct (napmatch cfg n n' a a' p p'); intros hnap, {
    rewrite napmatch.ff_update_unused_CN hnap, apply hv,
  }, {
    have hh := napmatch.asame hnap,
    cases hh,
    have hh2 := napmatch.npsame hnap,
    cases hh2 with hh1 hh2,
    cases hh1, cases hh2,
    rewrite extendCN.match, constructor,
  }
end



lemma napmatch.ff_extendEN_flip: ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n n':node} {a a':cfg.A}
        {p:cfg.ρ a} {p':cfg.ρ a'} {e e':exp cfg},
  napmatch cfg n n' a a' p p' = ff ->
  extendEN cfg n a p e (extendEN cfg n' a' p' e' ctx) =
  extendEN cfg n' a' p' e' (extendEN cfg n a p e ctx)
:= begin
  introv, intros hnap,
  apply ctxN.mk.inj2, {
    funext n'' a'' p'', unfold extendEN,
    unfold extendE, simp,
    destruct (napmatch cfg n n'' a a'' p p''); intros hnap1, {
      rewrite hnap1,
    }, {
      have hh := napmatch.asame hnap1, cases hh, clear hh,
      have hh := napmatch.npsame hnap1, cases hh with hh1 hh2,
      cases hh1, cases hh2, clear hh1 hh2,
      rewrite napmatch.id,
      simp, destruct (napmatch cfg n' n a' a p' p); intros hnap2, {
        rewrite hnap2,
      }, {
        have hh := napmatch.asame hnap2, cases hh, clear hh,
        have hh := napmatch.npsame hnap2, cases hh with hh1 hh2,
        cases hh1, cases hh2, clear hh1 hh2,
        rewrite napmatch.id at *, cases hnap,
      },
    },
  }, {
    rewrite extendEN.N, rewrite extendEN.N,
    rewrite extendEN.N, rewrite extendEN.N,
  },
end




/-
    ##      ## ########  #### ######## ########    ###    ##
    ##  ##  ## ##     ##  ##     ##    ##         ## ##   ##
    ##  ##  ## ##     ##  ##     ##    ##        ##   ##  ##
    ##  ##  ## ########   ##     ##    ######   ##     ## ##
    ##  ##  ## ##   ##    ##     ##    ##       ######### ##
    ##  ##  ## ##    ##   ##     ##    ##       ##     ## ##
     ###  ###  ##     ## ####    ##    ######## ##     ## ########
-/


lemma writeAL.cons {cfg:saigaconfig} : ∀ {c:ctxN cfg} {e:exp cfg} {hap:ap cfg} {al:list _} {n:node},
  writeAL c n ((e, hap) :: al) = extendEN cfg n (hap.attr) (hap.param) e (writeAL c n al)
:= begin
  introv,
  unfold writeAL,
end

lemma writeAL.N : ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n:node} {al},
  ((writeAL ctx n al).N) = ctx.N
:= begin
  introv, induction al, {
    unfold writeAL,
  }, {
    cases ctx, cases al_hd, unfold writeAL, simp,
    simp at al_ih, rewrite extendEN.N, apply al_ih,
  }  
end

lemma addN.writeAL.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n n':node} {al:list _},
    addN n' (writeAL c n al) = (writeAL (addN n' c) n al)
:= begin
  introv,
  apply ctxN.mk.inj2, {
    rewrite addN.c, induction al, {
      unfold writeAL, rewrite addN.c,
    }, {
      cases al_hd with e' ap',
      unfold writeAL,
      funext n'' a'' p'',
      cases c with cc cN,
      unfold extendEN, simp,
      unfold extendE, 
      destruct (napmatch cfg n n'' (ap'.attr) a'' (ap'.param) p''); intros hnap, {
        rewrite hnap, simp, rewrite al_ih,
      }, {
        rewrite hnap,
      },
    },
  }, {
    unfold addN, simp, rewrite writeAL.N,
    rewrite writeAL.N,
  }
end



lemma writeAL.addNs.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n:node} {al:list _} {l:list node},
    addNs (writeAL c n al) l =
    (writeAL (addNs c l) n al)
:= begin
  introv, 
  induction l, {
    unfold addNs,
  },
  apply ctxN.mk.inj2, {
    unfold addNs,
    rewrite addN.c,
    rewrite l_ih,
    rewrite <- addN.writeAL.flip,
    rewrite addN.c,
  }, {
    unfold addNs, rewrite l_ih,
    rewrite <- addN.writeAL.flip,
  },
end


lemma writeAL.unused {cfg:saigaconfig}: ∀ {c:ctxN cfg} {n:node} {a:cfg.A} {p:cfg.ρ a} {n':node} {al:list _},
  n ≠ n' ->
  (writeAL c n' al).c n a p = c.c n a p
:= begin
  introv, intros hneq,
  induction al, {
    unfold writeAL,
  }, {
    cases al_hd with e ap', unfold writeAL,
    unfold extendEN, unfold extendE, simp,
    destruct ((napmatch cfg n' n (ap'.attr) a (ap'.param) p)); intros hnap, {
      rewrite hnap, simp, rewrite al_ih,
    }, {
      -- cases ap',
      have hh1 := napmatch.asame hnap, cases hh1,
      have hh2 := napmatch.npsame hnap,
      cases hh2 with hh2 hh3,
      cases hh2, cases hh3,
      contradiction,
    },
  },
end


lemma addN.extendCN.flip {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n n':node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
    addN n' (extendCN cfg n a p v c) =
    (extendCN cfg n a p v (addN n' c))
:= begin
  introv,
  apply ctxN.mk.inj2, {
    rewrite addN.c,
    funext n'' a'' p'',
    destruct (napmatch cfg n n'' a a'' p p''); intros hnap, {
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c,
    }, {
            have hh1 := napmatch.asame hnap,
            cases hh1, clear hh1,
            have hh1 := napmatch.npsame hnap,
            cases hh1 with hh1 hh2,
            cases hh1, cases hh2, clear hh1 hh2,
      rewrite extendCN.match,
      rewrite extendCN.match,
    },
  }, {
    unfold addN, simp,
    rewrite extendCN.N,
    rewrite extendCN.N,
  }
end

lemma extendCN.extendEN {cfg:saigaconfig}: ∀ {ctx:ctxN cfg} {n:node}
        {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  extendEN cfg n a p {{v}} ctx = extendCN cfg n a p v ctx
:= begin
  introv, unfold extendEN, unfold extendCN,
end


lemma writeAL.extendEN.flip {cfg:saigaconfig}: ∀ {c:ctxN cfg} {n n':node} {al:list _}
        {a:cfg.A} {p:cfg.ρ a} {e:exp cfg},
  n ≠ n' ->
  extendEN cfg n a p e (writeAL c n' al) = 
  writeAL (extendEN cfg n a p e c) n' al
:= begin
  introv, intros he, induction al, {
    unfold writeAL,
  }, {
    cases al_hd with e' ap',
    unfold writeAL,
    rewrite <- al_ih,
    rewrite napmatch.ff_extendEN_flip,
    destruct (napmatch cfg n n' a (ap'.attr) p (ap'.param)); intros hnap,
    assumption,
    have hh1 := napmatch.asame hnap,
    cases hh1, clear hh1,
    have hh1 := napmatch.npsame hnap,
    cases hh1 with hh1 hh2,
    cases hh1, cases hh2, clear hh1 hh2,
    contradiction,
  }
end


lemma writeAL.extendCN.flip {cfg:saigaconfig}: ∀ {c:ctxN cfg} {n n':node} {al:list _}
        {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  n ≠ n' ->
  extendCN cfg n a p v (writeAL c n' al) = 
  writeAL (extendCN cfg n a p v c) n' al
:= begin
  introv, intros he,
  rewrite <- extendCN.extendEN,
  rewrite <- extendCN.extendEN,
  apply writeAL.extendEN.flip he,
end


lemma extendCN.HOwrite.flip2 {cfg:saigaconfig} : ∀ {c:ctxN cfg}
        {n n' n_f:node} {a a':cfg.A} {p:cfg.ρ a} {p':cfg.ρ a'}
        {v:cfg.τ a} {v':cfg.τ a'} {al:list _},
  napmatch cfg n n' a a' p p' = ff ->
  n ≠ n_f ->
  (extendCN cfg n a p v (extendCN cfg n' a' p' v' (addN n_f (writeAL c n_f al)))) =
  (extendCN cfg n' a' p' v' (addN n_f (writeAL (extendCN cfg n a p v c) n_f al)))
:= begin
  introv, intros hnap hne,
  apply ctxN.mk.inj2, tactic.rotate 1, {
    repeat {rewrite addN.writeAL.flip},
    rewrite addN.extendCN.flip,
    repeat {rewrite extendCN.N <|> rewrite writeAL.N},
  }, {
    funext n'' a'' p'',
    destruct (napmatch cfg n n'' a a'' p p''); intros hnap1, {
      rewrite napmatch.ff_update_unused_CN hnap1,
      destruct (napmatch cfg n' n'' a' a'' p' p''); intros hnap2, {
        rewrite napmatch.ff_update_unused_CN hnap2,
        rewrite napmatch.ff_update_unused_CN hnap2,
        rewrite addN.c, rewrite addN.c,
        induction al, {
          unfold writeAL,
          rewrite napmatch.ff_update_unused_CN hnap1,
        }, {
          cases al_hd with e' ap',
          rewrite writeAL.cons,
          rewrite writeAL.cons,
          destruct (napmatch cfg n_f n'' (ap'.attr) a'' (ap'.param) p''); intros hnap3, {
            rewrite napmatch.ff_update_unused_EN hnap3,
            rewrite napmatch.ff_update_unused_EN hnap3,
            rewrite al_ih,
          }, {
            have hh1 := napmatch.asame hnap3,
            cases hh1, clear hh1,
            have hh1 := napmatch.npsame hnap3,
            cases hh1 with hh1 hh2,
            cases hh1, cases hh2, clear hh1 hh2,
            -- napmatch_rewrites `hnap3,
            rewrite extendEN.match,
            rewrite extendEN.match,
          },
        },
      }, {
            have hh1 := napmatch.asame hnap2,
            cases hh1, clear hh1,
            have hh1 := napmatch.npsame hnap2,
            cases hh1 with hh1 hh2,
            cases hh1, cases hh2, clear hh1 hh2,
        rewrite extendCN.match,
        rewrite extendCN.match,
      },
    }, {
            have hh1 := napmatch.asame hnap1,
            cases hh1, clear hh1,
            have hh1 := napmatch.npsame hnap1,
            cases hh1 with hh1 hh2,
            cases hh1, cases hh2, clear hh1 hh2,
      rewrite extendCN.match,
      rewrite napmatch.symm at hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c,
      induction al, {
        unfold writeAL,
        rewrite extendCN.match,
      }, {
        cases al_hd with e' np',
        rewrite writeAL.cons,
        destruct (napmatch cfg n_f n (np'.attr) a (np'.param) p); intros hnap2, {
          rewrite napmatch.ff_update_unused_EN hnap2,
          rewrite al_ih,
        }, {
            have hh1 := napmatch.asame hnap2,
            cases hh1, clear hh1,
            have hh1 := napmatch.npsame hnap2,
            cases hh1 with hh1 hh2,
            cases hh1, cases hh2, clear hh1 hh2,
          contradiction,
        }
      }
    }
  }
end




/-
 ######## ########   ######  ##     ##       ##  ######  ##    ## ########
 ##       ##     ## ##    ## ##     ##      ##  ##    ## ###   ##    ##
 ##       ##     ## ##       ##     ##     ##   ##       ####  ##    ##
 ######   ########   ######  #########    ##    ##       ## ## ##    ##
 ##       ##   ##         ## ##     ##   ##     ##       ##  ####    ##
 ##       ##    ##  ##    ## ##     ##  ##      ##    ## ##   ###    ##
 ##       ##     ##  ######  ##     ## ##        ######  ##    ##    ##
-/



-- if ctx contains n, then n is not fresh for ctx
lemma fresh_for.addN.contra {cfg:saigaconfig}: ∀ {n:node} {c:ctxN cfg},
  n = fresh_for (addN n c) ->
  false
:= begin
  introv, intros hn,
  have hh1 := @addN.contained cfg c n,
  have hh2 := fresh_for.contained hh1,
  contradiction,
end


lemma fresh_for.extendCN {cfg:saigaconfig} : ∀ {ctx:ctxN cfg} {n:node}
        {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  fresh_for (extendCN cfg n a p v ctx) = fresh_for ctx
:= begin
  intros, rewrite <- extendCN.extendEN, apply fresh_for.extendEN,
end

lemma fresh_for.writeAL {cfg:saigaconfig} : ∀ {ctx:ctxN cfg} {n:node}
        {al:list _},
  fresh_for (writeAL ctx n al) = fresh_for ctx
:= begin
  intros,
  induction al, {
    unfold writeAL,
  }, {
    cases al_hd with e' ap',
    unfold writeAL,
    rewrite fresh_for.extendEN, apply al_ih,
  },
end


lemma bigstep.contained : ∀ {cfg:saigaconfig} {c1 c2:ctxN cfg}
        {e:exp cfg} {T:Type} {v:T} {n:node},
  contained n c1.N ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  contained n c2.N
:= begin
  introv, intros hc hs,
  induction hs, { -- refl
    assumption,
  }, { -- condT
    apply hs_ih_ts, apply hs_ih_cs, apply hc,
  }, { -- condF
    apply hs_ih_fs, apply hs_ih_cs, apply hc,
  }, { -- app
    apply hs_ih_ps, apply hs_ih_fs, apply hc,
  }, { -- attrValue
    apply hs_ih_cs, apply hs_ih_ps, apply hs_ih_ns, apply hc,
  }, { -- attrCached
    cases hs_ctx4, unfold extendCN, simp,
    apply hs_ih_cs, apply hs_ih_ps, apply hs_ih_ns, apply hc,
  }, { -- attrHO
    rewrite extendCN.N, apply contained.addN,
    rewrite writeAL.N, apply hs_ih_ps,
    apply hs_ih_ns, apply hc,
  }, { -- cache
    cases hs_ctx2, unfold extendCN, simp,
    apply hs_ih, apply hc,
  },
end



/-
  ######   #######  ##     ## ##    ## ########
 ##    ## ##     ## ##     ## ###   ## ##     ##
 ##       ##     ## ##     ## ####  ## ##     ##
  ######  ##     ## ##     ## ## ## ## ##     ##
       ## ##     ## ##     ## ##  #### ##     ##
 ##    ## ##     ## ##     ## ##   ### ##     ##
  ######   #######   #######  ##    ## ########
-/


lemma extend.sound : ∀ {ctx:context _} {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  soundC ctx ->
  soundC (extendC cfg n a p v ctx)
:= begin
  intros,
  constructor; intros; unfold extendC; cases hsnd: (napmatch cfg n n_1 a a_2 p p_1); unfold bite,
  {
    cases a_1, apply a_1_a,
  }, {
    cases napmatch.asame hsnd, constructor,
  }, {
    cases a_1, apply a_1_a_1,
  }, {
    cases napmatch.asame hsnd, constructor,
  },
end


/-
 ######## ##     ## ########      ######  ######## ##     ## ######## ########
 ##        ##   ##  ##     ##    ##    ##    ##    ##     ## ##       ##
 ##         ## ##   ##     ##    ##          ##    ##     ## ##       ##
 ######      ###    ########      ######     ##    ##     ## ######   ######
 ##         ## ##   ##                 ##    ##    ##     ## ##       ##
 ##        ##   ##  ##           ##    ##    ##    ##     ## ##       ##
 ######## ##     ## ##            ######     ##     #######  ##       ##
-/


lemma exp.val.type_eq {cfg:saigaconfig}:
  ∀ {T1 T2:Type} {v1:T1} {v2:T2},
    @exp.eVal cfg T1 v1 = @exp.eVal cfg T2 v2 ->
    T1 = T2
:= begin
  introv, intros he, cases he, reflexivity,
end

lemma exptype.val.type_eq {cfg:saigaconfig}:
  ∀ {T1 T2:Type} {v1:T1},
    expType (@exp.eVal cfg T1 v1) T2 ->
    T1 = T2
:= begin
  introv, intros ht, cases ht, reflexivity,
end


lemma bigstep.val.same.v : ∀ {cfg:_} {c1 c2:ctxN cfg} {T1 T2:Type} {v1:T1} {v2:T2},
  (c1 ⊨ {{v1}} »» c2 ⊨ v2) ->
  v2 == v1
:= begin
  introv, intros hs, cases hs, reflexivity,
end

lemma bigstep.val.same.ctx : ∀ {cfg:_} {c1 c2:ctxN cfg} {T1 T2:Type} {v1:T1} {v2:T2},
  (c1 ⊨ {{v1}} »» c2 ⊨ v2) ->
  c2 = c1
:= begin
  introv, intros hs, cases hs, reflexivity,
end



lemma bigstep.eMk.contra {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg}
        {l:_} {T:Type} {v:T},
  (c1 ⊨ exp.eMk l »» c2 ⊨ v) ->
  false
:= begin
  introv, intros hs, cases hs,
end