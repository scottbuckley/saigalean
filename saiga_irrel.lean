
import .saiga_lemmas
import .saiga_tactics
import .saiga_metatheoretic
import .saiga_irrelprep
open saiga
open tactic

variable cfg: saigaconfig




axiom selfcall_nochange_axiom {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg}
          {n:node} {a:cfg.A} {p:cfg.ρ a} {T:Type} {v:T},
  (c1 ⊨ c1.c n a p »» c2 ⊨ v) ->
  c2.c n a p = c1.c n a p





lemma bigstep.cong.crux.crux : ∀ {cfg:_} {c4 c4':ctxN cfg} {n a p} {v:_}
              {e:exp cfg} {T:Type} {v':T},
  (contained n c4.N) ->  
  (c4 ⊨ c4.c n a p »» c4 ⊨ v) ->
    legal c4 e ->
    (c4 ⊨ e »» c4' ⊨ v') ->
    (extendCN _ n a p v c4 ⊨ e »» extendCN _ n a p v c4' ⊨ v')
    ∧ (c4' ⊨ c4'.c n a p »» c4' ⊨ v)
:= begin
  introv, intros hc hsc,
  intros hl hs,
  induction hs,
  { -- refl
    apply and2 _ hsc,
    apply bigstep.refl,
  }, { -- condT
    have hh1 := hs_ih_cs hc  hsc  (lg.condC hl), splith `hh1,
    have hh2 := hs_ih_ts _ hh1b (lg.condT hl hs_cs), splith `hh2,
    apply and2 _ hh2b,
    apply bigstep.condT hh1a hh2a,
    apply bigstep.contained hc hs_cs,
  }, { -- condF
    have hh1 := hs_ih_cs hc  hsc  (lg.condC hl), splith `hh1,
    have hh2 := hs_ih_fs _ hh1b (lg.condF hl hs_cs), splith `hh2,
    apply and2 _ hh2b,
    apply bigstep.condF hh1a hh2a,
    apply bigstep.contained hc hs_cs,
  }, { -- app
    have hh1 := hs_ih_fs hc hsc (lg.appF hl), splith `hh1,
    have hh2 := hs_ih_ps _ hh1b (lg.appP hl hs_fs), splith `hh2,
    apply and2 _ hh2b,
    apply bigstep.app hh1a hh2a,
    apply bigstep.contained hc hs_fs,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns hc  hsc (lg.attrN hl), splith `hh1,
    have hh2 := hs_ih_ps _ hh1b (lg.attrP hl hs_ns), splith `hh2,
    have hh3 := hs_ih_cs _ hh2b _, splith `hh3, rotate 1, {
      apply bigstep.contained _ hs_ps,
      apply bigstep.contained hc hs_ns,
    }, {
      apply legal_context_axiom,
    }, {
      apply bigstep.contained hc hs_ns,
    },
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    apply and2 _ hh3b,
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      apply bigstep.attrValue _ hh1a hh2a,
      rewrite napmatch.ff_update_unused_CN hnp,
      apply hh3a,
      rewrite napmatch.ff_update_unused_CN hnp, apply hs_hv,
    }, {
      napmatch_rewrites `hnp,
      apply bigstep.attrValue _ hh1a hh2a,
      {
        rewrite extendCN.match,
        generalize hg: (hs_ctx3.c n a p) = ge,
        rewrite hg at *,
        cases hs_hv, cases hs_cs,
        have hh := bigstep.val.same.v hh2b,
        cases hh, apply bigstep.refl,
      }, {
        rewrite extendCN.match,
        constructor,
      },
    },
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],
    have hh1 := hs_ih_ns hc hsc (lg.attrN hl), splith `hh1,
    have hh2 := hs_ih_ps _ hh1b (lg.attrP hl hs_ns), splith `hh2,
    have hh3 := hs_ih_cs _ hh2b _, splith `hh3, rotate 1, {
      apply bigstep.contained _ hs_ps,
      apply bigstep.contained hc hs_ns,
    }, {
      apply legal_context_axiom,
    }, {
      apply bigstep.contained hc hs_ns,
    },
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    split, { -- left
      destruct (napmatch _ n n' a a' p p'); intro hnp, {
        -- different
        cases hs_ctx1 with c1c c1N,
        -- unfold extendCN,
        rewrite napmatch.ff_extend_flip_CN hnp,
        apply bigstep.attrCached _ hh1a hh2a, {
          rewrite napmatch.ff_update_unused_CN hnp,
          apply hh3a,
        }, {
          rewrite napmatch.ff_update_unused_CN hnp,
          apply hs_nv,
        },
      }, {
        -- same
        napmatch_rewrites `hnp,
        have hhsame := bigstep.determinism hh2b hs_cs,
        doublecases `hhsame,
        apply bigstep.attrValue _ hh1a hh2a, {
          rewrite extendCN.match,
          rewrite extendCN.redundant,
          apply bigstep.refl,
        }, {
          rewrite extendCN.match, constructor,
        },
      },
    }, { -- right
      destruct (napmatch _ n' n a' a p' p); intro hnp, {
        -- different
        rewrite (napmatch.ff_update_unused_CN hnp),
        apply bigstep.withcache.unused.samectx,
        apply legal_context_axiom, apply hh3b, reflexivity,
        have hh4 := bigstep.cacheperm _ hs_cs, rotate 1, {
          apply legal_context_axiom,
        },
        have hh5 := selfcall_nochange_axiom hs_cs,
        rewrite hh5, assumption,
      }, {
        -- same
        napmatch_rewrites `hnp,
        rewrite extendCN.match,
        have hh := bigstep.determinism hh2b hs_cs,
        doublecases `hh,
        apply bigstep.refl,
      }
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    have hh1 := hs_ih_ns hc  hsc (lg.attrN hl), splith `hh1,
    have hh2 := hs_ih_ps _ hh1b (lg.attrP hl hs_ns), splith `hh2, rotate 1, {
      apply bigstep.contained hc hs_ns,
    },
    clear hs_ih_ns hs_ih_ps,
    have hnap : (napmatch cfg n n' a a' p p' = ff), {
      destruct (napmatch cfg n n' a a' p p'); intros hnap, assumption,
      napmatch_rewrites `hnap,
      rewrite hs_ismk at hh2b,
      have hh4 := bigstep.eMk.contra hh2b,
      cases hh4,
    }, rewrite napmatch.symm at hnap,
    have hh3 : (n ≠ hs_n_f), {
      have hh4 := bigstep.contained hc hs_ns,
      have hh5 := bigstep.contained hh4 hs_ps,
      have hh6 := fresh_for.contained hh5,
      rewrite <- hs_isfresh at hh6,
      apply hh6,
    },
    apply and2 _, {
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c, rewrite writeAL.unused hh3,
      apply bigstep.HOwrite.unused legal_context_axiom hh2b hs_ismk hs_isfresh,
    },
    rewrite napmatch.symm at hnap,
    rewrite extendCN.HOwrite.flip2 hnap hh3,
    apply bigstep.attrHO _ _ _ hh1a hh2a, rotate 1, {
      rewrite fresh_for.extendCN, assumption,
    }, {
      rewrite napmatch.ff_update_unused_CN hnap,
      assumption,
    },
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    cases hl,
    have hh1 := hs_ih hc hsc _, splith `hh1,
    clear hs_ih, rotate 1, {
      apply legal_context_axiom,
    },
    destruct (napmatch _ n n' a a' p p'); intro hnp, {
      -- different
      split, {
        rewrite napmatch.ff_extend_flip_CN hnp,
        apply bigstep.cache, apply hh1a,
      }, {
        generalize he : (hs_ctx1.c n' a' p') = eC,
        rewrite he at *,
        have hv := (value_or_notvalue_or_ho eC),
        cases hv, {
          -- value
          cases hv,
          have hh := bigstep.val.same.ctx hs_c, cases hh,
          have hh := bigstep.val.same.v hs_c, cases hh,
          clear_refls,
          have hh : (extendCN cfg n' a' p' v'' hs_ctx1 = hs_ctx1), {
            apply extendCN.inj,
            funext n'' a'' p'',
            destruct (napmatch _ n' n'' a' a'' p' p''); intro hnp, {
              unfold extendCN, simp,
              rewrite (napmatch.ff_update_unused hnp),
            }, {
              napmatch_rewrites `hnp,
              rewrite extendCN.match, rewrite he,
            },
          }, {
            rewrite hh, apply hh1b,
          },
        }, cases hv, {
          -- not value
          rewrite napmatch.symm at hnp,
          rewrite (napmatch.ff_update_unused_CN hnp),
          
          apply bigstep.withcache.unused.samectx,
          apply legal_context_axiom, apply hh1b,
          reflexivity, rewrite <- he at hs_c,
          have hh := selfcall_nochange_axiom hs_c,
          have hh2 := bigstep.cacheperm legal_context_axiom hs_c,
          rewrite hh, apply hh2,
        }, {
          -- ho
          cases hv with al hv,
          rewrite hv at hs_c,
          have hh := bigstep.eMk.contra hs_c,
          cases hh,
        },
      },
    }, {
      -- same
      napmatch_rewrites `hnp,
      have hh := bigstep.determinism hsc hs_c,
      doublecases `hh,
      split, {
        apply bigstep.cache hh1a,
      }, {
        rewrite extendCN.match, apply bigstep.refl,
      },
    }, 
  },
end -- bigstep.cong.crux.crux


lemma bigstep.cong.crux.crux.simple : ∀ {cfg:_} {c4 c4':ctxN cfg} {e:exp cfg} {n a p} {v:_} {T:Type} {v':T},
  contained n c4.N ->
  (c4 ⊨ c4.c n a p »» c4 ⊨ v) ->
    legal c4 e ->
    (c4 ⊨ e »» c4' ⊨ v') ->
    (extendCN _ n a p v c4 ⊨ e »» extendCN _ n a p v c4' ⊨ v')
:= begin
  introv, intros hc hsc hl hs,
  apply andL (bigstep.cong.crux.crux hc hsc hl hs),
end







lemma bigstep.cong.right.crux.crux.weaker : ∀ {cfg:_} {c4:ctxN cfg}
        {n:node} {a:cfg.A} {p:cfg.ρ a} {v:cfg.τ a},
  contained n c4.N ->
  (c4 ⊨ c4.c n a p »» c4 ⊨ v) ->
  (
    ∀ (c4':ctxN cfg) {e:exp cfg} {T:Type} {v':T},
      legal (extendCN _ n a p v c4) e ->
      (extendCN _ n a p v c4 ⊨ e »» extendCN _ n a p v c4' ⊨ v') ->
      exists c4'',
      (c4 ⊨ e »» c4'' ⊨ v')
      ∧ (extendCN _ n a p v c4' = extendCN _ n a p v c4'')
      ∧ (c4'' ⊨ c4''.c n a p »» c4'' ⊨ v)
  )
:= begin
  introv, intros hc hsc, introv, intros hl hs,
  generalize hz1' : (extendCN cfg n a p v c4)  = ec4,  have hz1 := eq.symm hz1',
  generalize hz2' : (extendCN cfg n a p v c4') = ec4', have hz2 := eq.symm hz2',
  clear hz1' hz2', rewrite <- hz1 at hs, rewrite <- hz2 at hs,
  induction hs generalizing n a p v c4 c4',
  { -- refl
    existsi c4,
    apply and3 _ hz1 hsc,
    apply bigstep.refl,
  }, { -- condT
    rewrite hz1 at hs_cs,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.condC hl) hs_cs,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_cs hs_ctx2 hc hsc (lg.condC hl) hz1 hz3, clear hs_ih_cs,
    cases hh1 with c4''1 hh1, splith3 `hh1,
    have hh2 := hs_ih_ts _ (bigstep.contained hc hh1a) hh1c _ hh1b hz2, clear hs_ih_ts,
    rotate 1, {
      rewrite <- hh1b, apply lg.condT hl hs_cs,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2,
    existsi _, apply and3 _ hh2b hh2c,
    apply bigstep.condT hh1a hh2a,
  }, { -- condF
    rewrite hz1 at hs_cs,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.condC hl) hs_cs,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_cs hs_ctx2 hc hsc (lg.condC hl) hz1 hz3, clear hs_ih_cs,
    cases hh1 with c4''1 hh1, splith3 `hh1,
    have hh2 := hs_ih_fs _ (bigstep.contained hc hh1a) hh1c _ hh1b hz2, clear hs_ih_fs,
    rotate 1, {
      rewrite <- hh1b, apply lg.condF hl hs_cs,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2,
    existsi _, apply and3 _ hh2b hh2c,
    apply bigstep.condF hh1a hh2a,
  }, { -- app
    rewrite hz1 at hs_fs,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.appF hl) hs_fs,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_fs hs_ctx2 hc hsc (lg.appF hl) hz1 hz3, clear hs_ih_fs,
    cases hh1 with c4''1 hh1, splith3 `hh1,
    have hh2 := hs_ih_ps _ (bigstep.contained hc hh1a) hh1c _ hh1b hz2, clear hs_ih_ps,
    rotate 1, {
      rewrite <- hh1b, apply lg.appP hl hs_fs,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2,
    existsi _, apply and3 _ hh2b hh2c,
    apply bigstep.app hh1a hh2a,
  }, { -- attrValue
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite hz1 at hs_ns,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.attrN hl) hs_ns,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_ns _ hc hsc (lg.attrN hl) hz1 hz3,
    cases hh1 with c4''1 hh1, splith3 `hh1, clear hs_ih_ns,

    have hh2 := hs_ih_ps _ _ hh1c _ hh1b _, rotate 2, {
      apply bigstep.contained hc hh1a,
    }, {
      rewrite <- hh1b, apply lg.attrP hl hs_ns,
    }, {
      rewrite hz3 at hs_ps,
      apply bigstep.extendCN.remains _ _ hs_ps,
      rewrite extendCN.N, rewrite hh1b,
      rewrite extendCN.N, apply bigstep.contained hc hh1a,
      apply lg.attrP hl,rewrite <- hz3, assumption,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2, clear hs_ih_ps,
    
    have hh3 := hs_ih_cs _ _ hh2c _ hh2b _, rotate 2, {
      apply bigstep.contained _ hh2a,
      apply bigstep.contained hc hh1a,
    }, {
      rewrite <- hh2b, apply legal_context_axiom,
    }, {
      assumption,
    }, clear hs_ih_cs,
    cases hh3 with c4''3 hh3, splith3 `hh3,
    
    destruct (napmatch _ n n' a a' p p'); intro hnap, {
      -- different
      existsi _, apply and3 _ hh3b hh3c, {
        apply bigstep.attrValue _ hh1a hh2a, {
          rewrite hh2b at hh3a,
          rewrite napmatch.ff_update_unused_CN hnap at hh3a,
          apply hh3a,
        }, {
          rewrite hh2b at hs_hv,
          rewrite napmatch.ff_update_unused_CN hnap at hs_hv,
          assumption,
        },
      },
    }, {
      -- same
      napmatch_rewrites `hnap,
      rewrite hh2b at hh3a,
      rewrite extendCN.match at hh3a,
      cases hh3a,

      have hv := @value_or_notvalue_or_ho _ (c4''2.c n a p), 
      cases hv, {
        -- value
        existsi _,
        apply and3 _ hh3b hh3c,
        apply bigstep.attrValue hv hh1a hh2a hh2c,
      }, cases hv, {
        -- not value
        existsi _, apply and3, {
          apply bigstep.attrCached hv hh1a hh2a hh3c,
        }, {
          rewrite extendCN.redundant, assumption,
        }, {
          rewrite extendCN.match,
          apply bigstep.refl,
        },
      }, { -- ho
        cases hv with al hv,
        rewrite hv at hh3c,
        have hh := bigstep.eMk.contra hh3c; trivial,
      }
    },
  }, { -- attrCache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v''],
    rewrite hz1 at hs_ns,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.attrN hl) hs_ns,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_ns _ hc hsc (lg.attrN hl) hz1 hz3,
    cases hh1 with c4''1 hh1, splith3 `hh1, clear hs_ih_ns,
    have hh1d := bigstep.contained hc hh1a,
    have hh2 := hs_ih_ps _ (bigstep.contained hc hh1a) hh1c _ hh1b _, rotate 2, {
      apply lg.attrP hl, rewrite <- hh1b, assumption,
    }, {
      rewrite hz3 at hs_ps,
      apply bigstep.extendCN.remains _ _ hs_ps,
      rewrite extendCN.N, apply bigstep.contained _ hs_ns,
      rewrite extendCN.N, apply hc,
      apply lg.attrP hl, 
      rewrite <- hz3, assumption,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2, clear hs_ih_ps,
    have hh2d := bigstep.contained hh1d hh2a,
    have hh3 := hs_ih_cs _ hh2d hh2c _ hh2b _, rotate 2, {
      rewrite <- hh2b, apply legal_context_axiom,
    }, {
      rewrite hh2b at hs_cs,
      apply bigstep.extendCN.remains _ legal_context_axiom hs_cs,
      rewrite extendCN.N,
      apply bigstep.contained _ hh2a,
      apply bigstep.contained hc hh1a,
    },
    cases hh3 with c4''3 hh3, splith3 `hh3, clear hs_ih_cs,
    have hh3d := bigstep.contained hh2d hh3a,
    have hnap : (napmatch _ n n' a a' p p') = ff, {
      destruct (napmatch _ n n' a a' p p'); intro hnp2, {
        apply hnp2,
      }, {
        napmatch_rewrites `hnp2,
        rewrite hh2b at hs_nv,
        rewrite extendCN.match at hs_nv, cases hs_nv,
      },
    },

    existsi _, apply and3, {
      apply bigstep.attrCached _ hh1a hh2a, {
          rewrite hh2b at hh3a,
          rewrite napmatch.ff_update_unused_CN hnap at hh3a,
          apply hh3a,
      }, {
        rewrite hh2b at hs_nv,
        rewrite napmatch.ff_update_unused_CN hnap at hs_nv,
        apply hs_nv,
      },
    }, {
      rewrite hh3b,
      rewrite napmatch.symm at hnap,
      apply napmatch.ff_extend_flip_CN hnap,
    }, {
      rewrite napmatch.symm at hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite napmatch.symm at hnap,
      rewrite hh2b at hs_nv,
      rewrite napmatch.ff_update_unused_CN hnap at hs_nv,

      have hh4 : (c4''2 ⊨ c4''2.c n' a' p' »» c4''3 ⊨ v''), {
        rewrite hh2b at hh3a,
        rewrite napmatch.ff_update_unused_CN hnap at hh3a,
        apply hh3a,
      },
      have hh5 := selfcall_nochange_axiom hh4,
      rewrite <- hh5 at hh4,
      have hh6 := bigstep.cacheperm _ hh4, rotate 1, {
        rewrite hh5, apply legal_context_axiom,
      },
      apply bigstep.withcache.unused.samectx legal_context_axiom hh3c rfl hh6,
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    rewrite hz1 at hs_ns,
    have hz3 : (hs_ctx2 = extendCN cfg n a p v hs_ctx2), {
      apply bigstep.extendCN.remains _ (lg.attrN hl) hs_ns,
      rewrite extendCN.N, apply hc,
    },
    have hh1 := hs_ih_ns _ hc hsc (lg.attrN hl) hz1 hz3,
    cases hh1 with c4''1 hh1, splith3 `hh1, clear hs_ih_ns,
    have hh1d := bigstep.contained hc hh1a,
    have hh2 := hs_ih_ps _ (bigstep.contained hc hh1a) hh1c _ hh1b _, rotate 2, {
      apply lg.attrP hl, rewrite <- hh1b, assumption,
    }, {
      rewrite hz3 at hs_ps,
      apply bigstep.extendCN.remains _ _ hs_ps,
      rewrite extendCN.N, apply bigstep.contained _ hs_ns,
      rewrite extendCN.N, apply hc,
      apply lg.attrP hl, rewrite <- hz3, assumption,
    },
    cases hh2 with c4''2 hh2, splith3 `hh2, clear hs_ih_ps,
    have hh2d := bigstep.contained hh1d hh2a,

    have hnap : (napmatch _ n n' a a' p p') = ff, {
      destruct (napmatch _ n n' a a' p p'); intro hnp2, {
        apply hnp2,
      }, {
        napmatch_rewrites `hnp2,
        rewrite hh2b at hs_ismk,
        rewrite extendCN.match at hs_ismk,
        cases hs_ismk,
      },
    },
    have hfresh2 : (hs_n_f = fresh_for c4''2), {
      rewrite hh2b at hs_isfresh,
      rewrite hs_isfresh, apply fresh_for.N,
      rewrite extendCN.N,
    },

    have hne : (n ≠ hs_n_f), {
      have hh := fresh_for.contained hh2d,
      rewrite <- hfresh2 at hh, assumption,
    },

    existsi _, apply and3, {
      apply bigstep.attrHO hs_tp_a _ hfresh2 hh1a hh2a, rotate 1, {
        rewrite hh2b at hs_ismk,
        rewrite napmatch.ff_update_unused_CN hnap at hs_ismk,
        assumption,
      },
    }, {
      rewrite hh2b,
      rewrite extendCN.HOwrite.flip2 hnap hne,
    }, {
      rewrite napmatch.symm at hnap,
      rewrite napmatch.ff_update_unused_CN hnap,
      rewrite addN.c, rewrite writeAL.unused hne,

      apply bigstep.withHOwrite.unused.samectx legal_context_axiom hh2c rfl _ hfresh2, rotate 1, {
        rewrite hh2b at hs_ismk,
        rewrite napmatch.symm at hnap,
        rewrite napmatch.ff_update_unused_CN hnap at hs_ismk,
        assumption,
      },
    },
  }, { -- cache
    renames [`hs_n, `hs_a, `hs_p, `hs_v] [`n',`a',`p',`v'],

    have hl2 : (legal (extendCN cfg n a p v c4) hs_eC), {
      cases hl, apply legal_context_axiom,
    },

    rewrite hz1 at hs_c,
    have hz2 := bigstep.extendCN.remains _ hl2 hs_c, rotate 1, {
      rewrite extendCN.N, assumption,
    },

    have hh1 := hs_ih _ hc hsc hl2 hz1 hz2, clear hs_ih,
    cases hh1 with c4'' hh1, splith3 `hh1,

    existsi _,
    apply and3, {
      apply bigstep.cache hh1a,
    }, {
      destruct (napmatch _ n n' a a' p p'); intro hnap, {
        rewrite hh1b, rewrite napmatch.ff_extend_flip_CN hnap,
      }, {
        napmatch_rewrites `hnap, rewrite hh1b,
        rewrite extendCN.redundant,
        rewrite extendCN.redundant,
        cases hl,
        rewrite napmatch.id at hh1a, simp at hh1a,
        have hh := bigstep.val.same.v hh1a, cases hh,
        reflexivity,
      },
    }, {
      destruct (napmatch _ n n' a a' p p'); intro hnap, {
        rewrite napmatch.symm at hnap,
        rewrite napmatch.ff_update_unused_CN hnap,
        rewrite napmatch.symm at hnap,
        rewrite <- hz1 at hs_c, rewrite <- hz1 at hl2,
        have hh2 := bigstep.cacheperm _ hh1a, rotate 1, {
          cases hl,
          rewrite hnap, simp, apply legal_context_axiom,
        },

        have hec : (hs_eC = c4.c n' a' p'), {
          cases hl,
          rewrite hnap,
        },
        rewrite hec at hh1a,
        have hh4 := selfcall_nochange_axiom hh1a,
        apply bigstep.withcache.unused.samectx legal_context_axiom hh1c rfl,
        rewrite hec at hh2, rewrite <- hh4 at hh2,
        apply hh2,
      }, {
        napmatch_rewrites `hnap,
        rewrite extendCN.match,
        have hec : (hs_eC = {{v}}), {
          cases hl,
          rewrite napmatch.id,
        },
        rewrite hec at hs_c,
        have hh := bigstep.val.same.v hs_c,
        cases hh, apply bigstep.refl,
      },
    },
  },
end -- bigstep.cong.right.crux.crux.weaker






-- congruence
@[irreducible]
def cong {cfg:_} (c1 c2:ctxN cfg) : Prop :=
  forall {c1':ctxN cfg} {n a p} {T:Type} {v : T},
   (c1 ⊨ c1.c n a p »» c1' ⊨ v) ->
  exists c2',
   (c2 ⊨ c2.c n a p »» c2' ⊨ v)
notation c1 ` ≅ ` c2 := cong c1 c2
#check λ c1, λ c2, c1 ≅ c2



-- CONGRUENCE
lemma cong.trans {cfg:_} (c2:ctxN cfg) {c1 c3:ctxN cfg} : 
  (c1 ≅ c2) ->
  (c2 ≅ c3) ->
  (c1 ≅ c3)
:= begin
  intros cong1 cong2,
  unfold cong, introv, intros hs,
  have hh1 := cong1 hs,
  cases hh1 with c2' hh1,
  have hh2 := cong2 hh1,
  apply hh2,
end

lemma cong.refl {cfg:_} (c1:ctxN cfg) :
  (c1 ≅ c1)
:= begin
  unfold cong, introv, intros hs1,
  existsi c1', apply hs1,
end





theorem bigstep.cong.crux : forall {cfg:_} {c3:ctxN cfg} {n a p} {v:_},
  (contained n c3.N) ->
  (c3 ⊨ c3.c n a p »» c3 ⊨ v) ->
  (c3 ≅ extendCN _ n a p v c3)
:= begin
  introv, intros hc hsc,
  unfold cong, intros c1' n' a' p' T v' hs,

  destruct (napmatch cfg n n' a a' p p'); intros hnap, {
    existsi _,
    apply bigstep.cong.crux.crux.simple hc hsc _,
    rewrite napmatch.ff_update_unused_CN hnap,
    apply hs,
    rewrite napmatch.ff_update_unused_CN hnap,
    apply legal_context_axiom,
  }, {
    napmatch_rewrites `hnap,
    have hh1 := bigstep.determinism hsc hs,
    doublecases `hh1,
    existsi _,
    rewrite extendCN.match,
    apply bigstep.refl,
  }
end




theorem bigstep.cong.right.crux : forall {cfg:_} {c3:ctxN cfg} {n a p} {v:_},
  (contained n c3.N) ->
  (c3 ⊨ c3.c n a p »» c3 ⊨ v) ->
  (extendCN _ n a p v c3 ≅ c3)
:= begin
  introv, intros hc hsc,
  unfold cong, intros c1' n' a' p' T v' hs,
  destruct (napmatch cfg n n' a a' p p'); intros hnap, {
    have hh1 := bigstep.extendCN.remains _ legal_context_axiom hs, rotate 1, {
      rewrite extendCN.N, assumption,
    },
    rewrite napmatch.ff_update_unused_CN hnap at hs, rewrite hh1 at hs,
    have hh := bigstep.cong.right.crux.crux.weaker hc hsc _ _ hs, {
      cases hh with c4'' hh1,
      splith3 `hh1,
      existsi _,
      apply hh1a,
    }, {
      rewrite <- napmatch.ff_update_unused_CN hnap,
      apply legal_context_axiom,
    },
  }, {
    napmatch_rewrites `hnap,
    rewrite extendCN.match at hs,
    cases hs,
    existsi _,
    apply hsc,
  }
end





theorem bigstep.cong.HOcrux {cfg:saigaconfig} : ∀ {c3:ctxN cfg}
        {n a p} {fl:_} {tp_a:cfg.τ a = node} {n_f:node},
  (c3.c n a p = exp.eMk fl) ->
  n_f = fresh_for c3 ->
  (c3 ≅
    extendCN cfg n a p (eq.mpr tp_a n_f)
      (addN n_f
        (writeAL c3 n_f (fl n_f))))
:= begin
  introv, intros hsmk hf,
  unfold cong, intros c3' n' a' p' T v' hs,

  destruct (napmatch cfg n n' a a' p p'); intro hnap, rotate 1, {
    napmatch_rewrites `hnap,
    rewrite hsmk at hs,
    cases hs,
  }, {
    rewrite napmatch.ff_update_unused_CN hnap,
    rewrite addN.c,
    -- THIS IS WHERE WE WAVE OUR HANDS
    -- let's talk about (n')s relation to c3
    -- if we only care about (n')s that exists in c3
    -- (as per hs), then n'≠n_f, which gives us the following:
    rewrite writeAL.unused sorry,
    -- so if we look at all the things that (c3.c n' a' p')
    -- might call: if it never asks for n a p, then it will
    -- never create this HO node and everything is the same.
    -- if it DOES call n a p, the stuff will be ready created,
    -- vs the case in hs where it will simply be created
    -- in one go, and then it will be accessed.
    -- I think i could probably actually prove this,
    -- but it would likely take some time.
    
    admit,
  }
end



theorem bigstep.cong.right.HOcrux {cfg:saigaconfig} : ∀ {c3:ctxN cfg}
        {n a p} {fl:_} {tp_a:cfg.τ a = node} {n_f:node},
  (c3.c n a p = exp.eMk fl) ->
  n_f = fresh_for c3 ->
  (extendCN cfg n a p (eq.mpr tp_a n_f)
      (addN n_f
        (writeAL c3 n_f (fl n_f))) ≅ c3)
:= begin
  introv, intros hsmk hf,
  unfold cong, intros c3' n' a' p' T v' hs,
  destruct (napmatch cfg n n' a a' p p'); intro hnap, rotate 1, {
    napmatch_rewrites `hnap,
    -- here we get a contradiction in hs,
    admit,
  }, {
    -- this is just the inversion of the situation
    -- in HOcrux.
    admit,
  }
end








theorem bigstep.congBoth : forall {cfg:_} {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
  legal c1 e ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  (c1 ≅ c2) ∧ (c2 ≅ c1)
:= begin
  introv, intros hl hs,
  induction hs,
  { -- refl
    apply and2; apply cong.refl,
  }, { -- condT
    have hh1 := hs_ih_cs (lg.condC hl),
    have hh2 := hs_ih_ts (lg.condT hl hs_cs),
    splith `hh1, splith `hh2,
    apply and2; apply cong.trans; assumption,
  }, { -- condF
    have hh1 := hs_ih_cs (lg.condC hl),
    have hh2 := hs_ih_fs (lg.condF hl hs_cs),
    splith `hh1, splith `hh2,
    apply and2; apply cong.trans; assumption,
  }, { -- app
    have hh1 := hs_ih_fs (lg.appF hl),
    have hh2 := hs_ih_ps (lg.appP hl hs_fs),
    splith `hh1, splith `hh2,
    apply and2; apply cong.trans; assumption,
  }, { -- attrValue
    have hh1 := hs_ih_ns (lg.attrN hl),
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns),
    have hh3 := hs_ih_cs legal_context_axiom,
    splith `hh1, splith `hh2, splith `hh3,
    apply and2, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
    }, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
    },
  }, { -- attrCache
    have hh1 := hs_ih_ns (lg.attrN hl),
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns),
    have hh3 := hs_ih_cs _, rotate 1, apply legal_context_axiom,
    splith `hh1, splith `hh2, splith `hh3,
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    have hh := bigstep.cacheperm legal_context_axiom hs_cs,
    rewrite <- selfcall_nochange_axiom hs_cs at hh,
    apply and2, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
      apply cong.trans hs_ctx4; try {assumption},
      apply bigstep.cong.crux, {
        apply bigstep.contained _ hs_cs,
        apply bigstep.contained _ hs_ps,
        apply lg.attrCont hl hs_ns,
      },
      apply hh,
    }, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
      apply cong.trans hs_ctx4; try {assumption},
      apply bigstep.cong.right.crux, rotate 1,
      apply hh,
      apply bigstep.contained _ hs_cs,
      apply bigstep.contained _ hs_ps,
      apply lg.attrCont hl hs_ns,
    },
  }, { -- attrHO
    renames [`hs_n, `hs_a, `hs_p] [`n',`a',`p'],
    have hh1 := hs_ih_ns (lg.attrN hl),
    have hh2 := hs_ih_ps (lg.attrP hl hs_ns),
    clear hs_ih_ns hs_ih_ps,
    splith `hh1, splith `hh2,
    apply and2, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
      apply bigstep.cong.HOcrux,
      assumption, assumption,
    }, {
      apply cong.trans hs_ctx2; try {assumption},
      apply cong.trans hs_ctx3; try {assumption},
      apply bigstep.cong.right.HOcrux,
      assumption, assumption,
    },
  }, { -- cache
    cases hl, 
    have hh1 := hs_ih legal_context_axiom,
    splith `hh1, clear hs_ih,
    have hh := bigstep.cacheperm legal_context_axiom hs_c, 
    have hh2 := selfcall_nochange_axiom hs_c,
    rewrite <- hh2 at hh,
    apply and2, {
      apply cong.trans hs_ctx2; try {assumption},
      apply bigstep.cong.crux,
      apply bigstep.contained hl_a_1,
      assumption, assumption,
    }, {
      apply cong.trans hs_ctx2; try{assumption},
      apply bigstep.cong.right.crux,
      apply bigstep.contained hl_a_1,
      assumption, assumption,
    },
  },
end




theorem bigstep.cong.left : forall {cfg:_} {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
  legal c1 e ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  (c1 ≅ c2)
:= begin
  introv, intros hl hs,
  have hh := bigstep.congBoth hl hs,
  cases hh, assumption,
end

theorem bigstep.cong.right : forall {cfg:_} {c1 c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
  legal c1 e ->
  (c1 ⊨ e »» c2 ⊨ v) ->
  (c2 ≅ c1)
:= begin
  introv, intros hl hs,
  have hh := bigstep.congBoth hl hs,
  cases hh, assumption,
end











lemma cong.app {cfg:_}: ∀ {c1 c1' c2:ctxN cfg} {e:exp cfg} {T:Type} {v:T},
  (c1 ≅ c2) ->
  (c1 ⊨ e »» c1' ⊨ v) ->
  exists c2',
    (c2 ⊨ e »» c2' ⊨ v)
:= begin
  introv, intros hc hs,
  induction hs generalizing c2,
  { -- refl
    existsi _, constructor,
  }, { -- condT
    have hh1 := hs_ih_cs hc,
    cases hh1 with c2' hh1,
    have hh2 := bigstep.cong.right sorry hs_cs,
    have hh3 := bigstep.cong.left sorry hh1,
    have hh4 := cong.trans _ hh2 hc,
    have hh5 := cong.trans _ hh4 hh3,
    have hh6 := hs_ih_ts hh5,
    cases hh6 with c2'' hh6,
    existsi _,
    apply bigstep.condT hh1 hh6,
  }, { -- condF
    have hh1 := hs_ih_cs hc,
    cases hh1 with c2' hh1,
    have hh2 := bigstep.cong.right sorry hs_cs,
    have hh3 := bigstep.cong.left sorry hh1,
    have hh4 := cong.trans _ hh2 hc,
    have hh5 := cong.trans _ hh4 hh3,
    have hh6 := hs_ih_fs hh5,
    cases hh6 with c2'' hh6,
    existsi _,
    apply bigstep.condF hh1 hh6,
  }, { -- app
    have hh1 := hs_ih_fs hc,
    cases hh1 with c2' hh1,
    have hh2 := bigstep.cong.right sorry hs_fs,
    have hh3 := bigstep.cong.left sorry hh1,
    have hh4 := cong.trans _ hh2 hc,
    have hh5 := cong.trans _ hh4 hh3,
    have hh6 := hs_ih_ps hh5,
    cases hh6 with c2'' hh6,
    existsi _,
    apply bigstep.app hh1 hh6,
  }, { 
    have hh1 := hs_ih_ns hc,
    cases hh1 with c2' hh1,
    have hh2 := bigstep.cong.right sorry hs_ns,
    have hh3 := bigstep.cong.left sorry hh1,
    have hh4 := cong.trans _ hh2 hc,
    have hh5 := cong.trans _ hh4 hh3,
    have hh6 := hs_ih_ps hh5,
    cases hh6 with c2''' hh6,
    clear hs_ih_ns hs_ih_ps hs_ih_cs,
    have hh7 := bigstep.cong.right sorry hs_ps,
    have hh8 := bigstep.cong.left sorry hh6,
    have hh9 := cong.trans _ hh7 hh5,
    have h10 := cong.trans _ hh9 hh8,
    unfold cong at h10,
    have h11 := h10 hs_cs, clear h10,
    cases h11 with c2'''' h11,
    destruct (value_or_notvalue_or_ho (c2'''.c hs_n hs_a hs_p)); intros hv, {
      existsi _, apply bigstep.attrValue hv hh1 hh6 h11,
    }, cases hv, {
      -- here we assume correct typing
      have hhT : hs_T = cfg.τ hs_a := sorry, cases hhT,
      existsi _, apply bigstep.attrCached hv hh1 hh6 h11,
    }, {
      cases hv with fl hv,
      rewrite hv at h11, cases h11,
    },
  }, {
    -- etc.
  }
end
