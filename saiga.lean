import .nat_lemmas
namespace saiga

/-
    ###    ##     ## ####  #######  ##     ##  ######
   ## ##    ##   ##   ##  ##     ## ###   ### ##    ##
  ##   ##    ## ##    ##  ##     ## #### #### ##
 ##     ##    ###     ##  ##     ## ## ### ##  ######
 #########   ## ##    ##  ##     ## ##     ##       ##
 ##     ##  ##   ##   ##  ##     ## ##     ## ##    ##
 ##     ## ##     ## ####  #######  ##     ##  ######
-/

-- we need this for some type proofs
axiom fun_type_eq: ∀ {T1 T2 T3:Type},
  (T1 -> T2) = (T1 -> T3) ->
  T2 = T3



/-
  ######   #######  ##    ## ######## ####  ######
 ##    ## ##     ## ###   ## ##        ##  ##    ##
 ##       ##     ## ####  ## ##        ##  ##
 ##       ##     ## ## ## ## ######    ##  ##   ####
 ##       ##     ## ##  #### ##        ##  ##    ##
 ##    ## ##     ## ##   ### ##        ##  ##    ##
  ######   #######  ##    ## ##       ####  ######
-/

-- the configuration needed everything
-- mostly this is a list of attributes and
-- their types
structure saigaconfig :=
  (A:  Type)
  (τ ρ:  A -> Type)
  [Adec: decidable_eq A]
  [τinh: ∀ (a:A), inhabited (τ a)]
  [ρdec: ∀ (a:A), decidable_eq (ρ a)]

-- we always use cfg as a variable, unless
-- working with concrete attributes
variable cfg: saigaconfig


/-
 ##    ##  #######  ########  ########
 ###   ## ##     ## ##     ## ##
 ####  ## ##     ## ##     ## ##
 ## ## ## ##     ## ##     ## ######
 ##  #### ##     ## ##     ## ##
 ##   ### ##     ## ##     ## ##
 ##    ##  #######  ########  ########
-/

-- the node type, which has decidable equality and
-- is inhabited
inductive node
| node: nat -> node
instance node.dec_eq : decidable_eq node := begin tactic.mk_dec_eq_instance end
instance node.inhabited : inhabited node := begin tactic.mk_inhabited_instance end

/-
 ######## ##     ## ########  ########  ########  ######   ######  ####  #######  ##    ##
 ##        ##   ##  ##     ## ##     ## ##       ##    ## ##    ##  ##  ##     ## ###   ##
 ##         ## ##   ##     ## ##     ## ##       ##       ##        ##  ##     ## ####  ##
 ######      ###    ########  ########  ######    ######   ######   ##  ##     ## ## ## ##
 ##         ## ##   ##        ##   ##   ##             ##       ##  ##  ##     ## ##  ####
 ##        ##   ##  ##        ##    ##  ##       ##    ## ##    ##  ##  ##     ## ##   ###
 ######## ##     ## ##        ##     ## ########  ######   ######  ####  #######  ##    ##
-/

-- here we define the inductive type 'exp' for expressions,
-- along with some notations

-- A dependent tuple of an attribute and a parameter
structure ap :=  mk :: (attr:cfg.A) (param:cfg.ρ attr)

-- Expression
inductive exp
| eVal    {T:Type} (v:T):                             exp
| eCond   (eC eT eF:exp):                             exp
| eApp    (eF eP: exp):                               exp
| eAttr   (eN: exp) (a:cfg.A) (eP:exp):               exp
| eCache  (n:node) (a:cfg.A) (p:cfg.ρ a) (e:exp):     exp
| eMk     (fl:node -> list (exp × ap cfg)):           exp

-- expression notations
notation `{{` z `}}`                    := (exp.eVal _ z)
notation `IFF ` c ` THEN ` t ` ELSE ` f := exp.eCond c t f
notation f ` OF ` p                     := exp.eApp f p
notation n ` DOT ` a ` WITH ` p         := exp.eAttr n a p
notation n ` DOT ` a                    := exp.eAttr n a {{()}}
notation n ` / ` a ` / ` p ` ;=` e      := exp.eCache n a p e
notation `MK ` fl                       := exp.eMk fl

-- sanity checks for the notations
-- val
#check {{_}}
#check exp.eVal cfg _
-- cond
#check IFF _ THEN _ ELSE _
#check exp.eCond _ _ _
-- app
#check _ OF _
#check exp.eApp _ _
-- attr
#check _ DOT _
#check _ DOT _ WITH _
#check exp.eAttr _ _ _
-- cache
#check _/_/_ ;= _
#check exp.eCache _ _ _ _
-- mk
#check MK _
#check exp.eMk _


/-
 ##     ##    ###    ##       ##     ## ########
 ##     ##   ## ##   ##       ##     ## ##
 ##     ##  ##   ##  ##       ##     ## ##
 ##     ## ##     ## ##       ##     ## ######
  ##   ##  ######### ##       ##     ## ##
   ## ##   ##     ## ##       ##     ## ##
    ###    ##     ## ########  #######  ########
-/

-- here we develop the propositions that decide whether
-- an expression is a value (trivial), a nonvalue (not including mk),
-- or if the expression is (or contains) a mk expression


-- "expression is a value" property
inductive value {cfg:_} : exp cfg -> Prop
| val {T:Type} {v:T}: value (exp.eVal _ v)

-- "expression is not a value or a MK expression" property
inductive notvalue {cfg:_} : exp cfg -> Prop
| cond {e1 e2 e3:exp cfg} : notvalue (IFF e1 THEN e2 ELSE e3)
| app {e1 e2:exp cfg}    : notvalue (e1 OF e2)
| attr {e1 e2:exp cfg} {a:cfg.A} : notvalue (e1 DOT a WITH e2)
| cache {n:node} {a:cfg.A} {p:cfg.ρ a} {e:exp cfg} : notvalue (n/a/p;=e)

-- "is or contains a mk expression"
-- this is used in our strong progress proof
inductive cont_mk {cfg:_} : exp cfg -> Prop
| mk {hl:_}: cont_mk (exp.eMk hl)
| condC {eC eT eF:exp cfg}:
    cont_mk eC ->
    cont_mk (IFF eC THEN eT ELSE eF)
| condT {eC eT eF:exp cfg}:
    cont_mk eT ->
    cont_mk (IFF eC THEN eT ELSE eF)
| condF {eC eT eF:exp cfg}:
    cont_mk eF ->
    cont_mk (IFF eC THEN eT ELSE eF)
| appF {eF eP:exp cfg}:
    cont_mk eF ->
    cont_mk (eF OF eP)
| appP {eF eP:exp cfg}:
    cont_mk eP ->
    cont_mk (eF OF eP)
| attrN {eN eP:exp cfg} {a:cfg.A}:
    cont_mk eN ->
    cont_mk (eN DOT a WITH eP)
| attrP {eN eP:exp cfg} {a:cfg.A}:
    cont_mk eP ->
    cont_mk (eN DOT a WITH eP)
| cache {eC:exp cfg} {n:node} {a:cfg.A} {p:cfg.ρ a}:
    cont_mk eC ->
    cont_mk (n/a/p ;= eC)

/-
 ######## ##    ## ########  ########  ######
    ##     ##  ##  ##     ## ##       ##    ##
    ##      ####   ##     ## ##       ##
    ##       ##    ########  ######    ######
    ##       ##    ##        ##             ##
    ##       ##    ##        ##       ##    ##
    ##       ##    ##        ########  ######
-/

-- Expression types
inductive expType {cfg} : exp cfg -> Type -> Prop
| eVal {T:Type} {v:T}:
      expType {{v}} T
| eCond {eC eT eF:exp cfg} {T:Type}
    (ct: expType eC bool)
    (tt: expType eT T)
    (ft: expType eF T):
      expType (IFF eC THEN eT ELSE eF) T
| eApp {eF eP:exp cfg} {T1 T2:Type}
    (ft:expType eF (T1 -> T2))
    (pt:expType eP T1):
    expType (eF OF eP) T2
| eAttr {eN eP:exp cfg} {a:cfg.A}
    (nt: expType eN (node))
    (pt: expType eP (cfg.ρ a)):
      expType (eN DOT a WITH eP) (cfg.τ a)
| eCache (n:node) (a:cfg.A) (p:cfg.ρ a) (eC:exp cfg)
    (et: expType eC (cfg.τ a)):
      expType (n / a / p ;= eC) (cfg.τ a)
| eMk (fl: node -> list (exp cfg × ap cfg))
    (ta: ∀ (n:node) (e:exp cfg) (a:cfg.A) (p:cfg.ρ a),
      (e, (ap.mk a p)) ∈ (fl n) -> 
      expType e (cfg.τ a)
    ):
    expType (exp.eMk fl) (node)



/-
 ##    ##    ###    ########  ##     ##    ###    ########  ######  ##     ##
 ###   ##   ## ##   ##     ## ###   ###   ## ##      ##    ##    ## ##     ##
 ####  ##  ##   ##  ##     ## #### ####  ##   ##     ##    ##       ##     ##
 ## ## ## ##     ## ########  ## ### ## ##     ##    ##    ##       #########
 ##  #### ######### ##        ##     ## #########    ##    ##       ##     ##
 ##   ### ##     ## ##        ##     ## ##     ##    ##    ##    ## ##     ##
 ##    ## ##     ## ##        ##     ## ##     ##    ##     ######  ##     ##
-/

-- 'napmatch' is a boolean function that matches a triple
-- of node, attribute, parameter

-- type equality recursion into rho
@[reducible]
lemma arw : forall {a1 a2:cfg.A},
  a1 = a2 -> ((cfg.ρ a2) = (cfg.ρ a1))
:= begin intros, rewrite a, end

-- boolean equality function for node attribute paramater triples.
@[reducible]
def napmatch (n n':node) (a a':cfg.A) (p:cfg.ρ a) (p':cfg.ρ a') : bool :=
  @ite (n=n') _ bool
  ( -- n = n'
    @dite (a = a') (cfg.Adec a a') bool
    ( -- a = a'
      λ ha,
      @ite (p=cast (@arw cfg a a' ha) p') (cfg.ρdec a p _) bool
        tt -- p = p'
        ff -- p ≠ p' 
    ) (λ _, ff) -- a ≠ a'
  ) ff -- n ≠ n'

-- boolean equality function for attribute parameter doubles.
@[reducible]
def apmatch (a a':cfg.A) (p:cfg.ρ a) (p':cfg.ρ a') : bool :=
  @dite (a=a') (cfg.Adec a a') bool
  ( -- a = a'
    λ ha,
    @ite (p=cast (@arw cfg a a' ha) p') (cfg.ρdec a p _) bool
      tt -- p = p'
      ff -- p ≠ p' 
  ) (λ _, ff) -- a ≠ a'

-- boolean equality function for attributes
@[reducible]
def amatch (a a':cfg.A) : bool :=
  @ite (a=a') (cfg.Adec a a') bool tt ff




/-
  ######   #######  ##    ## ######## ######## ##     ## ########
 ##    ## ##     ## ###   ##    ##    ##        ##   ##     ##
 ##       ##     ## ####  ##    ##    ##         ## ##      ##
 ##       ##     ## ## ## ##    ##    ######      ###       ##
 ##       ##     ## ##  ####    ##    ##         ## ##      ##
 ##    ## ##     ## ##   ###    ##    ##        ##   ##     ##
  ######   #######  ##    ##    ##    ######## ##     ##    ##
-/

-- Context Type
def context := ∀ (n:node) (a:cfg.A) (p:cfg.ρ a), exp cfg

-- a context and a list of nodes
structure ctxN :=  mk :: (c:context cfg) (N:list node)

-- the "empty" context
@[reducible]
def emptyC : context cfg :=
  λ _ a _, {{(cfg.τinh a).default}}

@[reducible]
def emptyCN : ctxN cfg := ctxN.mk (emptyC cfg) []


-- extend the context with a value (caching)
@[reducible]
def extendC (n:node) (a:cfg.A) (p:cfg.ρ a) (v:cfg.τ a)
            (c:context cfg) : context cfg :=
  λ n' a' p',
    bite (napmatch cfg n n' a a' p p') 
    ({{v}})
    (c n' a' p')

-- extend the context with an expression (higher order)
@[reducible]
def extendE (n:node) (a:cfg.A) (p:cfg.ρ a) (e:exp cfg)
            (c:context cfg) : context cfg :=
  λ n' a' p',
    bite (napmatch cfg n n' a a' p p') 
    (e)
    (c n' a' p')

def extendCN (n:node) (a:cfg.A) (p:cfg.ρ a) (v:cfg.τ a) (c:ctxN cfg) : ctxN cfg
:= ctxN.mk (extendC cfg n a p v c.c) c.N

def extendEN (n:node) (a:cfg.A) (p:cfg.ρ a) (e:exp cfg) (c:ctxN cfg) : ctxN cfg
:= ctxN.mk (extendE cfg n a p e (c.c)) c.N

@[reducible]
def writeAL {cfg:saigaconfig} (ctx:ctxN cfg) (n:node) : list (exp cfg × ap cfg) -> ctxN cfg
| ((e, hap) :: al) := extendEN cfg n (hap.attr) (hap.param) e (writeAL al)
| []               := ctx



/-
 ######## ########   ######  ##     ##       ##  ######  ##    ## ########
 ##       ##     ## ##    ## ##     ##      ##  ##    ## ###   ##    ##
 ##       ##     ## ##       ##     ##     ##   ##       ####  ##    ##
 ######   ########   ######  #########    ##    ##       ## ## ##    ##
 ##       ##   ##         ## ##     ##   ##     ##       ##  ####    ##
 ##       ##    ##  ##    ## ##     ##  ##      ##    ## ##   ###    ##
 ##       ##     ##  ######  ##     ## ##        ######  ##    ##    ##
-/

-- the axioms and definitions we use to deal with nodes
-- that do and do not exist in a context

-- add a node to an (unordered) list of nodes. not implemented.
def addN_N (n:node) (nl:list node) : list node := sorry

-- produce a new node that did not exist in the ctxN
def fresh_for {cfg:saigaconfig} : ctxN cfg -> node := sorry

-- a node exists in a list of nodes
inductive contained:    node -> list node -> Prop

-- a node does not exist in a list of nodes
inductive notcontained: node -> list node -> Prop

-- add a node to a ctxN (using addN_N)
def addN {cfg:saigaconfig} (n:node) (c:ctxN cfg) : ctxN cfg :=
  ctxN.mk (c.c) (addN_N n c.N)

-- add a number of nodes to a ctxN
def addNs {cfg:saigaconfig} (c:ctxN cfg) : list node -> ctxN cfg
  | [] := c
  | (n :: r) := addN n (addNs r)

-- a list of nodes is not the same once a node is added to it
axiom addN.contra {cfg:saigaconfig} : ∀ {c:ctxN cfg} {n:node},
  c.N = (addN n c).N -> false

-- the order of adding nodes does not matter
axiom addN.order.irrel {cfg:saigaconfig} : ∀ {n n':node} {c:ctxN cfg},
  addN n (addN n' c) = addN n' (addN n c)

-- if n is contained in c, adding something to c does not change that
axiom contained.addN : ∀ {cfg:saigaconfig} {c:ctxN cfg} {n n':node},
  contained n c.N ->
  contained n (addN n' c).N

-- a node is contained in any list after adding it
axiom addN.contained : ∀ {cfg:saigaconfig} {c:ctxN cfg} {n:node},
  contained n (addN n c).N

-- fresh_for is the same if the list of nodes is the same
axiom fresh_for.N {cfg:saigaconfig} : ∀ {c1 c2:ctxN cfg},
  c1.N = c2.N ->
  fresh_for c1 = fresh_for c2

-- if a node is contained in a list, then it is not fresh for it
axiom fresh_for.contained {cfg:saigaconfig} : ∀ {ctx:ctxN cfg} {n:node},
  contained n ctx.N ->
  n ≠ fresh_for ctx

-- fresh_for isn't changed by addN, unless you add the same node
axiom fresh_for.notspoiled {cfg:saigaconfig} : ∀ {n:node}
  {c:ctxN cfg},
  n ≠ fresh_for c ->
  fresh_for c = fresh_for (addN n c)

-- addN does not change the set of outputs for a ctxN
lemma addN.c : ∀ {cfg:saigaconfig} {ctx:ctxN cfg} {n:node},
  (addN n ctx).c = ctx.c
:= begin
  introv, unfold addN,
end

-- changing the context function (not it's N) won't change the result
-- of fresh_for
axiom fresh_for.extendEN {cfg:saigaconfig} : ∀ {ctx:ctxN cfg} {n:node}
        {a:cfg.A} {p:cfg.ρ a} {e:exp cfg},
  fresh_for (extendEN cfg n a p e ctx) = fresh_for ctx


/-
 ##    ##  #######  ########  ######     ###     ######  ##     ## ########
 ###   ## ##     ##    ##    ##    ##   ## ##   ##    ## ##     ## ##
 ####  ## ##     ##    ##    ##        ##   ##  ##       ##     ## ##
 ## ## ## ##     ##    ##    ##       ##     ## ##       ######### ######
 ##  #### ##     ##    ##    ##       ######### ##       ##     ## ##
 ##   ### ##     ##    ##    ##    ## ##     ## ##    ## ##     ## ##
 ##    ##  #######     ##     ######  ##     ##  ######  ##     ## ########
-/

-- notcache means an expression doesn't hold any cache expressions
inductive notcache {cfg}: exp cfg -> Prop
| eVal:  ∀ {T:Type} {v:T},
    notcache {{v}}
| eCond: ∀ {eC eT eF:exp _},
    notcache eC ->
    notcache eT ->
    notcache eF ->
    notcache (IFF eC THEN eT ELSE eF)
| eApp:  ∀ {eF eP:exp _} ,
    notcache eF -> 
    notcache eP -> 
    notcache (eF OF eP)
| eAttr: ∀ {eN eP:exp _} {a:cfg.A},
    notcache eN ->
    notcache eP ->
    notcache (eN DOT a WITH eP)
-- | eMkNil:
--     notcache (exp.eMk [])
-- | eMkCons {e:exp cfg} {hap:ap cfg} {al:list (exp cfg × ap cfg)}:
--     notcache e ->
--     notcache (exp.eMk al) ->
--     notcache (exp.eMk ((e, hap) :: al))
    

/-
  ######   #######  ##     ## ##    ## ########      ######  ######## ##     ##
 ##    ## ##     ## ##     ## ###   ## ##     ##    ##    ##    ##     ##   ##
 ##       ##     ## ##     ## ####  ## ##     ##    ##          ##      ## ##
  ######  ##     ## ##     ## ## ## ## ##     ##    ##          ##       ###
       ## ##     ## ##     ## ##  #### ##     ##    ##          ##      ## ##
 ##    ## ##     ## ##     ## ##   ### ##     ##    ##    ##    ##     ##   ##
  ######   #######   #######  ##    ## ########      ######     ##    ##     ##
-/

-- states that every returned expression is notcache and typed
inductive soundC {cfg} : context cfg -> Prop
| soundC (ctx:context _):
    (∀ (n:node) (a:cfg.A) (p:cfg.ρ a), expType (ctx n a p) (cfg.τ a)) ->
    (∀ (n:node) (a:cfg.A) (p:cfg.ρ a), notcache (ctx n a p)) ->
    soundC ctx

-- the empty context is sound
lemma emptyC.sound: soundC (@emptyC cfg)
:= begin
  intros, constructor,
  { intros, unfold emptyC, constructor, },
  { intros, unfold emptyC, constructor, },
end

@[reducible] def getVal (T:Type) : (node × T -> T) :=
  prod.snd

@[reducible] def getValExp (T:Type) : exp cfg :=
  {{ getVal T }}



/-
  ######  ######## ######## ########
 ##    ##    ##    ##       ##     ##
 ##          ##    ##       ##     ##
  ######     ##    ######   ########
       ##    ##    ##       ##
 ##    ##    ##    ##       ##
  ######     ##    ######## ##
-/

-- step relation
inductive step {cfg:saigaconfig} : ctxN cfg -> exp cfg -> ctxN cfg -> exp cfg -> Prop
/-cond-/
| condStep {ctx ctx':ctxN _} {e e' eT eF:exp _}
  (cs: step ctx e ctx' e'):
  step ctx (IFF e THEN eT ELSE eF) ctx' (IFF e' THEN eT ELSE eF)

| condTrue {ctx:ctxN _} {eT eF:exp cfg}:
  step ctx (IFF {{tt}} THEN eT ELSE eF) ctx eT

| condFalse {ctx:ctxN _} {eT eF:exp cfg}:
  step ctx (IFF {{ff}} THEN eT ELSE eF) ctx eF

/-app-/
| appFun {ctx ctx':ctxN cfg} {eF eF' eP:exp cfg}
  (fs: step ctx eF ctx' eF'):
  step ctx (eF OF eP) ctx' (eF' OF eP)

| appParam  {T:Type} {ctx ctx':ctxN _} {f:T} {eP eP':exp cfg}
  (ps: step ctx eP ctx' eP'):
  step ctx ({{f}} OF eP) ctx' ({{f}} OF eP')

| appApp {T1 T2:Type} {ctx:ctxN cfg} {f:(T1 -> T2)} {p:T1}:
  step ctx ({{f}} OF {{p}}) ctx {{f p}}

/-attr-/
| attrNode {ctx ctx':ctxN _} {a:cfg.A} {eN eN' eP:exp _}
  (ns: step ctx eN ctx' eN'):
  step ctx (eN DOT a WITH eP) ctx' (eN' DOT a WITH eP)

| attrParam {ctx ctx':ctxN _} {a:cfg.A} {T:Type} {v:T} {eP eP':exp _}
  (ps: step ctx eP ctx' eP'):
  step ctx ({{v}} DOT a WITH eP) ctx' ({{v}} DOT a WITH eP')

| attrFetchValue {ctx:ctxN _} {a:cfg.A} {n:node} {p:cfg.ρ a}
  (v: value (ctx.c n a p)):
  step ctx ({{n}} DOT a WITH {{p}}) ctx (ctx.c n a p)

| attrFetchCached {ctx:ctxN _} {a:cfg.A} {n:node} {p:cfg.ρ a}
  (nv: notvalue (ctx.c n a p)):
  step ctx ({{n}} DOT a WITH {{p}}) ctx (n/a/p ;= (ctx.c n a p))

| attrFetchHO  {ctx:ctxN _} {a:cfg.A} {n:node} {p:cfg.ρ a} {fl: _} {n_f:node}
  (tp_a: cfg.τ a = node)
  (ismk: ctx.c n a p = exp.eMk fl)
  (isfresh: n_f = fresh_for ctx):
  step ctx ({{n}} DOT a WITH {{p}}) (@extendCN cfg n a p (eq.mpr tp_a n_f) (addN n_f (writeAL ctx n_f (fl n_f)))) {{n_f}}

/-cache-/
| cacheStep {ctx ctx':ctxN _} {a:cfg.A} {eC eC':exp _} {n:node} {p:cfg.ρ a}
  (es: step ctx eC ctx' eC'):
  step ctx (n/a/p ;= eC) ctx' (n/a/p ;= eC')

| cacheWrite {ctx:ctxN _} {a:cfg.A} {v:cfg.τ a} {n:node} {p:cfg.ρ a}:
  step ctx (n / a / p ;= {{v}}) (@extendCN cfg n a p v ctx) {{v}}

notation ctx1 ` ⊨ ` e1 ` ⟶ ` ctx2 ` ⊨ ` e2 := step ctx1 e1 ctx2 e2
#check (@step cfg (emptyCN cfg) {{tt}} (emptyCN cfg) {{tt}})
#check ((emptyCN cfg) ⊨ {{tt}} ⟶ (emptyCN cfg) ⊨ {{tt}})



/-
 ##     ## ##     ## ##       ######## ####  ######  ######## ######## ########
 ###   ### ##     ## ##          ##     ##  ##    ##    ##    ##       ##     ##
 #### #### ##     ## ##          ##     ##  ##          ##    ##       ##     ##
 ## ### ## ##     ## ##          ##     ##   ######     ##    ######   ########
 ##     ## ##     ## ##          ##     ##        ##    ##    ##       ##
 ##     ## ##     ## ##          ##     ##  ##    ##    ##    ##       ##
 ##     ##  #######  ########    ##    ####  ######     ##    ######## ##
-/

-- multistep relation
inductive multistep {cfg:saigaconfig} : ∀ {T:Type}, ctxN cfg -> exp cfg -> ctxN cfg -> T -> Prop
| refl : ∀ {T:Type} {v:T} {ctx:ctxN _},
  multistep ctx {{v}} ctx v
| step : ∀ {T:Type} {e1 e2:exp _} {v:T} {ctx1 ctx2 ctx3:ctxN _},
  step ctx1 e1 ctx2 e2 ->
  multistep ctx2 e2 ctx3 v ->
  multistep ctx1 e1 ctx3 v
notation ctx1 ` ⊨ ` e1 ` ⟹ ` ctx2 ` ⊨ ` e2 := multistep ctx1 e1 ctx2 e2
#check (multistep (emptyCN cfg) {{tt}} (emptyCN cfg) tt) 
#check ((emptyCN cfg) ⊨ {{tt}} ⟹ (emptyCN cfg) ⊨ tt)



/-
 ########  ####  ######    ######  ######## ######## ########
 ##     ##  ##  ##    ##  ##    ##    ##    ##       ##     ##
 ##     ##  ##  ##        ##          ##    ##       ##     ##
 ########   ##  ##   ####  ######     ##    ######   ########
 ##     ##  ##  ##    ##        ##    ##    ##       ##
 ##     ##  ##  ##    ##  ##    ##    ##    ##       ##
 ########  ####  ######    ######     ##    ######## ##
-/


inductive bigstep {cfg:saigaconfig} : ∀ {T:Type}, ctxN cfg -> exp cfg -> ctxN cfg -> T -> Prop
| refl {T:Type}{v:T} {ctx:ctxN _}:
    bigstep ctx {{v}} ctx v

| condT {T:Type} {eC eT eF:exp cfg} {v:T} {ctx1 ctx2 ctx3:ctxN cfg}
  (cs: bigstep ctx1 eC ctx2 tt)
  (ts: bigstep ctx2 eT ctx3 v):
  bigstep ctx1 (IFF eC THEN eT ELSE eF) ctx3 v

| condF {T:Type} {eC eT eF:exp cfg} {v:T} {ctx1 ctx2 ctx3:ctxN cfg}
  (cs:bigstep ctx1 eC ctx2 ff)
  (fs: bigstep ctx2 eF ctx3 v):
  bigstep ctx1 (IFF eC THEN eT ELSE eF) ctx3 v

| app {T1 T2:Type} {eF eP:exp cfg} {ctx1 ctx2 ctx3:ctxN cfg}
          {f:(T1 -> T2)} {p:T1}
  (fs: bigstep ctx1 eF ctx2 f)
  (ps: bigstep ctx2 eP ctx3 p):
  bigstep ctx1 (eF OF eP) ctx3 (f p)

| attrValue {eN eP:exp cfg} {a:cfg.A} {n:node} {p:cfg.ρ a} {T:Type}
          {v:T} {ctx1 ctx2 ctx3 ctx4:ctxN cfg}
  (hv: value (ctx3.c n a p))
  -- (cont: contained n ctx2.N)
  (ns: bigstep ctx1 eN ctx2 n)
  (ps: bigstep ctx2 eP ctx3 p)
  (cs: bigstep ctx3 (ctx3.c n a p) ctx4 v):
  bigstep ctx1 (eN DOT a WITH eP) ctx4 v

| attrCached {eN eP:exp cfg} {a:cfg.A} {n:node} {p:cfg.ρ a}
          {v:cfg.τ a} {ctx1 ctx2 ctx3 ctx4:ctxN cfg}
  (nv: notvalue (ctx3.c n a p))
  -- (cont: contained n ctx2.N)
  (ns: bigstep ctx1 eN ctx2 n)
  (ps: bigstep ctx2 eP ctx3 p)
  (cs: bigstep ctx3 (ctx3.c n a p) ctx4 v):
  bigstep ctx1 (eN DOT a WITH eP) (extendCN cfg n a p v ctx4) v

| attrHO {eN eP:exp cfg} {a:cfg.A} {n n_f:node} {p:cfg.ρ a} {ctx1 ctx2 ctx3:ctxN cfg} {fl:_}:
  ∀ (tp_a: cfg.τ a = node)
  -- (cont: contained n ctx2.N)
  (ismk: (ctx3.c n a p) = exp.eMk fl)
  (isfresh: n_f = fresh_for ctx3)
  (ns: bigstep ctx1 eN ctx2 n)
  (ps: bigstep ctx2 eP ctx3 p),
  bigstep ctx1 (eN DOT a WITH eP) (extendCN cfg n a p (eq.mpr tp_a n_f) (addN n_f (writeAL ctx3 n_f (fl n_f)))) n_f

| cache {eC:exp cfg} {a:cfg.A} {n:node} {p:cfg.ρ a} {v:cfg.τ a} {ctx1 ctx2:ctxN cfg}:
  ∀
  -- (cont: contained n ctx1.N)
  (c: bigstep ctx1 eC ctx2 v),
  bigstep ctx1 (n/a/p;= eC) (extendCN cfg n a p v ctx2) v
notation ctx1 ` ⊨ ` e ` »» ` ctx2 ` ⊨ ` v := bigstep ctx1 e ctx2 v
#check (bigstep (emptyCN cfg) {{tt}} (emptyCN cfg) tt)
#check ((emptyCN cfg) ⊨ {{tt}} »» (emptyCN cfg) ⊨ tt)














































-- lemma psum_exp_sizeof_extract: ∀ {e1 e2:exp cfg},
--   exp_my_sizeof cfg e1 < exp_my_sizeof cfg e2 ->
--   has_well_founded.r (psum.inl e1) (@psum.inl (exp cfg) (list (ap cfg × exp cfg)) e2)
-- := begin
--   intros, unfold has_well_founded.r, unfold sizeof_measure,
--   unfold measure, unfold inv_image, unfold sizeof,
--   unfold has_sizeof.sizeof, unfold psum.sizeof,
--   apply add_lt_add_left, exact a,
-- end

-- lemma psum_listexp_sizeof_extract: ∀ {n:nat} {le:list (ap cfg × exp cfg)},
--   has_well_founded.r (psum.inr le) (psum.inl (MK n ! le))
-- := begin
--   intros, unfold has_well_founded.r, unfold sizeof_measure,
--   unfold measure, unfold inv_image, unfold sizeof,
--   unfold has_sizeof.sizeof, unfold psum.sizeof,
--   unfold sizeof, unfold has_sizeof.sizeof,
--   apply add_lt_add_left, unfold exp_my_sizeof,
--   constructor,
-- end



-- lemma psum_e_less_list_w_e: ∀ {e:exp cfg} {ap:ap cfg} {r:list (saiga.ap cfg × exp cfg)},
--   has_well_founded.r (psum.inl e) (psum.inr ((ap, e) :: r))
-- := begin
--   intros, unfold has_well_founded.r, unfold sizeof_measure,
--   unfold measure, unfold inv_image, unfold sizeof,
--   unfold has_sizeof.sizeof, unfold psum.sizeof,
--   unfold sizeof, unfold has_sizeof.sizeof,
--   apply add_lt_add_left,
--   unfold list_exp_my_sizeof, apply nat.lt_of_succ_lt,
--   rewrite nat.one_add, rewrite nat.succ_add, apply nat.succ_lt_succ,
--   apply nat.lt_add_of_zero_lt_left,
--   induction r, {
--     constructor,
--   }, {
--     cases r_hd, unfold list_exp_my_sizeof,
--     apply nat.lt_add_left, exact r_ih,
--   },
-- end

-- lemma psum_list_less_bigger_list: ∀ {ape:ap cfg × exp cfg} {r:list (ap cfg × exp cfg)},
--   has_well_founded.r (psum.inr r) (@psum.inr (exp cfg) _ (ape :: r))
-- := begin
--   intros, unfold has_well_founded.r, unfold sizeof_measure,
--   unfold measure, unfold inv_image, unfold sizeof,
--   unfold has_sizeof.sizeof, unfold psum.sizeof,
--   unfold sizeof, unfold has_sizeof.sizeof,
--   apply add_lt_add_left, cases ape, unfold list_exp_my_sizeof,
--   rewrite nat.add_comm, apply nat.lt_add_of_zero_lt_left,
--   apply nat.zero_lt_one_add,
-- end


-- meta def exp_decreasing : tactic unit :=
--   (do to_expr ``(exp.r.cond_eC) >>= apply
--   <|> to_expr ``(exp.r.cond_eT) >>= apply
--   <|> to_expr ``(exp.r.cond_eF) >>= apply
--   <|> to_expr ``(exp.r.app_eP) >>= apply
--   <|> to_expr ``(exp.r.attr_eN) >>= apply
--   <|> to_expr ``(exp.r.attr_eP) >>= apply
--   <|> to_expr ``(exp.r.cache_eC) >>= apply, skip)

open tactic
-- --FIXME: remove the "skip"s
meta def exp_dec_tac : tactic unit :=
  -- (do to_expr ``(psum_exp_sizeof_extract) >>= apply,
  (do to_expr ``(exp.r.cond_eC) >>= apply
  <|> to_expr ``(exp.r.cond_eT) >>= apply
  <|> to_expr ``(exp.r.cond_eF) >>= apply
  <|> to_expr ``(exp.r.app_eF) >>= apply
  <|> to_expr ``(exp.r.app_eP) >>= apply
  <|> to_expr ``(exp.r.attr_eN) >>= apply
  <|> to_expr ``(exp.r.attr_eP) >>= apply
  -- <|> to_expr ``(exp.r.mk_member) >>= apply
  -- <|> to_expr ``(exp.r.mk_tail) >>= apply
  <|> to_expr ``(exp.r.cache_eN) >>= apply
  <|> to_expr ``(exp.r.cache_eP) >>= apply
  <|> to_expr ``(exp.r.cache_eC) >>= apply
  <|> fail "none of my 'exp.r.___' lemmas worked.", skip)

-- end exp_reduction_proofs















-- def node_to_nat : node -> nat
-- | (node.node n) := n

-- def nodemap_natmap (nmap:node -> node) : nat -> nat
-- | n := node_to_nat (nmap (node.node n))

-- lemma node_to_nat_nodenode_inverse: ∀ {nd:node},
--   node.node (node_to_nat nd) = nd
-- := begin
--   intros, cases nd, unfold node_to_nat,
-- end

-- lemma nodenode_node_to_nat_inverse: ∀ {n:nat},
--   (node_to_nat (node.node n)) = n
-- := begin
--   intros, unfold node_to_nat,
-- end

-- lemma nodemap_natmap_comp: ∀ {n:nat} {nmap1 nmap2:node -> node},
--   nodemap_natmap nmap1 (nodemap_natmap nmap2 n) = nodemap_natmap (nmap1 ∘ nmap2) n
-- := begin
--   introv, unfold function.comp, unfold nodemap_natmap, 
--   rewrite node_to_nat_nodenode_inverse,
-- end



end saiga